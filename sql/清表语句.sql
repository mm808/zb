-- 清表语句
TRUNCATE TABLE f_attach ;

TRUNCATE TABLE `f_category` ;

TRUNCATE TABLE `f_content` ;

TRUNCATE TABLE `f_content_article` ;

TRUNCATE TABLE `f_member` ;

TRUNCATE TABLE `f_member_log`;

TRUNCATE TABLE `f_sort` ;

TRUNCATE TABLE tag ;

TRUNCATE TABLE `mid_content_recomend` ;

TRUNCATE TABLE `mid_content_tag` ;