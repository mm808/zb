/*
SQLyog Ultimate v11.42 (64 bit)
MySQL - 5.6.23 : Database - yayi_spring_boot
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`yayi_spring_boot` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `yayi_spring_boot`;

/*Table structure for table `def_config` */

DROP TABLE IF EXISTS `def_config`;

CREATE TABLE `def_config` (
  `id` int(10) unsigned NOT NULL COMMENT 'id',
  `module` varchar(50) NOT NULL DEFAULT '' COMMENT '所属模块',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '配置名称',
  `description` varchar(50) NOT NULL DEFAULT '' COMMENT '描述',
  `k` varchar(255) NOT NULL DEFAULT '' COMMENT 'key',
  `v` varchar(2000) NOT NULL DEFAULT '' COMMENT 'value',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `create_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
  `update_id` int(11) NOT NULL DEFAULT '0' COMMENT '修改人id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `def_config` */

LOCK TABLES `def_config` WRITE;

insert  into `def_config`(`id`,`module`,`name`,`description`,`k`,`v`,`create_time`,`update_time`,`create_id`,`update_id`) values (1,'site','site','前台地址','front.domain','http://localhost:8082/box/front','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(2,'site','site','附件主机地址','attach.domain','http://localhost:8082/box/','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(3,'site','mail','SMTP主机','mail.host','smtp.126.com','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(4,'site','mail','邮箱端口','mail.port','25','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(5,'site','mail','邮箱地址','mail.username','platformitmuch@126.com','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(6,'site','mail','邮箱密码','mail.password','klozndsbemteyrih','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(7,'site','mail','是否认证','mail.smtp.auth','true','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(8,'site','mail','发信人','mail.from','发信人1','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(9,'site','mail','注册邮箱主题','mail.reg.subject','[]注册邮箱','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(10,'site','mail','注册邮箱模板','mail.reg.text','<!DOCTYPE html>\n<html>\n<head>\n<meta charset=\"UTF-8\">\n<title>Insert title here</title>\n</head>\n<body>\n<pre>\n您好！<br>\n您于 ${(date?string(\"yyyy年MM月dd日 hh:mm\"))!} 注册东信宝帐号${email!} ，点击以下链接，即可完成注册：<br>\n${url!}<br>\n(如果您无法点击此链接，请将它复制到浏览器地址栏后访问)<br>\n1、为了保障您帐号的安全性，请在 24小时内完成注册，此链接将在您注册完成后失效<br>\n2、请尽快完成注册，否则过期，即${(endDate?string(\"yyyy年MM月dd日 hh:mm\"))!}后该链接失效。<br>\n<br>\n东信宝团队<br>\n<br>\n${(date?string(\"yyyy年MM月dd日\"))!}<br>\n</pre>\n</body>\n</html>','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0);

UNLOCK TABLES;

/*Table structure for table `def_recomend` */

DROP TABLE IF EXISTS `def_recomend`;

CREATE TABLE `def_recomend` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '推荐名称',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `create_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
  `update_id` int(11) NOT NULL DEFAULT '0' COMMENT '修改人id',
  `del_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `def_recomend` */

LOCK TABLES `def_recomend` WRITE;

insert  into `def_recomend`(`id`,`name`,`create_time`,`update_time`,`create_id`,`update_id`,`del_flag`) values (1,'首页焦点图推荐','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,0),(2,'首页头条推荐','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,0),(3,'栏目首页推荐','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,0),(4,'栏目焦点图推荐','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,0);

UNLOCK TABLES;

/*Table structure for table `f_attach` */

DROP TABLE IF EXISTS `f_attach`;

CREATE TABLE `f_attach` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `member_id` int(11) NOT NULL DEFAULT '0' COMMENT 'f_member.id',
  `upload_dir` varchar(255) NOT NULL DEFAULT '' COMMENT '上传路径',
  `saved_filename` varchar(64) NOT NULL DEFAULT '' COMMENT '保存的文件名',
  `original_filename` varchar(80) NOT NULL DEFAULT '' COMMENT '原始的文件名',
  `extension` varchar(20) NOT NULL DEFAULT '' COMMENT '扩展名',
  `full_path` varchar(256) NOT NULL DEFAULT '' COMMENT '全路径(相对)',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `create_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
  `update_id` int(11) NOT NULL DEFAULT '0' COMMENT '修改人id',
  `del_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `f_attach` */

LOCK TABLES `f_attach` WRITE;

UNLOCK TABLES;

/*Table structure for table `f_category` */

DROP TABLE IF EXISTS `f_category`;

CREATE TABLE `f_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `model_id` int(11) NOT NULL DEFAULT '0' COMMENT 'f_model.id',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父栏目ID',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '名称',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `keyword` varchar(255) NOT NULL DEFAULT '' COMMENT '关键词',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `list_order` int(11) NOT NULL DEFAULT '0' COMMENT '排列顺序',
  `target` varchar(20) NOT NULL DEFAULT '' COMMENT '目标 例:_BLANK.',
  `has_content_flag` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否有内容',
  `show_flag` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否显示',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `create_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
  `update_id` int(11) NOT NULL DEFAULT '0' COMMENT '修改人id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `f_category` */

LOCK TABLES `f_category` WRITE;

insert  into `f_category`(`id`,`model_id`,`parent_id`,`name`,`title`,`keyword`,`description`,`list_order`,`target`,`has_content_flag`,`show_flag`,`create_time`,`update_time`,`create_id`,`update_id`) values (1,1,0,'栏目1','栏目1','关键词','描述',0,'',1,1,'2015-11-19 12:15:22','0000-00-00 00:00:00',1,0),(2,1,1,'子栏目','子栏目','子栏目','子栏目',0,'',1,1,'2015-11-19 12:15:38','0000-00-00 00:00:00',1,0);

UNLOCK TABLES;

/*Table structure for table `f_comment` */

DROP TABLE IF EXISTS `f_comment`;

CREATE TABLE `f_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父id',
  `content_id` int(11) NOT NULL DEFAULT '0' COMMENT '内容ID',
  `member_id` int(11) NOT NULL DEFAULT '0' COMMENT '发表人id',
  `to_member_id` int(11) NOT NULL DEFAULT '0' COMMENT '回复给的id',
  `reply_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '回复时间',
  `ip` int(11) NOT NULL DEFAULT '0' COMMENT 'ip',
  `txt` varchar(255) NOT NULL DEFAULT '' COMMENT '评论内容',
  `ups` smallint(6) NOT NULL DEFAULT '0' COMMENT '支持数',
  `downs` smallint(6) NOT NULL DEFAULT '0' COMMENT '反对数',
  `recomend_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否推荐',
  `check_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否审核',
  `create_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
  `update_id` int(11) NOT NULL DEFAULT '0' COMMENT '修改人id',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `del_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `f_comment` */

LOCK TABLES `f_comment` WRITE;

UNLOCK TABLES;

/*Table structure for table `f_content` */

DROP TABLE IF EXISTS `f_content`;

CREATE TABLE `f_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `member_id` int(11) NOT NULL DEFAULT '0' COMMENT 'f_content.id',
  `category_id` int(11) NOT NULL DEFAULT '0' COMMENT 'f_category.id',
  `sort_id` int(11) NOT NULL DEFAULT '0' COMMENT 'f_sort.id(二级分类)',
  `model_id` int(11) NOT NULL DEFAULT '0' COMMENT 'f_model.id',
  `cover` varchar(255) NOT NULL DEFAULT '' COMMENT '文章封面',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `keyword` varchar(255) NOT NULL DEFAULT '' COMMENT '关键词',
  `author` varchar(50) NOT NULL DEFAULT '' COMMENT '作者',
  `source` varchar(50) NOT NULL DEFAULT '' COMMENT '来源',
  `source_url` varchar(255) NOT NULL DEFAULT '' COMMENT '来源地址',
  `relation_ids` varchar(255) NOT NULL DEFAULT '' COMMENT '相关内容, 多个用,分隔',
  `issue_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '发表时间',
  `create_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
  `update_id` int(11) NOT NULL DEFAULT '0' COMMENT '修改人id',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `del_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `f_content` */

LOCK TABLES `f_content` WRITE;

UNLOCK TABLES;

/*Table structure for table `f_content_article` */

DROP TABLE IF EXISTS `f_content_article`;

CREATE TABLE `f_content_article` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `content_id` int(11) NOT NULL DEFAULT '0' COMMENT 'f_content.id',
  `summary` varchar(255) NOT NULL DEFAULT '' COMMENT '摘要',
  `txt` text NOT NULL COMMENT '内容',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `create_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
  `update_id` int(11) NOT NULL DEFAULT '0' COMMENT '修改人id',
  `del_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `f_content_article` */

LOCK TABLES `f_content_article` WRITE;

UNLOCK TABLES;

/*Table structure for table `f_member` */

DROP TABLE IF EXISTS `f_member`;

CREATE TABLE `f_member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `username` varchar(50) NOT NULL DEFAULT '' COMMENT '用户名',
  `mobile` varchar(15) NOT NULL DEFAULT '' COMMENT '手机号',
  `email` varchar(50) NOT NULL DEFAULT '' COMMENT '邮箱',
  `password` varchar(255) NOT NULL DEFAULT '' COMMENT '密码',
  `face` varchar(255) NOT NULL DEFAULT '' COMMENT '头像',
  `name` varchar(20) NOT NULL DEFAULT '' COMMENT '姓名',
  `nation` varchar(20) NOT NULL DEFAULT '' COMMENT '民族',
  `origin_place` varchar(40) NOT NULL DEFAULT '' COMMENT '籍贯',
  `home_address` varchar(40) NOT NULL DEFAULT '' COMMENT '家庭住址',
  `birthday` date NOT NULL DEFAULT '0000-00-00' COMMENT '生日',
  `sex` tinyint(4) NOT NULL DEFAULT '0' COMMENT '姓名',
  `bind_mobile_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '绑定手机号标识',
  `bind_email_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '绑定邮箱标识',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '用户状态 0:正常 1:查封 2:待审核',
  `reg_ip` varchar(20) NOT NULL DEFAULT '' COMMENT '注册ip',
  `reg_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '注册时间',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `create_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
  `update_id` int(11) NOT NULL DEFAULT '0' COMMENT '修改人id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_uni_username` (`username`),
  UNIQUE KEY `idx_uni_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `f_member` */

LOCK TABLES `f_member` WRITE;

insert  into `f_member`(`id`,`username`,`mobile`,`email`,`password`,`face`,`name`,`nation`,`origin_place`,`home_address`,`birthday`,`sex`,`bind_mobile_flag`,`bind_email_flag`,`status`,`reg_ip`,`reg_time`,`create_time`,`update_time`,`create_id`,`update_id`) values (1,'eacdy','15151816012','eacdy0000@126.com','$2a$04$YYqH6o5D3CfSgTSg8M9O6uK36/HEFDMFKSKa3rmeJV3v/7JcJLNSe','','周立','汉族','江苏.泰州','银杏新村34','2015-12-01',1,1,1,0,'0:0:0:0:0:0:0:1','2015-12-22 23:54:18','2015-12-22 23:54:18','0001-01-01 00:00:00',-1,0),(2,'itmuch','15151816013','itmuch0000@126.com','$2a$04$niID1A80kVz4bGn91CQkYeB/2ujubVR0SLEP7qh41gdu88FDieLDK','','','','','','0001-01-01',0,1,1,0,'0:0:0:0:0:0:0:1','2015-12-23 10:04:57','2015-12-23 10:04:57','0001-01-01 00:00:00',-1,0);

UNLOCK TABLES;

/*Table structure for table `f_member_log` */

DROP TABLE IF EXISTS `f_member_log`;

CREATE TABLE `f_member_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `member_id` int(11) NOT NULL DEFAULT '0' COMMENT 'f_member',
  `operation` tinyint(4) NOT NULL DEFAULT '0' COMMENT '操作 1:登陆 2:注册 3:忘记密码 4:修改密码',
  `time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '操作时间',
  `ip` varchar(20) NOT NULL DEFAULT '' COMMENT '操作ip',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `create_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
  `update_id` int(11) NOT NULL DEFAULT '0' COMMENT '修改人id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `f_member_log` */

LOCK TABLES `f_member_log` WRITE;

UNLOCK TABLES;

/*Table structure for table `persistent_logins` */

DROP TABLE IF EXISTS `persistent_logins`;

CREATE TABLE `persistent_logins` (
  `username` varchar(64) NOT NULL,
  `series` varchar(64) NOT NULL,
  `token` varchar(64) NOT NULL,
  `last_used` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`series`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `persistent_logins` */

LOCK TABLES `persistent_logins` WRITE;

insert  into `persistent_logins`(`username`,`series`,`token`,`last_used`) values ('eacdy0000@126.com','oY6eSzqv3Y51G2vzPJxMJQ==','0umHVAM4OZBGokKgiHtesg==','2015-12-23 23:13:13'),('itmuch0000@126.com','vMkWwWTc7r2En+exf5KksQ==','Ugem3SREW/V9PMs4L6Mwdg==','2015-12-23 13:25:22');

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
