/*
SQLyog Ultimate v11.42 (64 bit)
MySQL - 5.6.23 : Database - zb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `f_member` */

DROP TABLE IF EXISTS `f_member`;

CREATE TABLE `f_member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `username` varchar(50) NOT NULL DEFAULT '' COMMENT '用户名',
  `mobile` varchar(15) NOT NULL DEFAULT '' COMMENT '手机号',
  `email` varchar(50) NOT NULL DEFAULT '' COMMENT '邮箱',
  `password` varchar(255) NOT NULL DEFAULT '' COMMENT '密码',
  `face` varchar(255) NOT NULL DEFAULT '' COMMENT '头像',
  `name` varchar(20) NOT NULL DEFAULT '' COMMENT '姓名',
  `origin_place` varchar(40) NOT NULL DEFAULT '' COMMENT '籍贯',
  `home_address` varchar(40) NOT NULL DEFAULT '' COMMENT '家庭住址',
  `birthday` date NOT NULL DEFAULT '0000-00-00' COMMENT '生日',
  `sex` tinyint(4) NOT NULL DEFAULT '0' COMMENT '姓名',
  `bind_mobile_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '绑定手机号标识',
  `bind_email_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '绑定邮箱标识',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '用户状态 0:正常 1:查封 2:待审核',
  `last_time` date NOT NULL DEFAULT '0000-00-00',
  `reg_time` date NOT NULL DEFAULT '0000-00-00',
  `login_ip` bigint(20) NOT NULL DEFAULT '0',
  `last_ip` bigint(20) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `create_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
  `update_id` int(11) NOT NULL DEFAULT '0' COMMENT '修改人id',
  `del_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_uni_username` (`username`),
  UNIQUE KEY `idx_uni_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `f_member` */

LOCK TABLES `f_member` WRITE;

insert  into `f_member`(`id`,`username`,`mobile`,`email`,`password`,`face`,`name`,`origin_place`,`home_address`,`birthday`,`sex`,`bind_mobile_flag`,`bind_email_flag`,`status`,`last_time`,`reg_time`,`login_ip`,`last_ip`,`create_time`,`update_time`,`create_id`,`update_id`,`del_flag`) values (1,'eacdy','','eacdy0000@126.com','$2a$16$aoH1PMj/PO8r4gcUTC3mcOy2TvmEr441PH.1IP6/jA25tQtjY7cOm','','','','','0000-00-00',0,0,0,0,'0000-00-00','0000-00-00',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,0),(2,'cc','','cc@cc.com','$2a$10$wsv4lLBGlcF93425CsLgVuwlADKprqriq802sWGsCMnCgpDzf0xJm','','','','','0000-00-00',0,0,0,0,'0000-00-00','0000-00-00',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,0),(3,'ttt','','ttt@tt','$2a$10$E8nNTEFWlhZCoQOeldUQsuc0nK6cXbc7ZNdKaLQMVYQ1MYnf2TO2S','','','','','0000-00-00',0,0,0,0,'0000-00-00','0000-00-00',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,0);

UNLOCK TABLES;

/*Table structure for table `persistent_logins` */

DROP TABLE IF EXISTS `persistent_logins`;

CREATE TABLE `persistent_logins` (
  `username` varchar(64) NOT NULL,
  `series` varchar(64) NOT NULL,
  `token` varchar(64) NOT NULL,
  `last_used` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`series`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `persistent_logins` */

LOCK TABLES `persistent_logins` WRITE;

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
