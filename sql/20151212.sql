/*
SQLyog Ultimate v11.42 (64 bit)
MySQL - 5.6.25 : Database - zb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `f_member` */

DROP TABLE IF EXISTS `f_member`;

CREATE TABLE `f_member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `username` varchar(50) NOT NULL DEFAULT '' COMMENT '用户名',
  `mobile` varchar(15) NOT NULL DEFAULT '' COMMENT '手机号',
  `email` varchar(50) NOT NULL DEFAULT '' COMMENT '邮箱',
  `password` varchar(255) NOT NULL DEFAULT '' COMMENT '密码',
  `face` varchar(255) NOT NULL DEFAULT '' COMMENT '头像',
  `name` varchar(20) NOT NULL DEFAULT '' COMMENT '姓名',
  `origin_place` varchar(40) NOT NULL DEFAULT '' COMMENT '籍贯',
  `home_address` varchar(40) NOT NULL DEFAULT '' COMMENT '家庭住址',
  `birthday` date NOT NULL DEFAULT '0000-00-00' COMMENT '生日',
  `sex` tinyint(4) NOT NULL DEFAULT '0' COMMENT '姓名',
  `bind_mobile_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '绑定手机号标识',
  `bind_email_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '绑定邮箱标识',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '用户状态 0:正常 1:查封 2:待审核',
  `last_time` date NOT NULL DEFAULT '0000-00-00',
  `reg_time` date NOT NULL DEFAULT '0000-00-00',
  `login_ip` bigint(20) NOT NULL DEFAULT '0',
  `last_ip` bigint(20) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `create_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
  `update_id` int(11) NOT NULL DEFAULT '0' COMMENT '修改人id',
  `del_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `f_member` */

LOCK TABLES `f_member` WRITE;

insert  into `f_member`(`id`,`username`,`mobile`,`email`,`password`,`face`,`name`,`origin_place`,`home_address`,`birthday`,`sex`,`bind_mobile_flag`,`bind_email_flag`,`status`,`last_time`,`reg_time`,`login_ip`,`last_ip`,`create_time`,`update_time`,`create_id`,`update_id`,`del_flag`) values (1,'eacdy','15151816012','eacdy0000@126.com','$2a$04$.wmJ7afDiqb4Q4tZhZEtQ.qV.9l/6Si1MgWyl2U4qctBsT7J0eA8W','','','','','2015-12-12',0,0,0,0,'2015-12-12','2015-12-12',0,-1,'2015-12-12 02:00:25','2015-12-12 02:00:25',0,1,0),(2,'itmuch','15850770072','itmuch0000@126.com','$2a$04$.wmJ7afDiqb4Q4tZhZEtQ.qV.9l/6Si1MgWyl2U4qctBsT7J0eA8W','','','','','2015-12-12',0,0,1,0,'2015-12-12','0001-01-01',0,-1,'0001-01-01 00:00:00','2015-12-12 02:29:04',0,0,0);

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
