/*
SQLyog Ultimate v11.42 (64 bit)
MySQL - 5.6.25 : Database - yayi_spring_boot
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`yayi_spring_boot` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `yayi_spring_boot`;

/*Table structure for table `def_config` */

DROP TABLE IF EXISTS `def_config`;

CREATE TABLE `def_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `module` varchar(50) NOT NULL DEFAULT '' COMMENT '所属模块',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '配置名称',
  `description` varchar(50) NOT NULL DEFAULT '' COMMENT '描述',
  `k` varchar(255) NOT NULL DEFAULT '' COMMENT 'key',
  `v` varchar(2000) NOT NULL DEFAULT '' COMMENT 'value',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `create_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
  `update_id` int(11) NOT NULL DEFAULT '0' COMMENT '修改人id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `def_config` */

LOCK TABLES `def_config` WRITE;

insert  into `def_config`(`id`,`module`,`name`,`description`,`k`,`v`,`create_time`,`update_time`,`create_id`,`update_id`) values (1,'site','site','前台地址','front.domain','http://localhost:8082/box/front','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(2,'site','site','附件主机地址','attach.domain','http://localhost:8082/box','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(3,'site','mail','SMTP主机','mail.host','smtp.126.com','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(4,'site','mail','邮箱端口','mail.port','25','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(5,'site','mail','邮箱地址','mail.username','platformitmuch@126.com','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(6,'site','mail','邮箱密码','mail.password','klozndsbemteyrih','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(7,'site','mail','是否认证','mail.smtp.auth','true','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(8,'site','mail','发信人','mail.from','发信人1','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(9,'site','mail','注册邮箱主题','mail.reg.subject','[]注册邮箱','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(10,'site','mail','注册邮箱模板','mail.reg.text','<!DOCTYPE html>\n<html>\n<head>\n<meta charset=\"UTF-8\">\n<title>Insert title here</title>\n</head>\n<body>\n<pre>\n您好！<br>\n您于 ${(date?string(\"yyyy年MM月dd日 hh:mm\"))!} 注册东信宝帐号${email!} ，点击以下链接，即可完成注册：<br>\n${url!}<br>\n(如果您无法点击此链接，请将它复制到浏览器地址栏后访问)<br>\n1、为了保障您帐号的安全性，请在 24小时内完成注册，此链接将在您注册完成后失效<br>\n2、请尽快完成注册，否则过期，即${(endDate?string(\"yyyy年MM月dd日 hh:mm\"))!}后该链接失效。<br>\n<br>\n东信宝团队<br>\n<br>\n${(date?string(\"yyyy年MM月dd日\"))!}<br>\n</pre>\n</body>\n</html>','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0);

UNLOCK TABLES;

/*Table structure for table `def_recomend` */

DROP TABLE IF EXISTS `def_recomend`;

CREATE TABLE `def_recomend` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '推荐名称',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `create_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
  `update_id` int(11) NOT NULL DEFAULT '0' COMMENT '修改人id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `def_recomend` */

LOCK TABLES `def_recomend` WRITE;

insert  into `def_recomend`(`id`,`name`,`create_time`,`update_time`,`create_id`,`update_id`) values (1,'首页焦点图推荐','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(2,'首页头条推荐','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(3,'栏目首页推荐','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(4,'栏目焦点图推荐','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0);

UNLOCK TABLES;

/*Table structure for table `f_attach` */

DROP TABLE IF EXISTS `f_attach`;

CREATE TABLE `f_attach` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `member_id` int(11) NOT NULL DEFAULT '0' COMMENT 'f_member.id',
  `original_filename` varchar(80) NOT NULL DEFAULT '' COMMENT '原始的文件名',
  `uuid` varchar(80) NOT NULL DEFAULT '' COMMENT '文件uuid(即不带后缀存储文件名)',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '存放路径(相对于f_config表attach.domain)',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `create_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
  `update_id` int(11) NOT NULL DEFAULT '0' COMMENT '修改人id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `f_attach` */

LOCK TABLES `f_attach` WRITE;

insert  into `f_attach`(`id`,`member_id`,`original_filename`,`uuid`,`path`,`create_time`,`update_time`,`create_id`,`update_id`) values (1,-1,'banner.jpg','5925c1ac-730c-4f61-8d72-a596e9687048','upload/001/-1/20160106/5925c1ac-730c-4f61-8d72-a596e9687048.jpg','2016-01-06 16:10:09','0000-00-00 00:00:00',-1,0),(2,-1,'1.03版登录页面-06.png','a827f3e2-6f98-4e2b-8f9f-821e8aeebbd3','upload/001/-1/20160106/a827f3e2-6f98-4e2b-8f9f-821e8aeebbd3.png','2016-01-06 16:11:30','0000-00-00 00:00:00',-1,0),(3,-1,'1.03版登录页面-06.png','5e131486-ca01-49d7-8856-4a3d079d057d','upload/001/-1/20160106/5e131486-ca01-49d7-8856-4a3d079d057d.png','2016-01-06 16:12:07','0000-00-00 00:00:00',-1,0);

UNLOCK TABLES;

/*Table structure for table `f_category` */

DROP TABLE IF EXISTS `f_category`;

CREATE TABLE `f_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `model_id` int(11) NOT NULL DEFAULT '0' COMMENT 'f_model.id',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父栏目ID',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '名称',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `keyword` varchar(255) NOT NULL DEFAULT '' COMMENT '关键词',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `list_order` int(11) NOT NULL DEFAULT '0' COMMENT '排列顺序',
  `target` varchar(20) NOT NULL DEFAULT '' COMMENT '目标 例:_BLANK.',
  `has_content_flag` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否有内容',
  `show_flag` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否显示',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `create_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
  `update_id` int(11) NOT NULL DEFAULT '0' COMMENT '修改人id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `f_category` */

LOCK TABLES `f_category` WRITE;

insert  into `f_category`(`id`,`model_id`,`parent_id`,`name`,`title`,`keyword`,`description`,`list_order`,`target`,`has_content_flag`,`show_flag`,`create_time`,`update_time`,`create_id`,`update_id`) values (1,1,0,'栏目ID:1','栏目ID:1','栏目ID:1','栏目ID:1',0,'',0,0,'2015-12-30 13:27:36','0000-00-00 00:00:00',-1,0),(2,1,1,'子栏目ID:2','子栏目ID:2','子栏目ID:2','子栏目ID:2',0,'',0,0,'2015-12-30 13:27:59','0000-00-00 00:00:00',-1,0);

UNLOCK TABLES;

/*Table structure for table `f_comment` */

DROP TABLE IF EXISTS `f_comment`;

CREATE TABLE `f_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父id',
  `content_id` int(11) NOT NULL DEFAULT '0' COMMENT '内容ID',
  `member_id` int(11) NOT NULL DEFAULT '0' COMMENT '发表人id',
  `to_member_id` int(11) NOT NULL DEFAULT '0' COMMENT '回复给的id',
  `reply_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '回复时间',
  `ip` int(11) NOT NULL DEFAULT '0' COMMENT 'ip',
  `txt` varchar(255) NOT NULL DEFAULT '' COMMENT '评论内容',
  `ups` smallint(6) NOT NULL DEFAULT '0' COMMENT '支持数',
  `downs` smallint(6) NOT NULL DEFAULT '0' COMMENT '反对数',
  `recomend_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否推荐',
  `check_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否审核',
  `create_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
  `update_id` int(11) NOT NULL DEFAULT '0' COMMENT '修改人id',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;

/*Data for the table `f_comment` */

LOCK TABLES `f_comment` WRITE;

insert  into `f_comment`(`id`,`parent_id`,`content_id`,`member_id`,`to_member_id`,`reply_time`,`ip`,`txt`,`ups`,`downs`,`recomend_flag`,`check_flag`,`create_id`,`update_id`,`create_time`,`update_time`) values (1,0,13,0,0,'2015-11-19 16:09:54',0,'xxxxxx',0,0,0,0,0,0,'2015-11-19 15:40:10','0000-00-00 00:00:00'),(2,0,13,0,0,'2015-11-21 16:09:56',0,'aaaaa',0,0,0,0,0,0,'2015-11-19 15:49:37','0000-00-00 00:00:00'),(3,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(6,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(7,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(8,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(9,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(10,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(11,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(12,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(13,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(14,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(15,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,1,0,'2015-11-19 16:18:56','0000-00-00 00:00:00'),(16,0,13,0,0,'0000-00-00 00:00:00',0,'aaaa',0,0,0,0,1,0,'2015-11-19 16:19:06','0000-00-00 00:00:00'),(17,0,13,0,0,'0000-00-00 00:00:00',0,'aaaa',0,0,0,0,1,0,'2015-11-19 16:19:57','0000-00-00 00:00:00'),(18,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(19,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(20,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(21,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(22,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(23,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(24,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(25,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(26,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(27,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(28,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(29,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(30,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(31,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(32,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(33,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(34,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(35,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(36,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(37,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(38,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(39,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(40,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(41,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(42,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(43,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(44,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(45,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(46,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(47,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(48,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(49,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(50,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(51,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(52,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(53,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(54,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(55,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(56,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(57,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(58,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(59,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(60,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(61,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(62,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(63,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(64,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(65,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(66,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(67,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(68,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(69,0,13,0,0,'0000-00-00 00:00:00',0,'',0,0,0,0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00');

UNLOCK TABLES;

/*Table structure for table `f_content` */

DROP TABLE IF EXISTS `f_content`;

CREATE TABLE `f_content` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `member_id` int(11) NOT NULL DEFAULT '0' COMMENT 'f_content.id',
  `category_id` int(11) NOT NULL DEFAULT '0' COMMENT 'f_category.id',
  `sort_id` int(11) DEFAULT NULL COMMENT 'f_sort.id(二级分类)',
  `model_id` int(11) NOT NULL DEFAULT '0' COMMENT 'f_model.id',
  `cover` varchar(255) NOT NULL DEFAULT '' COMMENT '文章封面',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `keyword` varchar(255) NOT NULL DEFAULT '' COMMENT '关键词',
  `author` varchar(50) NOT NULL DEFAULT '' COMMENT '作者',
  `source` varchar(50) NOT NULL DEFAULT '' COMMENT '来源',
  `source_url` varchar(255) NOT NULL DEFAULT '' COMMENT '来源地址',
  `relation_ids` varchar(255) NOT NULL DEFAULT '' COMMENT '相关内容, 多个用,分隔',
  `issue_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '发表时间',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `create_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
  `update_id` int(11) NOT NULL DEFAULT '0' COMMENT '修改人id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `f_content` */

LOCK TABLES `f_content` WRITE;

insert  into `f_content`(`id`,`member_id`,`category_id`,`sort_id`,`model_id`,`cover`,`title`,`keyword`,`author`,`source`,`source_url`,`relation_ids`,`issue_time`,`create_time`,`update_time`,`create_id`,`update_id`) values (2,1,2,1,1,'http://localhost:8082/box/upload/001/-1/20160106/5e131486-ca01-49d7-8856-4a3d079d057d.png','标题1','关键词1,关键词2,关键词3','作者','来源','','','2015-12-15 00:00:00','2015-12-30 13:35:57','0000-00-00 00:00:00',-1,0),(3,1,2,1,1,'http://localhost:8082/box/upload/001/-1/20160106/5e131486-ca01-49d7-8856-4a3d079d057d.png','标题1','关键词1,关键词2,关键词3','作者','来源','','','2015-12-15 00:00:00','2015-12-30 13:43:55','0000-00-00 00:00:00',-1,0),(4,1,2,1,1,'http://localhost:8082/box/upload/001/-1/20160106/5e131486-ca01-49d7-8856-4a3d079d057d.png','标题1','关键词1,关键词2,关键词3','作者','来源','','','2015-12-15 00:00:00','2015-12-30 13:44:32','0000-00-00 00:00:00',-1,0),(5,1,2,2,1,'http://localhost:8082/box/upload/001/-1/20160106/5e131486-ca01-49d7-8856-4a3d079d057d.png','标题1','关键词1,关键词2,关键词3','作者','来源','','','2015-12-15 00:00:00','2015-12-30 13:46:14','0000-00-00 00:00:00',-1,0),(9,1,2,2,1,'http://localhost:8082/box/upload/001/-1/20160106/5e131486-ca01-49d7-8856-4a3d079d057d.png','标题003','111','1212','1212','','','2016-01-21 00:00:00','2016-01-06 14:03:25','0000-00-00 00:00:00',-1,0),(10,1,2,2,1,'http://localhost:8082/box/upload/001/-1/20160106/5e131486-ca01-49d7-8856-4a3d079d057d.png','1111111111111','1111','12321','1313','','','2016-02-03 00:00:00','2016-01-06 14:40:56','0000-00-00 00:00:00',-1,0),(11,1,2,2,1,'http://localhost:8082/box/upload/001/-1/20160106/5e131486-ca01-49d7-8856-4a3d079d057d.png','@@@@@@@@@@@@','@@@@@@@@@@@@','@@@@@@@@@@@@','@@@@@@@@@@@@','','','2016-01-26 00:00:00','2016-01-06 14:56:28','0000-00-00 00:00:00',-1,0),(12,1,2,2,1,'http://localhost:8082/box/upload/001/-1/20160106/5e131486-ca01-49d7-8856-4a3d079d057d.png','554544554','554544554','554544554','554544554','','','2016-01-20 00:00:00','2016-01-06 14:58:05','0000-00-00 00:00:00',-1,0),(13,1,2,2,1,'http://localhost:8082/box/upload/001/-1/20160106/5e131486-ca01-49d7-8856-4a3d079d057d.png','!!!!!!!!!!!!','!!!!!!!!!!!!','!!!!!!!!!!!!','!!!!!!!!!!!!','','','2016-01-19 00:00:00','2016-01-06 14:58:55','0000-00-00 00:00:00',-1,0);

UNLOCK TABLES;

/*Table structure for table `f_content_article` */

DROP TABLE IF EXISTS `f_content_article`;

CREATE TABLE `f_content_article` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `summary` varchar(255) NOT NULL DEFAULT '' COMMENT '摘要',
  `txt` text NOT NULL COMMENT '内容',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `create_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
  `update_id` int(11) NOT NULL DEFAULT '0' COMMENT '修改人id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `f_content_article` */

LOCK TABLES `f_content_article` WRITE;

insert  into `f_content_article`(`id`,`summary`,`txt`,`create_time`,`update_time`,`create_id`,`update_id`) values (2,'摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要','<p>内容内容内容内容内容内容内容内容内容内容内容内容内容</p>','2015-12-30 13:35:57','0000-00-00 00:00:00',-1,0),(3,'摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要','<p>内容内容内容内容内容内容内容内容内容内容内容内容内容</p>','2015-12-30 13:43:55','0000-00-00 00:00:00',-1,0),(4,'摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要','<p>内容内容内容内容内容内容内容内容内容内容内容内容内容</p>','2015-12-30 13:44:32','0000-00-00 00:00:00',-1,0),(5,'摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要','<p>内容内容内容内容内容内容内容内容内容内容内容内容内容</p>','2015-12-30 13:46:14','0000-00-00 00:00:00',-1,0),(9,'1212','<p>121212121212</p>','2016-01-06 14:03:25','0000-00-00 00:00:00',-1,0),(10,'123213213213','<p>123213213</p>','2016-01-06 14:40:57','0000-00-00 00:00:00',-1,0),(11,'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@','<p>@@@@@@@@@@@@</p>','2016-01-06 14:56:28','0000-00-00 00:00:00',-1,0),(12,'554544554554544554554544554554544554','<p>554544554</p>','2016-01-06 14:58:05','0000-00-00 00:00:00',-1,0),(13,'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!','<p>!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</p>','2016-01-06 14:58:55','2016-01-06 16:12:24',-1,-1);

UNLOCK TABLES;

/*Table structure for table `f_member` */

DROP TABLE IF EXISTS `f_member`;

CREATE TABLE `f_member` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `username` varchar(50) NOT NULL DEFAULT '' COMMENT '用户名',
  `mobile` varchar(15) NOT NULL DEFAULT '' COMMENT '手机号',
  `email` varchar(50) NOT NULL DEFAULT '' COMMENT '邮箱',
  `password` varchar(255) NOT NULL DEFAULT '' COMMENT '密码',
  `face` varchar(255) NOT NULL DEFAULT '' COMMENT '头像',
  `name` varchar(20) NOT NULL DEFAULT '' COMMENT '姓名',
  `nation` varchar(20) NOT NULL DEFAULT '' COMMENT '民族',
  `origin_place` varchar(40) NOT NULL DEFAULT '' COMMENT '籍贯',
  `home_address` varchar(40) NOT NULL DEFAULT '' COMMENT '家庭住址',
  `birthday` date NOT NULL DEFAULT '0000-00-00' COMMENT '生日',
  `sex` tinyint(4) NOT NULL DEFAULT '0' COMMENT '姓名',
  `sign` varchar(25) NOT NULL DEFAULT '' COMMENT '用户签名',
  `bind_mobile_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '绑定手机号标识',
  `bind_email_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '绑定邮箱标识',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '用户状态 0:正常 1:查封 2:待审核',
  `reg_ip` varchar(20) NOT NULL DEFAULT '' COMMENT '注册ip',
  `reg_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '注册时间',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `create_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
  `update_id` int(11) NOT NULL DEFAULT '0' COMMENT '修改人id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_uni_username` (`username`),
  UNIQUE KEY `idx_uni_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `f_member` */

LOCK TABLES `f_member` WRITE;

insert  into `f_member`(`id`,`username`,`mobile`,`email`,`password`,`face`,`name`,`nation`,`origin_place`,`home_address`,`birthday`,`sex`,`sign`,`bind_mobile_flag`,`bind_email_flag`,`status`,`reg_ip`,`reg_time`,`create_time`,`update_time`,`create_id`,`update_id`) values (1,'eacdy','15850770072','eacdy0000@126.com','$2a$04$vt2kTfWPKsnfDspWtCz90uZwV8H7vpV5bBeYIok1IImnYWnd3NKU2','','','','','','0000-00-00',0,'宣言: 我就是我',1,1,0,'0:0:0:0:0:0:0:1','2016-01-06 16:03:04','2016-01-06 16:03:04','2016-01-06 16:03:16',-1,-1);

UNLOCK TABLES;

/*Table structure for table `f_member_log` */

DROP TABLE IF EXISTS `f_member_log`;

CREATE TABLE `f_member_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `member_id` int(11) NOT NULL DEFAULT '0' COMMENT 'f_member',
  `operation` tinyint(4) NOT NULL DEFAULT '0' COMMENT '操作 1:登陆 2:注册 3:忘记密码 4:修改密码',
  `time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '操作时间',
  `ip` varchar(20) NOT NULL DEFAULT '' COMMENT '操作ip',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `create_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
  `update_id` int(11) NOT NULL DEFAULT '0' COMMENT '修改人id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `f_member_log` */

LOCK TABLES `f_member_log` WRITE;

insert  into `f_member_log`(`id`,`member_id`,`operation`,`time`,`ip`,`create_time`,`update_time`,`create_id`,`update_id`) values (1,1,0,'2016-01-06 16:03:23','0:0:0:0:0:0:0:1','2016-01-06 16:03:23','0000-00-00 00:00:00',1,0);

UNLOCK TABLES;

/*Table structure for table `f_sort` */

DROP TABLE IF EXISTS `f_sort`;

CREATE TABLE `f_sort` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id 二级分类id',
  `parent_id` int(11) DEFAULT NULL COMMENT '父id',
  `category_id` int(11) NOT NULL DEFAULT '0' COMMENT 'f_category.id',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `keyword` varchar(255) NOT NULL DEFAULT '' COMMENT '关键词',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `list_order` smallint(6) NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `create_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
  `update_id` int(11) NOT NULL DEFAULT '0' COMMENT '修改人id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `f_sort` */

LOCK TABLES `f_sort` WRITE;

insert  into `f_sort`(`id`,`parent_id`,`category_id`,`name`,`title`,`keyword`,`description`,`list_order`,`create_time`,`update_time`,`create_id`,`update_id`) values (1,NULL,2,'分类:ID1','分类:ID1','分类:ID1','分类:ID1',0,'2015-12-30 13:28:24','0000-00-00 00:00:00',-1,0),(2,1,2,'子分类:ID2','子分类:ID2','子分类:ID2','子分类:ID2',0,'2015-12-30 13:28:40','0000-00-00 00:00:00',-1,0);

UNLOCK TABLES;

/*Table structure for table `f_tag` */

DROP TABLE IF EXISTS `f_tag`;

CREATE TABLE `f_tag` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(40) NOT NULL DEFAULT '' COMMENT '标签名称',
  `default_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否默认 0否 1是',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `create_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建人id',
  `update_id` int(11) NOT NULL DEFAULT '0' COMMENT '修改人id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

/*Data for the table `f_tag` */

LOCK TABLES `f_tag` WRITE;

insert  into `f_tag`(`id`,`name`,`default_flag`,`create_time`,`update_time`,`create_id`,`update_id`) values (1,'默认标签1',1,'0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(2,'默认标签2',1,'0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(3,'默认标签3',1,'0000-00-00 00:00:00','0000-00-00 00:00:00',0,0),(4,'111111111111111',0,'2015-12-29 19:20:56','0000-00-00 00:00:00',-1,0),(5,'222222222',0,'2015-12-29 21:29:23','0000-00-00 00:00:00',-1,0),(6,'1111111111',0,'2015-12-30 11:56:07','0000-00-00 00:00:00',-1,0),(10,'标签3',0,'2015-12-30 13:35:57','0000-00-00 00:00:00',-1,0),(11,'标签1',0,'2015-12-30 13:35:57','0000-00-00 00:00:00',-1,0),(12,'标签2',0,'2015-12-30 13:35:57','0000-00-00 00:00:00',-1,0),(16,'12121',0,'2016-01-06 14:03:25','0000-00-00 00:00:00',-1,0),(17,'111',0,'2016-01-06 14:40:57','0000-00-00 00:00:00',-1,0),(18,'@@@@@@@@@@@@',0,'2016-01-06 14:56:28','0000-00-00 00:00:00',-1,0),(19,'554544554',0,'2016-01-06 14:58:05','0000-00-00 00:00:00',-1,0),(23,'!!!!!!!!!!!!',0,'2016-01-06 15:19:00','0000-00-00 00:00:00',-1,0),(24,'@@@@@@@@@@@@@@',0,'2016-01-06 15:59:29','0000-00-00 00:00:00',-1,0);

UNLOCK TABLES;

/*Table structure for table `mid_content_recomend` */

DROP TABLE IF EXISTS `mid_content_recomend`;

CREATE TABLE `mid_content_recomend` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `content_id` int(11) NOT NULL DEFAULT '0' COMMENT 'f_content.id',
  `recomend_id` int(11) NOT NULL DEFAULT '0' COMMENT 'f_recomend.id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

/*Data for the table `mid_content_recomend` */

LOCK TABLES `mid_content_recomend` WRITE;

insert  into `mid_content_recomend`(`id`,`content_id`,`recomend_id`) values (1,2,4),(2,3,4),(3,3,0),(4,3,0),(5,4,0),(6,4,0),(7,4,0),(8,4,0),(9,5,4),(10,5,3),(11,5,1),(12,5,2),(20,13,4),(21,13,2),(22,13,1),(23,13,3);

UNLOCK TABLES;

/*Table structure for table `mid_content_tag` */

DROP TABLE IF EXISTS `mid_content_tag`;

CREATE TABLE `mid_content_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `content_id` int(11) NOT NULL DEFAULT '0' COMMENT 'f_content.id',
  `tag_id` int(11) NOT NULL DEFAULT '0' COMMENT 'f_tag.id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

/*Data for the table `mid_content_tag` */

LOCK TABLES `mid_content_tag` WRITE;

insert  into `mid_content_tag`(`id`,`content_id`,`tag_id`) values (1,2,12),(2,2,10),(3,2,11),(4,3,12),(5,3,11),(6,3,10),(7,4,12),(8,4,11),(9,4,10),(10,5,11),(11,5,12),(12,5,10),(16,9,16),(17,10,17),(18,11,18),(19,12,19),(29,13,24);

UNLOCK TABLES;

/*Table structure for table `persistent_logins` */

DROP TABLE IF EXISTS `persistent_logins`;

CREATE TABLE `persistent_logins` (
  `username` varchar(64) NOT NULL,
  `series` varchar(64) NOT NULL,
  `token` varchar(64) NOT NULL,
  `last_used` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`series`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `persistent_logins` */

LOCK TABLES `persistent_logins` WRITE;

insert  into `persistent_logins`(`username`,`series`,`token`,`last_used`) values ('itmuch0000@126.com','vMkWwWTc7r2En+exf5KksQ==','Ugem3SREW/V9PMs4L6Mwdg==','2015-12-23 13:25:22');

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
