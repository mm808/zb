<#macro sList data topId>
    <#list data as item>
        <#if (item.parentId)??>
            <#local parentId=item.parentId>
        <#else >
            <#local parentId=0/>
        </#if>
        <#if topId == parentId>
        <tr class="treegrid-${(item.id)!} <#if item.parentId!=0>treegrid-parent-${(item.parentId)!}</#if>">
            <td>${(item.id)!}<input type="hidden" name="ids" value="${item.id?c}"/></td>
            <td>${(item.name)!}</td>
            <td>${(item.title)!}</td>
            <td><a href="${url!}/${(item.id)!}/1" target="_blank">访问</a></td>
            <td>${((item.showFlag==true)?string('是','否'))!}</td>
            <td>
                <a class="label label-success" href="javascript:"
                   onclick="edit(${(item.id)!}, '${ctx}${adminPath}/sort/edit/${(item.id)!}');">修改</a>
                <a class="label label-danger" href="javascript:"
                   onclick="del(${(item.id)!}, '${ctx}${adminPath}/sort/del/${(item.id)!}')">删除</a>
                <#if (item.parentId == 0)>
                    <a class="label label-primary" href="javascript:"
                       onclick="add(${(item.id)!}, '${ctx}${adminPath}/sort/add');">添加子分类</a>
                </#if>
            </td>
        </tr>
            <@sList data=data topId=item.id/>
        <#else>
        </#if>
    </#list>
</#macro>
<#-- 用于后台递归生成栏目列表 -->