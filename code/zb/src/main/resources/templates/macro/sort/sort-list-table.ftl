<#macro sTable data topId>

<table class="table table-striped">
    <tr>
        <th>id</th>
        <th>栏目名称</th>
        <th>标题</th>
        <th>访问</th>
        <th>是否可见</th>
        <th>操作</th>
    </tr>
    <#include "sort-list-recursion.ftl">
    <@sList data topId />

</table>
</#macro>
<#-- 用于后台递归生成栏目列表，直接生成table -->