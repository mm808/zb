<#macro optgroup data selected>
    <#if data??>
        <#list data as item>
            <#if (item.children)??>
                <#local children=item.children>
                <#if (children)??>
                <optgroup label="${(item.name)!}">
                    <#list children as child>
                        <option value="${(child.id)!}" <#if child.id==selected>selected</#if>>${(child.name)!}</option>
                    </#list>

                </optgroup>
                </#if>
            <#else>
            <option value="${(item.id)!}" <#if item.id==selected>selected</#if>>${(item.name)!}</option>
            </#if>
        </#list>
    </#if>
</#macro>

<#macro selectGroup data name selected=0 className="form-control" id="" message="请选择">
<select name="${name}" name="${name}" class="${className}" id="${id}">
    <option value="0" <#if selected == 0>selected</#if>>${message}</option>

    <@optgroup data selected></@optgroup>
</select>
</#macro>
