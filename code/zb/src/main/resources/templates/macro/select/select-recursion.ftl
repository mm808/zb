<#macro option data pid indent selected>
    <#if data??>
        <#local temp = indent+1 />
        <#list data as item>
            <#if (item.parentId)??>
                <#local parentId=item.parentId>
            <#else>
                <#local parentId=0/>
            </#if>
            <#if pid==parentId>
            <option value="${item.id}" <#if selected==item.id>selected</#if>>
                <#if temp gte 2>
                    <#list 1..temp as x>
                        <#if x gt 2>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                        </#if>
                    </#list>
                    &nbsp;&nbsp;&nbsp;&nbsp;┠
                </#if>
            ${item.name}
            </option>
                <@option data=data pid=item.id indent=temp selected=selected/>
            <#else>
            </#if>
        </#list>
    </#if>
</#macro>

<#macro select data pid indent name selected=0 className="form-control" id="" message="请选择">
<select name="${name}" class="${className}" id="${id}">
    <option value="0" <#if selected == 0>selected</#if>>${message}</option>
    <@option data pid indent selected></@option>
</select>
</#macro>


<#-- 使用示例:
<@cata categories 0 0/> -->
<#--生成select-->