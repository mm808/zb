<#macro cList data topId>
    <#list data as item>
        <#if (item.parentId)??>
            <#local parentId=item.parentId>
        <#else >
            <#local parentId=0/>
        </#if>
        <#if topId == parentId>
        <tr class="treegrid-${(item.id)!} <#if item.parentId!=0>treegrid-parent-${(item.parentId)!}</#if>">
            <td>${(item.id)!}<input type="hidden" name="ids" value="${item.id?c}"/></td>
            <td>${(item.name)!}</td>
            <td>${(item.title)!}</td>
            <td><a href="${url!}/${(item.id)!}/1" target="_blank">访问</a></td>
            <td>${((item.showFlag==true)?string('是','否'))!}</td>
            <td>
                <a class="label label-primary" href="javascript:" onclick="add(${(item.id)!}, '${ctx}${adminPath}/category/add');">添加子栏目</a>
                <a class="label label-success" href="javascript:" onclick="edit(${(item.id)!}, '${ctx}${adminPath}/category/edit/${(item.id)!}');">修改</a>
                <a class="label label-danger" href="javascript:" onclick="del(${(item.id)!}, '${ctx}${adminPath}/category/del/')">删除</a>
                <a class="label label-warning" href="${ctx}${adminPath}/sort/${(item.id)!}" target="_BLANK">二级分类管理</a>
            </td>
        </tr>
            <@cList data=data topId=item.id/>
        <#else>
        </#if>
    </#list>
</#macro>
<#-- 用于后台递归生成栏目列表 -->