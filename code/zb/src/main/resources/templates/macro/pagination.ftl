<#macro pagination list url showPages=5>
<ul class="pagination pagination-sm">
    <#if !(list.first)>
        <li>
            <a href="${url}/${list.number}" aria-label="Previous">
                <span aria-hidden="true">上一页</span>
            </a>
        </li>
    </#if>
    <#local currNo=list.number+1/>
    <#local totalPages=list.totalPages/>

    <#if totalPages <= showPages>
    <#-- 如果总页码小于显示的页码, 直接显示所有页码 -->
        <#list 1..totalPages as page>
            <#if page==currNo>
                <li class="active">
                    <a>${page }</a>
                </li>
            <#else>
                <li>
                    <a href="${url}/${page}">${page}</a>
                </li>
            </#if>
        </#list>
    <#else>
    <#-- 总页码大于显示的页码 -->
        <#local temp1=((currNo-1)/showPages)?floor/>
        <#local temp2=(totalPages/showPages)?floor/>
        <#local start=temp1*showPages +1/>
        <#local end=(temp1+1)                                                                                                                                                                                                                                                                                                                                                                                                                                                      *showPages/>
        <#if temp1<temp2>
            <#list start..end as page>
                <#if page==currNo>
                    <li class="active">
                        <a>${page }</a>
                    </li>
                <#else>
                    <li>
                        <a href="${url}/${page}">${page}</a>
                    </li>
                </#if>
            </#list>
        <#-- 展示最后的几页 -->
        <#elseif temp1==temp2>
            <#list start..totalPages as page>
                <#if page==currNo>
                    <li class="active">
                        <a>${page }</a>
                    </li>
                <#else>
                    <li>
                        <a href="${url}/${page}">${page}</a>
                    </li>
                </#if>
            </#list>
        </#if>
    </#if>

    <#if !(list.last)>
        <li>
            <a href="${url}/${currNo+1}" aria-label="Next">
                <span aria-hidden="true">下一页</span>
            </a>
        </li>
    </#if>
</ul>
</#macro>
<!-- 分页控件,其中list是 对象; url是翻页的地址 -->
