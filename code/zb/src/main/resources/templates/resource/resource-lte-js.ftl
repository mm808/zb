<!-- jQuery 2.1.4 -->
<script src="${ctx}/back/scripts/AdminLTE-2.3.2/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="${ctx}/back/scripts/AdminLTE-2.3.2/plugins/jQueryUI/jquery-ui.js"></script>
<script>
    //Make the dashboard widgets sortable Using jquery UI
    $(".connectedSortable").sortable({
        placeholder: "sort-highlight",
        connectWith: ".connectedSortable",
        handle: ".box-header, .nav-tabs",
        forcePlaceholderSize: true,
        zIndex: 999999
    });
</script>
<!-- Bootstrap 3.3.5 -->
<script src="${ctx}/back/scripts/AdminLTE-2.3.2/bootstrap/js/bootstrap.js"></script>
<!-- FastClick -->
<script src="${ctx}/back/scripts/AdminLTE-2.3.2/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${ctx}/back/scripts/AdminLTE-2.3.2/dist/js/app.js"></script>

<!-- 以下是自己加的js -->
<script type="text/javascript" src="${ctx}/back/scripts/bootstrap-table/1.9.1/dist/bootstrap-table.js"></script>
<script type="text/javascript" src="${ctx}/back/scripts/sweetalert/1.1.1/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="${ctx}/back/scripts/jquery-treegrid/0.3.0/js/jquery.treegrid.min.js"></script>
<script type="text/javascript" src="${ctx}/back/scripts/jquery-treegrid/0.3.0/js/jquery.treegrid.bootstrap3.js"></script>
<script type="text/javascript" src="${ctx}/back/scripts/jquery.serializeJSON/2.6.2/jquery.serializejson.js"></script>
<script type="text/javascript" src="${ctx}/back/scripts/jquery-validation/1.14.0/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="${ctx}/back/scripts/bootstrap-select/1.7.5/dist/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="${ctx}/back/scripts/zTree_v3/js/jquery.ztree.core-3.5.min.js"></script>
<script type="text/javascript" src="${ctx}/back/scripts/bootstrap-datepicker-1.5.0/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="${ctx}/back/scripts/bootstrap-fileinput/4.2.7/js/fileinput.min.js"></script>
<script type="text/javascript" src="${ctx}/back/scripts/bootstrap-fileinput/4.2.7/js/fileinput_locale_zh.js"></script>

<script type="text/javascript">
    $(function () {
        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");
        $(document).ajaxSend(function (e, xhr, options) {
            xhr.setRequestHeader(header, token);
        });
    });
</script>
