
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="${ctx}/back/scripts/AdminLTE-2.3.2/bootstrap/css/bootstrap.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="${ctx}/back/scripts/Font-Awesome/4.4.0/css/font-awesome.css">
<!-- Ionicons -->
<link rel="stylesheet" href="${ctx}/back/scripts/ionicons/2.0.1/css/ionicons.css">
<!-- Theme style -->
<link rel="stylesheet" href="${ctx}/back/scripts/AdminLTE-2.3.2/dist/css/AdminLTE.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="${ctx}/back/scripts/AdminLTE-2.3.2/dist/css/skins/_all-skins.min.css">
<!-- iCheck -->
<link rel="stylesheet" href="${ctx}/back/scripts/AdminLTE-2.3.2/plugins/iCheck/flat/blue.css">
<!-- Morris chart -->
<link rel="stylesheet" href="${ctx}/back/scripts/AdminLTE-2.3.2/plugins/morris/morris.css">
<!-- jvectormap -->
<link rel="stylesheet" href="${ctx}/back/scripts/AdminLTE-2.3.2/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Date Picker -->
<link rel="stylesheet" href="${ctx}/back/scripts/AdminLTE-2.3.2/plugins/datepicker/datepicker3.css">
<!-- Daterange picker -->
<link rel="stylesheet" href="${ctx}/back/scripts/AdminLTE-2.3.2/plugins/daterangepicker/daterangepicker-bs3.css">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="${ctx}/back/scripts/AdminLTE-2.3.2/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

<!-- 以下是自己加的css -->
<link href="${ctx}/back/scripts/bootstrap-table/1.9.1/dist/bootstrap-table.css" rel="stylesheet">
<link href="${ctx}/back/scripts/sweetalert/1.1.1/dist/sweetalert.css" rel="stylesheet">
<link href="${ctx}/back/scripts/jquery-treegrid/0.3.0/css/jquery.treegrid.css" rel="stylesheet">
<link href="${ctx}/back/scripts/jquery-validation/1.14.0/demo/css/cmxform.css" rel="stylesheet">
<link href="${ctx}/back/scripts/bootstrap-select/1.7.5/dist/css/bootstrap-select.min.css" rel="stylesheet">
<link href="${ctx}/back/scripts/zTree_v3/css/zTreeStyle/zTreeStyle.css" rel="stylesheet">
<link href="${ctx}/back/scripts/bootstrap-datepicker-1.5.0/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href="${ctx}/back/scripts/bootstrap-fileinput/4.2.7/css/fileinput.min.css" rel="stylesheet">
<script type="text/javascript">
    window.UEDITOR_HOME_URL = '${ctx}/back/scripts/ueditor-1.4.3.1/';
    COMMON_PATH = '${ctx}${commonPath}';
    ADMIN_PATH = '${ctx}${adminPath}';
    FRONT_PATH = '${ctx}';
    CSRF_PARAM = '_csrf';
    CSRF_TOKEN = '${_csrf.token}';
</script>
<script type="text/javascript" src="${ctx}/back/scripts/ueditor-1.4.3.1/ueditor.config.js"></script>
<script type="text/javascript" src="${ctx}/back/scripts/ueditor-1.4.3.1/ueditor.all.min.js"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
