<div class="main_left">
	<!--导航开始-->
	<div class="nav_wrapper">
		<ul>
			<li><a class="li1" href="javascript:">个人中心</a></li>
			<li class="selected"><a href="${ctx}/member">个人中心</a></li>
			
			<li><a class="li2" href="javascript:">我的文章</a></li>
			<li><a href="${ctx}/member/article/add">发布文章</a></li>
			<li><a href="${ctx}/member/article/1">已发布文章</a></li>
			
			<li><a class="li3" href="javascript:">我的视频</a></li>
			<li><a href="person-9.html">发布视频</a></li>
			<li><a href="person-8.html">已发布视频</a></li>
			
			<li><a class="li4" href="#">我的消息</a></li>
			<li><a href="person-7.html">我的发布</a></li>
			<li><a href="person-6.html">回复我的(<span style="color: #FF0000;">5</span>)</a></li>
			
			<li><a class="li5">我的资料</a></li>
			<li><a href="${ctx}/member/info">个人信息</a></li>
			<li><a href="${ctx}/member/change-pwd">修改密码</a></li>
		</ul>
	</div>
	<!--导航结束-->
</div>