<#include "/layout/front/default/default-header.ftl">
<#include "/layout/front/default/default-footer.ftl">
<#assign ctx=request.contextPath>
<#macro layout title header=defaultHeader footer=defaultFooter>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>

    <title>${title}</title>
    <link href="${ctx}/front/scripts/css/public/reset.css" rel="stylesheet" type="text/css"/>
    <link href="${ctx}/front/scripts/css/index/index.css" rel="stylesheet" type="text/css"/>
    <link href="${ctx}/front/scripts/js/lib/Kslider/css/css.css" rel="stylesheet" type="text/css"/>
    <link href="${ctx}/front/scripts/js/lib/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="${ctx}/front/scripts/css/lr/lr.css" rel="stylesheet" />

    <script src="${ctx}/front/scripts/js/lib/jquery-1.11.1.min.js"></script>
    <script src="${ctx}/front/scripts/js/lib/Kslider/js/jquery.Kslider-1.2.js"></script>
    <script src="${ctx}/front/scripts/js/lib/jquery.rotate.min.js"></script>
</head>
<body class="body_bg">
    <@header/>
    <#nested>
    <@footer/>
</body>
<script src="${ctx}/front/scripts/js/index/index.js"></script>
</html>
</#macro>
