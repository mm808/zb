<#include "/layout/front/category/category-header.ftl">
<#include "/layout/front/category/category-footer.ftl">
<#assign ctx=request.contextPath>
<#macro articleLayout title header=categoryHeader footer=categoryFooter>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
    <title>${title}</title>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <link href="${ctx}/front/scripts/lib/lunbo1/css/focusStyle.css" rel="stylesheet">
    <link href="${ctx}/front/scripts/css/public/reset.css" rel="stylesheet" type="text/css"/>
    <link href="${ctx}/front/scripts/js/lib/Kslider/css/css.css" rel="stylesheet" type="text/css"/>
    <link href="${ctx}/front/scripts/js/lib/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="${ctx}/front/scripts/css/index/index.css" rel="stylesheet" type="text/css"/>
    <link href="${ctx}/front/scripts/css/activity/activity.css" rel="stylesheet" type="text/css"/>
    <link href="${ctx}/front/scripts/css/wenzhang/wenzhang.css" rel="stylesheet" type="text/css"/>
    <script src="${ctx}/front/scripts/js/lib/jquery-1.11.1.min.js"></script>
    <script src="${ctx}/front/scripts/js/lib/Kslider/js/jquery.Kslider-1.2.js"></script>
    <script src="${ctx}/front/scripts/js/lib/jquery.rotate.min.js"></script>
    <script src="${ctx}/front/scripts/js/artic/js/script.js"></script>
    <link href="${ctx}/front/scripts/js/artic/css/index.css" rel="stylesheet"/>
    <link href="${ctx}/front/scripts/css/article/main.css" rel="stylesheet"/>

    <script type="text/javascript">
        window.UEDITOR_HOME_URL = '${ctx}/static/scripts/back/ueditor-1.4.3.1/';
        CTX = '${ctx}';
        COMMON_PATH = '${ctx}${commonPath}';
        ADMIN_PATH = '${ctx}${adminPath}';
        FRONT_PATH = '${ctx}';
        STATIC = '${ctx}/static/';
    </script>
</head>
<body class="body_bg">
    <@header/>
    <#nested>
    <@footer/>
</body>
<script src="${ctx}/front/scripts/js/index/index.js"></script>
</html>
</#macro>
