<#macro articleFooter>
<!--底部开始-->
<div class="footer">
    <div class="footer_header">
        <ul>
            <li>关于我们</li>
            <li>加入我们</li>
            <li style="border-right:1px solid #000000;">常见问题</li>
        </ul>
    </div>
    <div class="footer_content">
        <div class="friendlink">友情链接</div>
        <div class="coopwebsite">
            <ul>
                <li>百度</li>
                <li>阿里</li>
                <li>女人街</li>
                <li>人人网</li>
                <li>蘑菇街</li>
                <li>百度</li>
                <li>阿里</li>
                <li>女人街</li>
                <li>人人网</li>
                <li>蘑菇街</li>
            </ul>
        </div>
        <div class="ownwebsite">
            Copy@盒子科技-xx盒子科技有限公司 &nbsp;京ICP备12013432号-1&nbsp;电话：010-66666666
            <a href="#"><img src="http://icon.cnzz.com/img/pic.gif" style="margin-left:50px; margin-top:-5px;"/></a>
        </div>
    </div>
</div>
<!--底部结束-->
<!--浮动导航-->
<div class="floatnav">
    <ul>
        <li class="f1">
            <div class="right_overlap"> 文章</div>
        </li>
        <li class="f2">
            <div class="right_overlap"> 视频</div>
        </li>
        <li class="f3">
            <div class="right_overlap"> 自习</div>
        </li>
        <li class="f4">
            <div class="right_overlap"> 产品</div>
        </li>
        <li class="f5">
            <div class="right_overlap"> 测评</div>
        </li>
        <li class="f6">
            <div class="right_overlap"> 活动</div>
        </li>
        <li class="f7">
            <div class="right_overlap"> 小组</div>
        </li>
        <li class="f8">
            <div class="right_overlap" style="background:#FF9900; filter:alpha(opacity=50);
      -moz-opacity:0.5;  
      -khtml-opacity: 0.5;  
      opacity: 0.5;  display:none;"></div>
        </li>
    </ul>
</div>
<!--浮动导航结束-->
</#macro>