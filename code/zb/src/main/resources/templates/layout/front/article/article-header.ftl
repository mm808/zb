<#assign security=JspTaglibs["http://www.springframework.org/security/tags"]>
<#macro articleHeader>
<div class="header">
    <div class="header_content">
        <a href="${ctx}/">
            <div class="left"></div>
        </a>
        <div class="middle">
            <ul>
                <li><a href="${ctx}/">首页</a></li>
                <li><a href="${ctx}/category/2/1">文章</a></li>
                <li><a href="#">视频</a></li>
                <li><a href="#">自习室</a></li>
                <li><a href="#">产品</a></li>
                <li><a href="#">测评</a></li>
                <li><a href="#">活动</a></li>
                <li><a href="#">兴趣小组</a></li>
            </ul>
        </div>
        <div class="right">
            <@security.authorize access="hasRole('ROLE_ANONYMOUS')" var="isAnon" />
            <#if isAnon?? && isAnon>
                <a class="login_btn" href="${ctx}/login">登录</a>
                <a class="login_btn" href="${ctx}/reg">注册</a>
            <#else>
                <!--顶部下拉开始-->
                <a href="#">
                    <div class="nav_drop">
                        <div class="div1">
                            <img src="${ctx}/front/images/123456.jpg"/>
                        </div>
                        <div class="div2"><@security.authentication property="name" /></div>
                        <div class="div3" onclick="drop(this);">
                            <div id="drop_panel" class="drop_panel">
                                <img src="${ctx}/front/images/xiaobiao.png"/>
                                <ul style="margin-bottom:0px;">
                                    <li><a class="a1" href="${ctx}/member">个人中心</a></li>
                                    <li>
                                        <a class="a2" href="#">
                                            我的消息<span style="color:#FF0000;">(1000)</span>
                                        </a>
                                    </li>
                                    <li><a class="a3" href="${ctx}/member/change-pwd">修改密码</a></li>
                                    <li><a class="a3" href="${ctx}${adminPath}/category/article">后台管理</a></li>
                                    <li>
                                        <form action="${ctx }/logout" method="post" id="logoutForm">
                                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        </form>
                                        <a class="a4" href="#"
                                           onclick="document.getElementById('logoutForm').submit();">退出</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </a>
                <!--顶部下拉结束-->
            </#if>
        </div>
    </div>
</div>
</#macro>