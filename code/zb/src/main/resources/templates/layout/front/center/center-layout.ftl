<#include "/layout/front/center/center-header.ftl">
<#include "/layout/front/center/center-footer.ftl">
<#assign ctx=request.contextPath>
<#macro layout title header=centerHeader footer=centerFooter>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>

    <title>${title}</title>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <link href="${ctx}/front/scripts/css/public/reset.css" rel="stylesheet" type="text/css"/>
    <link href="${ctx}/front/scripts/css/index/index.css" rel="stylesheet" type="text/css"/>
    <link href="${ctx}/front/scripts/js/lib/Kslider/css/css.css" rel="stylesheet" type="text/css"/>
    <link href="${ctx}/front/scripts/js/lib/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="${ctx}/front/scripts/css/person/person.css" rel="stylesheet" type="text/css"/>
    <link href="${ctx}/front/scripts/css/person/personInfo.css" rel="stylesheet" type="text/css"/>
    <link href="${ctx}/front/scripts/js/lib/datepicker/css/bootstrap-datepicker.css" rel="stylesheet" type="text/css"/>
    <link href="${ctx}/front/scripts/js/lib/webuploader/webuploader.css" rel="stylesheet" type="text/css">
    <link href="${ctx}/front/scripts/lib/upload/css/fileinput.css" rel="stylesheet"/>
    <link href="${ctx}/back/scripts/bootstrap-fileinput/4.2.7/css/fileinput.min.css" rel="stylesheet">

    <script src="${ctx}/front/scripts/js/lib/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="${ctx}/front/scripts/js/lib/Kslider/js/jquery.Kslider-1.2.js" type="text/javascript"></script>
    <script src="${ctx}/front/scripts/js/lib/jquery.rotate.min.js" type="text/javascript"></script>
    <script src="${ctx}/front/scripts/js/lib/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="${ctx}/front/scripts/js/lib/datepicker/locales/bootstrap-datepicker.zh-CN.min.js" type="text/javascript"></script>
    <script src="${ctx}/front/scripts/js/lib/webuploader/webuploader.js" type="text/javascript"></script>
    <script src="${ctx}/front/scripts/js/person/personInfo.js" type="text/javascript"></script>
    <script src="${ctx}/back/scripts/bootstrap-fileinput/4.2.7/js/fileinput.min.js" type="text/javascript"></script>
    <script src="${ctx}/back/scripts/bootstrap-fileinput/4.2.7/js/fileinput_locale_zh.js" type="text/javascript"></script>
    <script src="${ctx}/front/scripts/js/lib/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        window.UEDITOR_HOME_URL = '${ctx}/back/scripts/ueditor-1.4.3.1/';
        CTX = '${ctx}';
        COMMON_PATH = '${ctx}${commonPath}';
        ADMIN_PATH = '${ctx}${adminPath}';
        FRONT_PATH = '${ctx}';
        CSRF_PARAM = '_csrf';
        CSRF_TOKEN = '${_csrf.token}';
        STATIC = '${ctx}/front/scripts/';
    </script>

    <script type="text/javascript" src="${ctx}/back/scripts/ueditor-1.4.3.1/ueditor.config.js"></script>
    <script type="text/javascript" src="${ctx}/back/scripts/ueditor-1.4.3.1/ueditor.all.min.js"></script>

    <link href="${ctx}/back/scripts/jquery-validation/1.14.0/demo/css/cmxform.css" rel="stylesheet">
    <script src="${ctx}/back/scripts/jquery-validation/1.14.0/dist/jquery.validate.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            var token = $("meta[name='_csrf']").attr("content");
            var header = $("meta[name='_csrf_header']").attr("content");
            $(document).ajaxSend(function (e, xhr, options) {
                xhr.setRequestHeader(header, token);
            });
        });
    </script>
</head>
<body class="body_bg">
    <@header/>
<!--内容开始-->
<div class="main">
    <#include "/layout/front/center/center-aside.ftl"/>
    <#nested>
</div>
<!--内容结束-->
    <@footer/>
</body>
<script src="${ctx}/front/scripts/js/index/index.js"></script>
</html>
</#macro>
