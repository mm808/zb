<#include "/layout/front/article/article-header.ftl">
<#include "/layout/front/article/article-footer.ftl">
<#assign ctx=request.contextPath>
<#macro articleLayout title header=articleHeader footer=articleFooter>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
    <title>${title}</title>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <link href="${ctx}/front/scripts/css/public/reset.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/front/scripts/css/index/index.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/front/scripts/js/lib/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/front/scripts/js/lib/Kslider/css/css.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/front/scripts/css/lib/slider.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/front/scripts/css/product/productInfo.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/front/scripts/css/manhuaHtmlArea.1.0.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/front/scripts/css/comment.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/front/scripts/css/video/video.css" rel="stylesheet" />


    <script src="${ctx}/front/scripts/js/lib/jquery-1.11.1.min.js"></script>
    <script src="${ctx}/front/scripts/js/lib/Kslider/js/jquery.Kslider-1.2.js"></script>
    <script src="${ctx}/front/scripts/js/lib/jquery.rotate.min.js"></script>
    <script src="${ctx}/front/scripts/js/slides.min.jquery.js"></script>
    <script src="${ctx}/front/scripts/js/manhuaHtmlArea.1.0.js" type="text/javascript"></script>
    <script src="${ctx}/front/scripts/js/comment.js"></script>
    <script src="${ctx}/front/scripts/js/product/productInfo.js"></script>

    <script type="text/javascript">
        window.UEDITOR_HOME_URL = '${ctx}/front/scripts/scripts/back/ueditor-1.4.3.1/';
        CTX = '${ctx}';
        COMMON_PATH = '${ctx}${commonPath}';
        ADMIN_PATH = '${ctx}${adminPath}';
        FRONT_PATH = '${ctx}';
        STATIC = '${ctx}/front/scripts/';
    </script>


</head>
<body class="body_bg">
    <@header/>
    <#nested>
    <@footer/>
</body>
<script src="${ctx}/front/scripts/js/index/index.js"></script>
</html>
</#macro>
