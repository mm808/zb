<#include "/layout/back/default/b-header.ftl">
<#include "/layout/back/default/b-aside.ftl">
<#include "/layout/back/default/b-footer.ftl">
<#assign ctx=request.contextPath>
<#macro bLayout title header=bHeader footer=bFooter>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <title>${title}</title>
    <#include "/resource/resource-lte-css.ftl">
    <#include "/resource/resource-lte-js.ftl">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <!-- header -->
    <@bHeader/>
    <!-- aside -->
    <@bAside/>
    <#nested />
    <!-- footer -->
    <@bFooter/>
</div>
<!-- ./wrapper -->
</body>
</html>
</#macro>
