<#macro bAside>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">导航</li>

            <li class="active treeview">
                <a href="javascript:">
                    <i class="fa fa-dashboard"></i>
                    <span>内容管理</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="${ctx}${adminPath}/category/article">
                            <i class="fa fa-circle-o"></i> 文章栏目管理
                        </a>
                    </li>
                </ul>
                <ul class="treeview-menu">
                    <li>
                        <a href="${ctx}${adminPath}/category/video">
                            <i class="fa fa-circle-o"></i> 视频栏目管理
                        </a>
                    </li>
                </ul>
                <ul class="treeview-menu">
                    <li>
                        <a href="${ctx}${adminPath}/content">
                            <i class="fa fa-circle-o"></i> 内容管理
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
</#macro>