<#macro bFooter>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 0.0.1
    </div>
    <strong>Copyright &copy; 2014-2015 <a href="http://www.itmuch.com">ITMuch Studio</a>.
    </strong> All rights reserved.
</footer>
</#macro>