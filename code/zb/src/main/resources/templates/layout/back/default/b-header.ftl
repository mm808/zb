<#assign security=JspTaglibs["http://www.springframework.org/security/tags"]>
<#macro bHeader>
<header class="main-header">
    <!-- Logo -->
    <a href="${ctx}${adminPath}/" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini">
      <b>管</b>理
    </span>
        <!-- logo for regular state and mobile devices -->
    <span class="logo-lg">
      <b>后台</b>管理
    </span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span>欢迎：</span>
                    <span class="hidden-xs">
                        <@security.authentication property="name" />
                    </span>
                </a>
                    <ul class="dropdown-menu">
                        <li><a href="${ctx}/logout">退出</a></li>
                        <li class="divider"></li>
                        <li><a href="${ctx}/">前台首页</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
</#macro>