<#include "/layout/back/default/b-layout.ftl">
<@bLayout "二级分类管理">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-folder"></i>二级分类
            <small>管理</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="#"> <i class="fa fa-dashboard"></i> Home
                </a>
            </li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <section class="col-lg-12 ">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <button id="add" class="btn btn-primary" onclick="add(0, '${ctx}${adminPath}/sort/add');">
                            <i class="fa fa-save"></i> 添加二级分类
                        </button>

                    </div>
                    <div class="box-body">
                        <table class="table tree">
                            <!-- 递归生成树形表 -->
                            <#include "/macro/sort/sort-list-table.ftl">
                            <@sTable sorts 0 />
                        </table>
                    </div>
                </div>
            </section>
            <!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->


        <div class="modal fade" id="addModal" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <form id="mainForm">
                        <div class="modal-body">
                            <input type="hidden" name="id" id="id">
                            <input type="hidden" value="${(category.id)!}" name="categoryId">
                            <div class="form-group">
                                <label for="categoryId">所属栏目</label>
                                <select class="selectpicker form-control" disabled="disabled">
                                    <option value="${(category.id)!}">${(category.name)!}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="parentId">上级分类</label>
                                <select class="selectpicker form-control" name="parentId" id="parentId">
                                    <option value="">---请选择子分类---</option>
                                    <!-- 自定义指令 -->
                                    <#include "/macro/select/select-recursion.ftl">
                                    <@option sorts 0 1 0/>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name">名称</label>
                                <input type="text" name="name" class="form-control" id="name" placeholder="请输入栏目名称">
                            </div>
                            <div class="form-group">
                                <label for="title">标题</label>
                                <input type="text" name="title" class="form-control" id="title" placeholder="请输入标题">
                            </div>
                            <div class="form-group">
                                <label for="keyword">关键词</label>
                                <input type="text" name="keyword" class="form-control" id="keyword" placeholder="请输入关键词,多个关键词用,分割">
                            </div>
                            <div class="form-group">
                                <label for="description">描述</label>
                                <input type="text" name="description" class="form-control" id="description" placeholder="请输入栏目描述">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                            <button type="submit" class="btn btn-primary">提交</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
        </div>
        <!-- /.modal -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<input type="hidden" id="submitUrl">
<script src="${ctx}/back/js/sort.js" type="text/javascript"></script>
</@bLayout>