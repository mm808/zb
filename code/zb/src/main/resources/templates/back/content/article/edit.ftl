<#include "/layout/back/default/b-layout.ftl">
<@bLayout "修改文章">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><i class="fa fa-file"></i>内容
            <small>管理</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">
                <i class="fa fa-dashboard"></i> Home
            </a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <section class="col-lg-12">
                <!-- quick email widget -->
                <div class="box box-info">
                    <div class="box-header with-border">文章</div>
                    <div class="box-body">
                        <form id="mainForm" action="${ctx}${adminPath}/content/edit/${(editVo.id)!}" method="post">
                            <div class="modal-body">
                                <section class="content">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <#if (sorts)?? && (sorts?size>0)>
                                                <div class="form-group ">
                                                    <label for="name">二级分类</label>
                                                    <#include "/macro/select/select-recursion.ftl">
                                                    <@select sorts 0 0 "sortId" editVo.sortId "form-control" />
                                                </div>
                                            </#if>
                                            <div class="form-group ">
                                                <label for="title">
                                                    <span>标题</span>
                                                    <span class="text-danger">(*)</span>
                                                </label>
                                                <input type="text" name="title" class="form-control" id="title" placeholder="请输入标题" value="${(editVo.title)!}">
                                            </div>
                                            <div class="form-group ">
                                                <label for="keyword">关键词</label>
                                                <input type="text" name="keyword" class="form-control" id="keyword" placeholder="多个关键词用,分割" value="${(editVo.keyword)!}">
                                            </div>
                                            <div class="form-group ">
                                                <label for="tagsString">标签</label>
                                                <input type="text" name="tagsString" class="form-control " id="tagsString" placeholder="多个标签使用,分割" value="${(editVo.tagsString)!}">
                                            </div>
                                            <div class="form-group ">
                                                <label for="author">作者</label>
                                                <input type="text" name="author" class="form-control " id="author" placeholder="请输入作者" value="${(editVo.author)!}">
                                            </div>
                                            <div class="form-group ">
                                                <label for="source">来源</label>
                                                <input type="text" name="source" class="form-control " id="source" placeholder="请输入来源" value="${(editVo.source)!}">
                                            </div>
                                            <div class="form-group ">
                                                <label for="issueTime">发表时间</label>
                                                <input type="text" name="issueTime" class="form-control " id="issueTime"
                                                       placeholder="请输入发表时间" value="${(editVo.issueTime?string("yyyy-MM-dd"))!''}">
                                            </div>
                                            <div class="form-group ">
                                                <label for="relationIds">相关文章</label>
                                                <input type="text" name="relationIds" class="form-control " id="relationIds" placeholder="请输入相关文章" value="${(editVo.relationIds)!}">
                                            </div>
                                            <#if recomends??>
                                                <div class="form-group ">
                                                    <label for="recomendIds">推荐方式</label>
                                                    <div class="checkbox">
                                                        <#list recomends as recomend>
                                                            <label>
                                                                <input type="checkbox" name="recomendIds" value="${(recomend.id)!}"
                                                                    <#if (editVo.recomendIds)?? && (editVo.recomendIds?size>0)>
                                                                        <#assign recomendIds=(editVo.recomendIds)/>
                                                                        <#list recomendIds as recomendId>
                                                                       <#if recomendId == recomend.id>checked</#if>
                                                                        </#list>
                                                                    </#if>>
                                                                <span>${(recomend.name)!}</span>
                                                            </label>
                                                        </#list>
                                                    </div>
                                                </div>
                                            </#if>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group ">
                                                <label for="cover">封面</label>
                                                <input type="hidden" name="cover" id="cover" value="${(editVo.cover)!}"
                                                       placeholder="请输入相关文章">
                                                <input id="file" type="file" class="file" name="file">
                                            </div>
                                            <div class="form-group ">
                                                <label for="summary">摘要</label>
                                                <textarea name="summary" id="summary" class="form-control" placeholder="请输入摘要" rows="4">${(editVo.summary)!}</textarea>
                                            </div>
                                            <div class="form-group ">
                                                <label for="txt">
                                                    <span>内容</span>
                                                    <span class="text-danger">(*)</span>
                                                </label>
                                                <textarea name="txt" id="txt" placeholder="请输入内容" rows="10" cols="80" style="height: 250px;">${(editVo.txt)!}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                <button type="submit" class="btn btn-primary">提交</button>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
            <!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script src="${ctx}/back/js/article-edit.js" type="text/javascript"></script>
</@bLayout>