<#include "/layout/back/default/b-layout.ftl">
<@bLayout "内容管理">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><i class="fa fa-file"></i>内容
            <small>管理</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">
                <i class="fa fa-dashboard"></i> Home
            </a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-3">
                <!-- quick email widget -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">栏目</h3>
                    </div>
                    <div class="box-body">
                        <ul id="mainTree" class="ztree"></ul>
                    </div>
                </div>
            </section>
            <section class="col-lg-9">
                <!-- quick email widget -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <button class="btn btn-primary" onclick="add()">
                            <i class="fa fa-save"></i> 添加
                        </button>
                        <button class="btn btn-success" onclick="edit()">
                            <i class="fa fa-edit"></i> 修改
                        </button>
                        <button class="btn btn-danger" onclick="batchRemove();">
                            <i class="fa fa-remove"></i> 批量删除
                        </button>
                    </div>
                    <div class="box-body">
                        <div id="toolbar">
                            <form id="searchForm" class="form-inline">
                                <div class="form-group">
                                    <label for="searchTitle">标题</label>
                                    <input type="text" class="form-control" name="title" id="searchTitle" placeholder="请输入标题">
                                </div>
                                <div class="form-group input-daterange">
                                    <label for="searchStartTime">发表时间</label>
                                    <div class="input-group input-daterange">
                                        <input type="text" class="form-control" name="startTime" id="searchStartTime" placeholder="请输入起始时间"/>
                                        <span class="input-group-addon">至</span>
                                        <input type="text" class="form-control" name="endTime" id="searchEndTime" placeholder="请输入结束时间"/>
                                    </div>
                                </div>
                                <input class="btn btn-default" type="button" id="searchBtn" value="搜索">
                            </form>
                        </div>
                        <table id="table"></table>
                    </div>
                </div>

            </section>
            <!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<input type="hidden" id="categoryId">
<script src="${ctx}/back/js/content.js" type="text/javascript"></script>
</@bLayout>