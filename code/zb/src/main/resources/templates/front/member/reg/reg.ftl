<#include "/layout/front/default/default-layout.ftl">
<@layout "注册">
<div class="main" id="center">
    <div class="main_top">
        <span class="label label-default">HI,赶紧登录口腔盒子大家庭，一起交流知识分享快乐吧！</span>
    </div>
    <div class="main_lable">
        <div class="button_c">
            <a href="${ctx}/login">
                <div class="login_button">用户登录</div>
            </a><a href="${ctx}/reg">
            <div class="login_button" style="border-bottom: 2px solid #3CC09B;">用户注册</div>
        </a>
        </div>
    </div>
    <div class="main_login">
        <#if regVo??>
            <div class="alert alert-danger">
                <@spring.bind "regVo.*" />
                <@spring.showErrors "<br>" />
            </div>
        </#if>
        <form id="signupForm" method="post">
            <div class="login_input">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon clean">
                            <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                        </div>
                        <input type="text" name="username" class="form-control clean" placeholder="请输入账号"
                               value="${(regVo.username)!}"/>
                    </div>
                </div>
            </div>
            <div>
                <div class="login_input">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon clean">
                                <span class="glyphicon glyphicon-lock" aria-hidden="true"></span>
                            </div>
                            <input type="password" name="password" class="form-control clean" placeholder="密码"
                                   value="${(regVo.password)!}"/>
                        </div>
                    </div>
                </div>
                <div class="login_input">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon clean">
                                <span class="glyphicon glyphicon-lock" aria-hidden="true"></span>
                            </div>
                            <input type="password" name="password2" class="form-control clean" placeholder="确认密码"/>
                        </div>
                    </div>
                </div>
                <div class="login_input">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon clean">
                                <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                            </div>
                            <input type="text" class="form-control clean  email" name="email" placeholder="邮箱"
                                   value="${(regVo.email)!}"/>
                        </div>
                    </div>
                </div>
                <div class="login_input">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon clean">
                                <span class="glyphicon glyphicon-phone" aria-hidden="true"></span>
                            </div>
                            <input type="text" class="form-control clean  email" name="mobile" placeholder="手机号"
                                   value="${(regVo.mobile)!}"/>
                        </div>
                    </div>
                </div>
                <div class="login_input" style="height: auto;">
                    <div class="form-group" style="width: 60%; float: left;">
                        <div class="input-group">
                            <div class="input-group-addon clean">
                                <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
                            </div>
                            <input type="text" class="form-control clean" name="mobileCode" placeholder="请输入验证码"/>
                        </div>
                    </div>
                    <div style="width: 40%; float: left; text-align: right">
                        <button type="button" class="btn"
                                style="border-radius: 25px; background-color: #eee; color: #aaa">
                            免费获得验证码
                        </button>
                    </div>
                </div>
                <div style="float: none; clear: both;"></div>
                <div class="login_input l">
                    <div class="checkbox">
                        <label>
                            <#if (regVo.agree)?? && regVo.agree == true>
                                <input type="checkbox" name="agree" checked/>
                            <#else>
                                <input type="checkbox" name="agree"/>
                            </#if> 我已阅读并同意<a href="#" style="color: #999">《使用协议》</a>
                        </label>
                    </div>
                </div>
                <div class="login_input">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    <button type="submit" class="btn btn-success btn_l" name="_eventId_submit">注册</button>
                </div>
        </form>
    </div>
</div>
</@layout>