<#include "/layout/front/center/center-layout.ftl">
<@layout "个人中心">
<div class="main_right">
    <div style="height: 260px; width: 800px; margin: 20px; margin-left: 0px;">
        <img src="${ctx}/front/images/abc1.jpg" style="height: 100%; width: 100%;"/>
    </div>
    <!--个人信息-->
    <div class="person_info">
        <div class="title">基本信息</div>
        <div class="person_intro">

            <div class="left">
                <#if (member.face)?? && (member.face!='')>
                    <img src="${ctx}/${(member.face)!}" style="height: 100%; width: 100%;"/>
                <#else>
                    <img src="${ctx}/front/images/231.jpg" style="height: 100%; width: 100%;"/>
                </#if>
            </div>

            <div class="right">
                <ul>
                    <li>姓名：${(member.name)!}</li>
                    <li>用户积分：1000</li>
                    <li>性别：
                        <#if member??>
                            <#if member.sex==1> 男
                            <#elseif member.sex==2> 女
                            <#else> 未知
                            </#if>
                        </#if>
                    </li>
                    <li>用户邮箱: ${(member.email)!}</li>
                    <li>出生日期：${(member.birthday?string("yyyy-MM-dd"))!}</li>
                    <li>联系方式：${(member.homeAddress)!}</li>
                </ul>
            </div>
        </div>
    </div>
    <!--个人信息结束-->
    <!--评论回复区域-->
    <div class="comment">
        <div class="comment_header">
            <ul>
                <li class="selected"><a href="person-1.html">最新发言</a></li>
                <li><a href="person-2.html">最新回复</a></li>
            </ul>
        </div>
        <div class="comment_body">
            <ul>
                <li><a href="#">
                    <div class="div1">我的发言我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发</div>
                    <div class="div2">
                        <span style="float: left;">回复(20)</span>
                        <span style="float: right;">发表时间：2015-3-16 10:10 — 来自文章</span>
                    </div>
                </a></li>
                <li><a href="#">
                    <div class="div1">我的发言我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发</div>
                    <div class="div2">
                        <span style="float: left;">回复(20)</span>
                        <span style="float: right;">发表时间：2015-3-16 10:10 — 来自文章</span>
                    </div>
                </a></li>
                <li><a href="#">
                    <div class="div1">我的发言我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发</div>
                    <div class="div2">
                        <span style="float: left;">回复(20)</span>
                        <span style="float: right;">发表时间：2015-3-16 10:10 — 来自文章</span>
                    </div>
                </a></li>
                <li><a href="#">
                    <div class="div1">我的发言我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发</div>
                    <div class="div2">
                        <span style="float: left;">回复(20)</span>
                        <span style="float: right;">发表时间：2015-3-16 10:10 — 来自文章</span>
                    </div>
                </a></li>
                <li><a href="#">
                    <div class="div1">我的发言我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发我的发</div>
                    <div class="div2">
                        <span style="float: left;">回复(20)</span>
                        <span style="float: right;">发表时间：2015-3-16 10:10 — 来自文章</span>
                    </div>
                </a></li>

            </ul>
        </div>
    </div>
    <!--评论回复内容结束-->
</div>
</@layout>