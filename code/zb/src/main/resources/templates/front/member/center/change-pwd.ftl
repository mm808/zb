<#include "/layout/front/center/center-layout.ftl">
<@layout "修改密码">
<div class="main_right">
    <!--评论回复区域-->
    <div class="comment">
        <div class="comment_header">
            <ul>
                <li>
                    <a href="${ctx}/member/info">个人信息</a>
                </li>
                <li class="selected">
                    <a href="${ctx}/member/change-pwd">修改密码</a>
                </li>
            </ul>
        </div>
        <div class="comment_body">
            <form class="form-horizontal" action="${ctx}/member/change-pwd" method="post">
                <div class="form-group">
                    <label class="col-sm-2 control-label">原密码：</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="password" name="oldPassword"/>
                    </div>
                    <div class="col-sm-offset-2 col-sm-10">
                        <#if info??> <@spring.bind "info.oldPassword" /><@spring.showErrors "<br />" "error"/> </#if>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">新密码：</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="password" name="password"/>
                    </div>
                    <div class="col-sm-offset-2 col-sm-10">
                        <#if info??> <@spring.bind "info.password" /><@spring.showErrors "<br />" "error"/> </#if>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">确认密码：</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="password" name="password2"/>
                    </div>
                    <div class="col-sm-offset-2 col-sm-10">
                        <#if info??> <@spring.bind "info.password2" /><@spring.showErrors "<br />" "error"/> </#if>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <input class="btn btn-success" value="保存" type="submit"/>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--评论回复内容结束-->
</div>
</@layout>