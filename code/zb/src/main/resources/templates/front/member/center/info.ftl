<#include "/layout/front/center/center-layout.ftl">
<#assign security=JspTaglibs["http://www.springframework.org/security/tags"]>
<@layout "个人信息">
<div class="main_right">
    <!--评论回复区域-->
    <div class="comment">
        <div class="comment_header">
            <ul>
                <li class="selected">
                    <a href="${ctx}/member/info">个人信息</a>
                </li>
                <li>
                    <a href="${ctx}/member/change-pwd">修改密码</a>
                </li>
            </ul>
        </div>
        <div class="comment_body">
            <form class="form-horizontal" action="${ctx}/member/info" method="post">
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <#if info??>
                            <#assign status = springMacroRequestContext.getBindStatus("info.*")>
                            <#if (status.errorMessages)?? && ((status.errorMessages)?size >0)>
                                <div class="alert alert-danger">
                                    <@spring.bind "info.*" />
                                        <@spring.showErrors "<br>"/>
                                </div>
                            <#else >
                            </#if>
                        </#if>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">头像：</label>
                    <div class="col-sm-10">
                        <div class="user_img">
                            <#if (info.face)?? && (info.face!='')>
                                <img src="${ctx}/${(info.face)!}" style="height: 50px; width: 50px;" id="faceImg"/>
                            <#else>
                                <img src="${ctx}/front/images/231.jpg" style="height: 50px; width: 50px;" id="faceImg"/>
                            </#if>
                        </div>
                        <div id="picker1" class="upload"></div>
                        <input type="hidden" name="face" value="${(info.face)!}" id="faceInput"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">姓名：</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="text" name="name" value="${(info.name)!}"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">性别：</label>
                    <div class="col-sm-10">
                        <#if (info.sex)?? && (info.sex) == 1>
                            <label class="radio-inline">
                                <input name="sex" type="radio" value="1" checked/>
                                <span>男</span>
                            </label>
                        <#else>
                            <label class="radio-inline">
                                <input name="sex" type="radio" value="1"/>
                                <span>男</span>
                            </label>
                        </#if> <#if (info.sex)?? && (info.sex)== 2>
                        <label class="radio-inline">
                            <input name="sex" type="radio" value="2" checked/>
                            <span>女</span>
                        </label>
                    <#else>
                        <label class="radio-inline">
                            <input name="sex" type="radio" value="2"/>
                            <span>女</span>
                        </label>
                    </#if>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">民族：</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="text" name="nation" value="${(info.nation)!}"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">生日：</label>
                    <div class="col-sm-10">
                        <input readonly="true" data-date-end-date="0d" data-date-format="yyyy-mm-dd"
                               class="form-control datepicker" type="text" name="birthday"
                               value="${(info.birthday?string("yyyy-MM-dd"))!''}"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">手机号：</label>
                    <div class="col-sm-10">
                        <input class="form-control" disabled value="<@security.authentication property="principal.member.mobile" />">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">电子邮件：</label>
                    <div class="col-sm-10">
                        <input class="form-control" disabled value="<@security.authentication property="name" />">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">籍贯：</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="text" name="originPlace" value="${(info.originPlace)!}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">住址：</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="text" name="homeAddress" value="${(info.homeAddress)!}"/>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <input class="btn btn-success" value="保存" type="submit"/>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--评论回复内容结束-->
</div>
</@layout>