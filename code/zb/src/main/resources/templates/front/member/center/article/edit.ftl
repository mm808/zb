﻿<#include "/layout/front/center/center-layout.ftl">
<#assign security=JspTaglibs["http://www.springframework.org/security/tags"]>
<@layout "文章编辑">
<div class="main_right">
    <!--评论回复区域-->
    <div class="comment">
        <div class="comment_header">
            <ul>
                <li class="selected">
                    <a href="${ctx}/member/article/add">我要发布</a>
                </li>
                <li>
                    <a href="${ctx}/member/article/1">已发布</a>
                </li>
            </ul>
        </div>
        <div class="comment_body">
            <form class="form-horizontal" action="${ctx}/member/article/edit/${(editVo.id)!}" method="post">
                <#--<div class="form-group">
                    <label class="col-sm-2 control-label">选择分类：</label>
                    <div class="col-sm-10">
                        <#include "/macro/select/select-recursion.ftl">
                        <@select categories 0 0 "categoryId" editVo.categoryId "form-control" "categoryId" />
                    </div>
                </div>-->
                <div class="form-group">
                    <label class="col-sm-2 control-label">二级分类：</label>
                    <div class="col-sm-10">
                        <#include "/macro/select/select-group.ftl">
                            <@selectGroup sorts "sortId" editVo.sortId />
                    </div>
                </div>
                <div class="form-group" id="sort">
                    <label class="col-sm-2 control-label">二级分类：</label>
                    <div class="col-sm-10">
                        <div class="col-sm-5">
                            <select class="form-control" id="topSort">
                            </select>
                        </div>
                        <div class="col-sm-5">
                            <select class="form-control" id="sonSort">
                            </select>
                        </div>
                        <input type="hidden" id="sortId" name="sortId"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">选择标签：</label>
                    <div class="col-sm-10">
                        <#list defaultTags as tag>
                            <span class="video_bq2">${tag.name}</span>
                        </#list>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">自定义标签：</label>
                    <div class="col-sm-10">
                        <input class="form-control" placeholder="多个标签用,分割" type="text" name="tagsString" id="tags" value="${(editVo.tagsString)!}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">作者：</label>
                    <div class="col-sm-10">
                        <input class="form-control" placeholder="请输入作者" type="text" name="author" id="author" value="${(editVo.author)!}"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">来源：</label>
                    <div class="col-sm-10">
                        <input class="form-control" placeholder="请输入来源" type="text" name="source" id="source" value="${(editVo.source)!}"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">图片上传：</label>
                    <div class="col-sm-10">
                        <input id="file" type="file" class="file" name="file"/>
                        <input name="cover" id="cover" type="hidden" value="${(editVo.cover)!}"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">文章标题：</label>
                    <div class="col-sm-10">
                        <input class="form-control" placeholder="文章标题不超过30字" type="text" name="title" value="${(editVo.title)!}"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">文章内容：</label>
                    <div class="col-sm-10">
                        <textarea id="editor" style="height: 150px" name="txt">${(editVo.txt)!}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <button type="submit" class="btn btn-success">发表</button>
                        <button type="button" class="btn btn-info">预览</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--评论回复内容结束-->
</div>
<script type="text/javascript" src="${ctx}/front/js/member/article-edit.js"></script>
</@layout>