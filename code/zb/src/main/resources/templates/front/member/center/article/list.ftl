﻿<#include "/layout/front/center/center-layout.ftl">
<#assign security=JspTaglibs["http://www.springframework.org/security/tags"]>
<@layout "文章发布">
<!--内容开始-->
<div class="main_right">
    <!--评论回复区域-->
    <div class="comment">
        <div class="comment_header">
            <ul>
                <li><a href="${ctx}/member/article/add">我要发布</a></li>
                <li class="selected"><a href="${ctx}/member/article/1">已发布</a></li>
            </ul>
        </div>
        <div class="comment_body">
            <form action="#">
                <ul style="">
                    <#if (list.content)??>
                        <#list list.content as item>
                            <li class="yfb_li">
                            <span class="yfb_span1">
                            <a href="${ctx}/content/${(item.id)!}/1">${(item.title)!}</a>
                            </span>
                                <span class="yfb_span2">${(item.createTime?string("yyyy-MM-dd"))!} </span>
                            <span class="yfb_span3">
                                <a href="${ctx}/member/article/edit/${(item.id)!}">编辑</a>
                            </span>
                                <a class="yfb_span4" href="${ctx}/member/article/del/${(item.id)!}">删除</a>
                            </li>
                        </#list>
                    </#if>
                </ul>
            </form>
            <div class="pagination_wrapper" style="width: 100%; height: 60px; text-align: center; line-height: 60px; padding-top: 15px;">
                <#include "/macro/pagination.ftl">
                <@pagination list=list url=url/></div>
        </div>
    </div>
    <!--评论回复内容结束-->
</div>
</@layout>