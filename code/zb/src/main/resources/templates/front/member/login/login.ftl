<#include "/layout/front/default/default-layout.ftl">
<@layout "登录">
<div class="main">
    <div class="main_top">
        <span class="label label-default">HI,赶紧登录口腔盒子大家庭，一起交流知识分享快乐吧！</span>
    </div>
    <div class="main_lable">
        <div class="button_c">
            <a href="${ctx}/login">
                <div class="login_button" style="border-bottom: 2px solid #3CC09B;">用户登录</div>
            </a><a href="${ctx}/reg">
            <div class="login_button">用户注册</div>
        </a>
        </div>
    </div>
    <div class="main_login">
        <#if SPRING_SECURITY_LAST_EXCEPTION??>
            <div class="alert alert-danger">${SPRING_SECURITY_LAST_EXCEPTION.message}</div>
        </#if>
        <form id="signupForm" action="${ctx}/login" method="post">
            <div class="login_input">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon clean">
                            <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                        </div>
                        <input type="text" class="form-control clean" placeholder="请输入账号" name="username" value="${(username)!}">
                    </div>
                </div>
            </div>
            <div class="login_input">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon clean">
                            <span class="glyphicon glyphicon-lock" aria-hidden="true"></span>
                        </div>
                        <input type="password" class="form-control clean" placeholder="请输入密码" name="password">
                    </div>
                </div>
            </div>
            <div class="login_input l">
                <div class="checkbox">
                    <label> <input type="checkbox" name="remember-me"> 下次自动登录
                    </label> <a href="#" style="margin-left: 220px; color: #3CC09B">忘记密码？</a>
                </div>
            </div>
            <div class="login_input">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <button type="submit" id="btn" class="btn btn-warning btn_l">登录</button>
            </div>
        </form>
    </div>
</div>
</@layout>