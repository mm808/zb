<#include "/layout/front/default/default-layout.ftl">
<@layout "首页">
<div class="main">
    <div class="nav">
        <div class="left"></div>
        <div class="middle_wrapper">
            <div id="lastnews_slideup" class="middle">
                <ul class="slideup">
                    <li><i class="point">回首历史：拉手网的命运早就注1</i></li>
                    <li><i class="point">回首历史：拉手网的命运早就注2</i></li>
                    <li><i class="point">回首历史：拉手网的命运早就注3</i></li>
                    <li><i class="point">回首历史：拉手网的命运早就注4</i></li>
                    <li><i class="point">回首历史：拉手网的命运早就注5</i></li>
                    <li><i class="point">回首历史：拉手网的命运早就注6</i></li>
                    <li><i class="point">回首历史：拉手网的命运早就注7</i></li>
                    <li><i class="point">回首历史：拉手网的命运早就注8</i></li>
                </ul>
            </div>
        </div>
        <div class="right">
            <div class="myselect">
                <div class="select_txt">
                    <span id="select_value">文章</span>
                    <img class="downlog" src="${ctx}/front/images/xiala.png"/>
                </div>
                <ul>
                    <li class="selected">文章</li>
                    <li>视频</li>
                    <li>产品</li>
                    <li>测试</li>
                </ul>
            </div>
            <div class="keyword_wrapper">
                <input placeHolder="输入关键词" class="keyword" type="text"/>
            </div>
            <div class="search_btn">
                搜索
            </div>
        </div>
    </div>
    <div class="content">
        <div class="content_left">
            <div id="slider" class="slider">
                <ul>
                    <li>
                        <a href="#">
                            <img width="800" height="350" border="0" src="${ctx}/front/images/01.jpg" alt="1111111111"/>
                        </a>
                    </li>
                    <li><a href="#">
                        <img width="800" height="350" border="0" src="${ctx}/front/images/02.jpg" alt="22222222222"/></a>
                    </li>
                    <li>
                        <a href="#"><img width="800" height="350" border="0" src="${ctx}/front/images/03.jpg" alt="333333333333"/></a>
                    </li>
                    <li>
                        <a href="#"><img width="800" height="350" border="0" src="${ctx}/front/images/04.jpg" alt="444444444444"/></a>
                    </li>
                    <li>
                        <a href="#"><img width="800" height="300" border="0" src="${ctx}/front/images/1.PNG" alt="444444444444"/></a>
                    <li>
                </ul>
                <span id="prevBtn" type="PREV" class="sliderBtn"></span>
                <span id="nextBtn" type="NEXT" class="sliderBtn"></span>
                <div id="altbox">&nbsp;</div>
            </div>
            <div class="advertise_div"><img style="width:800px; height:90px;" src="${ctx}/front/images/advertise.PNG"/>
            </div>
            <!--文章部分-->
            <div class="article">
                <div class="article_nav">
                    <div class="left"><span>文章</span></div>
                    <div class="right">
                        <ul class="nav_item_wraper">
                            <li>文章</li>
                            <li>磨牙</li>
                            <li>牙齿美白</li>
                            <li>磨牙</li>
                            <li>文章</li>
                            <li>磨牙</li>
                            <li>牙齿美白</li>
                            <li>磨牙</li>
                            <li>文章</li>
                            <li>磨牙</li>
                        </ul>
                    </div>
                </div>
                <div class="article_content">
                    <div class="article_item">
                        <div class="left"><img class="hot" src="${ctx}/front/images/hot.PNG"/> <img
                                style="height:168px; width:248px;" src="${ctx}/front/images/article_item.PNG"/></div>
                        <div class="right">
                            <div class="title_wrapper">三分钟教你如何养牙</div>
                            <div class="writer_wrapper">王兰 2015年12月18日 14:08</div>
                            <div class="content_wrapper">
                                整牙的好处实在太多了，一口漂亮整齐的牙齿，会让人更加的自信，微笑更加的甜美迷人，所以明星整牙更为普遍。明星都说自己没整过容，但整牙的好处却远大于整容呢。
                            </div>
                            <div class="label_wrapper">
                                <span class="label_item">爱牙</span> <span class="label_item">爱牙</span> <span
                                    class="label_item">爱牙</span> <span class="label_item">爱牙</span> <span
                                    class="label_item">爱牙</span></div>
                            <div class="other_wrapper">
                                <span class="glyphicon glyphicon-eye-open"></span>
                                浏览(100)
                                <span class="glyphicon glyphicon-comment"></span>
                                回复（100）
                                <span class="glyphicon glyphicon-heart" style="color: rgb(255, 0, 0);"></span>
                                &nbsp喜欢（100）
                            </div>
                        </div>
                    </div>
                    <div class="article_item">
                        <div class="left"><img class="hot" src="${ctx}/front/images/hot.PNG"/> <img
                                style="height:168px; width:248px;" src="${ctx}/front/images/article_item.PNG"/></div>
                        <div class="right">
                            <div class="title_wrapper">三分钟教你如何养牙</div>
                            <div class="writer_wrapper">王兰 2015年12月18日 14:08</div>
                            <div class="content_wrapper">
                                整牙的好处实在太多了，一口漂亮整齐的牙齿，会让人更加的自信，微笑更加的甜美迷人，所以明星整牙更为普遍。明星都说自己没整过容，但整牙的好处却远大于整容呢。
                            </div>
                            <div class="label_wrapper">
                                <span class="label_item">爱牙</span> <span class="label_item">爱牙</span> <span
                                    class="label_item">爱牙</span> <span class="label_item">爱牙</span> <span
                                    class="label_item">爱牙</span></div>
                            <div class="other_wrapper">
                                <span class="glyphicon glyphicon-eye-open"></span>
                                浏览(100)
                                <span class="glyphicon glyphicon-comment"></span>
                                回复（100）
                                <span class="glyphicon glyphicon-heart" style="color: rgb(255, 0, 0);"></span>
                                &nbsp喜欢（100）
                            </div>
                        </div>
                    </div>
                    <div class="article_item">
                        <div class="left"><img class="hot" src="${ctx}/front/images/hot.PNG"/> <img
                                style="height:168px; width:248px;" src="${ctx}/front/images/article_item.PNG"/></div>
                        <div class="right">
                            <div class="title_wrapper">三分钟教你如何养牙</div>
                            <div class="writer_wrapper">王兰 2015年12月18日 14:08</div>
                            <div class="content_wrapper">
                                整牙的好处实在太多了，一口漂亮整齐的牙齿，会让人更加的自信，微笑更加的甜美迷人，所以明星整牙更为普遍。明星都说自己没整过容，但整牙的好处却远大于整容呢。
                            </div>
                            <div class="label_wrapper">
                                <span class="label_item">爱牙</span> <span class="label_item">爱牙</span> <span
                                    class="label_item">爱牙</span> <span class="label_item">爱牙</span> <span
                                    class="label_item">爱牙</span></div>
                            <div class="other_wrapper">
                                <span class="glyphicon glyphicon-eye-open"></span>
                                浏览(100)
                                <span class="glyphicon glyphicon-comment"></span>
                                回复（100）
                                <span class="glyphicon glyphicon-heart" style="color: rgb(255, 0, 0);"></span>
                                &nbsp喜欢（100）
                            </div>
                        </div>
                    </div>

                    <div class="article_item">
                        <div class="left"><img class="hot" src="${ctx}/front/images/hot.PNG"/> <img
                                style="height:168px; width:248px;" src="${ctx}/front/images/article_item.PNG"/></div>
                        <div class="right">
                            <div class="title_wrapper">三分钟教你如何养牙</div>
                            <div class="writer_wrapper">王兰 2015年12月18日 14:08</div>
                            <div class="content_wrapper">
                                整牙的好处实在太多了，一口漂亮整齐的牙齿，会让人更加的自信，微笑更加的甜美迷人，所以明星整牙更为普遍。明星都说自己没整过容，但整牙的好处却远大于整容呢。
                            </div>
                            <div class="label_wrapper">
                                <span class="label_item">爱牙</span> <span class="label_item">爱牙</span> <span
                                    class="label_item">爱牙</span> <span class="label_item">爱牙</span> <span
                                    class="label_item">爱牙</span></div>
                            <div class="other_wrapper">
                                <span class="glyphicon glyphicon-eye-open"></span>
                                浏览(100)
                                <span class="glyphicon glyphicon-comment"></span>
                                回复（100）
                                <span class="glyphicon glyphicon-heart" style="color: rgb(255, 0, 0);"></span>
                                &nbsp喜欢（100）
                            </div>
                        </div>
                    </div>

                    <div class="article_item">
                        <div class="left"><img class="hot" src="${ctx}/front/images/hot.PNG"/> <img
                                style="height:168px; width:248px;" src="${ctx}/front/images/article_item.PNG"/></div>
                        <div class="right">
                            <div class="title_wrapper">三分钟教你如何养牙</div>
                            <div class="writer_wrapper">王兰 2015年12月18日 14:08</div>
                            <div class="content_wrapper">
                                整牙的好处实在太多了，一口漂亮整齐的牙齿，会让人更加的自信，微笑更加的甜美迷人，所以明星整牙更为普遍。明星都说自己没整过容，但整牙的好处却远大于整容呢。
                            </div>
                            <div class="label_wrapper">
                                <span class="label_item">爱牙</span> <span class="label_item">爱牙</span> <span
                                    class="label_item">爱牙</span> <span class="label_item">爱牙</span> <span
                                    class="label_item">爱牙</span></div>
                            <div class="other_wrapper">
                                <span class="glyphicon glyphicon-eye-open"></span>
                                浏览(100)
                                <span class="glyphicon glyphicon-comment"></span>
                                回复（100）
                                <span class="glyphicon glyphicon-heart" style="color: rgb(255, 0, 0);"></span>
                                &nbsp喜欢（100）
                            </div>
                        </div>
                    </div>
                    <!--item结束-->


                </div>
                <!--文章内容结束-->

            </div>
            <!--文章结束-->
            <!--视频-->
            <div class="video">
                <!--视频导航-->
                <div class="video_nav">
                    <div class="left">
                        <span>视频</span>
                    </div>
                    <div class="right">
                        <ul class="nav_item_wraper">
                            <li>文章</li>
                            <li>磨牙</li>
                            <li>牙齿美白</li>
                            <li>磨牙</li>
                            <li>文章</li>
                            <li>磨牙</li>
                            <li>牙齿美白</li>
                            <li>磨牙</li>
                            <li>文章</li>
                            <li>磨牙</li>
                        </ul>
                    </div>
                </div>
                <!--视频导航结束-->
                <!--视频内容开始-->
                <div class="video_content">
                    <div class="up">
                        <div class="left">
                            <img id="video_big_img" src="#" style=" height:100%; width:100%;"/>
                        </div>
                        <div class="right">
                            <ul>
                                <li data-path="${ctx}/front/images/first_video.png"><a href="#">首尔大学么么教授来华教授来华讲学首尔大学么么教授来华讲学讲学</a>
                                </li>
                                <li data-path="${ctx}/front/images/video1.jpg"><a href="#">首尔大学么么教授来华教授来华讲学首尔大学么么教授来华讲学讲学</a></li>
                                <li data-path="${ctx}/front/images/first_video.png"><a href="#">首尔大学么么教授来华教授来华讲学首尔大学么么教授来华讲学讲学</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="down">
                        <ul>
                            <li>
                                <div class="img_wrapper">
                                    <img src="${ctx}/front/images/1002.png" style="height:100%; width:100%;"/>
                                </div>
                                <div class="title">快来看视频啦</div>
                            </li>
                            <li>
                                <div class="img_wrapper">
                                    <img src="${ctx}/front/images/1002.png" style="height:100%; width:100%;"/>
                                </div>
                                <div class="title">快来看视频啦</div>
                            </li>
                            <li>
                                <div class="img_wrapper">
                                    <img src="${ctx}/front/images/1002.png" style="height:100%; width:100%;"/>
                                </div>
                                <div class="title">快来看视频啦</div>
                            </li>
                            <li>
                                <div class="img_wrapper">
                                    <img src="${ctx}/front/images/1002.png" style="height:100%; width:100%;"/>
                                </div>
                                <div class="title">快来看视频啦</div>
                            </li>
                        </ul>
                    </div>


                </div>
                <!--视频内容结束-->
            </div>
            <!--视频结束-->
            <!--自习室开始-->
            <div class="room">
                <!--自习室导航开始-->
                <div class="room_nav">
                    <div class="left">
                        <span>自习室</span>
                    </div>
                    <div class="right">
                        <ul class="nav_item_wraper">
                            <li>文档</li>
                            <li>视频</li>
                        </ul>
                    </div>
                </div>
                <!--自习室导航结束-->
                <div class="room_content">
                    <div class="left">
                        <img title="ggggggggggggg" style="height:390px; width:190px" src="${ctx}/front/images/room1.PNG"/>
                    </div>
                    <div class="right">
                        <ul>
                            <li>
                                <div class="img_wrapper">
                                    <img title="sdfsdf" src="${ctx}/front/images/room3.PNG"/>
                                </div>
                                <div class="title">12123</div>
                            </li>
                            <li>
                                <div class="img_wrapper">
                                    <img title="sdfsdf" src="${ctx}/front/images/room3.PNG"/>
                                </div>
                                <div class="title">asdfassdfassssssssssss</div>
                            </li>
                            <li>
                                <div class="img_wrapper">
                                    <img title="sdfsdf" src="${ctx}/front/images/room3.PNG"/>
                                </div>
                                <div class="title">12123</div>
                            </li>
                            <li>
                                <div class="img_wrapper">
                                    <img src="${ctx}/front/images/room3.PNG" title="sdfsdf"/>
                                </div>
                                <div class="title">12123</div>
                            </li>
                            <li>
                                <div class="img_wrapper">
                                    <img alt="ggggggggggggg" src="${ctx}/front/images/room3.PNG"/>
                                </div>
                                <div class="title">12123</div>
                            </li>
                            <li>
                                <div class="img_wrapper">
                                    <img src="${ctx}/front/images/room3.PNG"/>
                                </div>
                                <div class="title">12123</div>
                            </li>
                        </ul>
                    </div>
                </div>


            </div>
            <!--自习室结束-->

            <!--产品中心开始-->
            <div class="product">
                <!--产品中心导航开始-->
                <div class="product_nav">
                    <div class="left">
                        <span>产品中心</span>
                    </div>
                    <div class="right">
                        <ul class="nav_item_wraper">
                            <li>一类</li>
                            <li>二类</li>
                            <li>一类</li>
                            <li>二类</li>
                        </ul>
                    </div>
                </div>
                <!--产品中心导航结束-->
                <!--产品内容开始-->
                <div class="product_content">
                    <!--产品内容左边-->
                    <div class="left">
                        <ul>
                            <li>搜狐IT</li>
                            <li>搜狐IT</li>
                            <li>搜狐IT</li>
                            <li>搜狐IT</li>
                            <li>搜狐IT</li>
                            <li>搜狐IT</li>
                            <li>搜狐IT</li>
                            <li>搜狐IT</li>
                            <li>搜狐IT</li>
                            <li>搜狐IT</li>
                            <li>搜狐IT</li>
                        </ul>
                    </div>
                    <!--产品内容右边-->
                    <div class="right">
                        <div class="up">
                            <img style="width:100%; height:100%;" src="${ctx}/front/images/product1.PNG"/>
                        </div>
                        <div class="down">
                            <ul>
                                <li>
                                    <div class="img_wrapper">
                                        <img style=" width:100%; height:100%;" src="${ctx}/front/images/products2.PNG"/>
                                    </div>
                                    <div class="title">名称名称</div>
                                </li>
                                <li>
                                    <div class="img_wrapper">
                                        <img style=" width:100%; height:100%;" src="${ctx}/front/images/products2.PNG"/>
                                    </div>
                                    <div class="title">名称名称</div>
                                </li>
                                <li id="ff">
                                    <div class="img_wrapper">
                                        <img style=" width:100%; height:100%;" src="${ctx}/front/images/products2.PNG"/>
                                    </div>
                                    <div class="title">名称名名称名名称名名称名称</div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--产品右边结束-->
                </div>
                <!--产品内容结束-->
            </div>
            <!--产品中心结束-->

            <!--活动栏目-->
            <div class="activity">
                <!--活动导航开始-->
                <div class="activity_nav">
                    <div class="left">
                        <span>活动栏目</span>
                    </div>
                    <div class="right">
                        <ul class="nav_item_wraper">
                            <li>栏目1</li>
                            <li>栏目2</li>
                        </ul>
                    </div>
                </div>
                <!--活动导航结束-->
                <div class="activity_content">
                    <div class="left">
                        <div class="img_wrapper">
                            <img style="width:400px;height:270px;" src="${ctx}/front/images/activity.PNG"/>
                        </div>
                        <div class="title_wapper">你不造口腔医学中的定律</div>
                        <div class="content_wapper">你不造口腔医学中的定律你不造口腔医学中的定律你不造口腔医学中的定律你不造口腔医学中的定律你不造口不造口腔医学中的定律</div>
                        <div class="fun_wrapper">
                            <div class="label_date">开始日期：2015-8-19</div>
                            <div class="people">80人参加</div>
                            <div class="takepart">我要参加</div>
                        </div>
                    </div>

                    <!--右边内容开始-->
                    <div class="right">

                        <div class="item">
                            <div class="item_left">
                                <img style=" height:100%; width:100%;" src="${ctx}/front/images/activit2.PNG"/>
                            </div>
                            <div class="item_right">
                                <div class="title_wrapper">快来参加活动啦</div>
                                <div class="content_wrapper">日活动日活动日活动日活日活活动日活动日日活动日活动日活动</div>
                                <div class="date_wrapper">开始日期：2012-9-12</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="item_left">
                                <img style=" height:100%; width:100%;" src="${ctx}/front/images/activit2.PNG"/>
                            </div>
                            <div class="item_right">
                                <div class="title_wrapper">今日日活今日活今日活今日活动</div>
                                <div class="content_wrapper">日活动日活动日活动日活日活活动日活动日日活动日活动日活动</div>
                                <div class="date_wrapper">开始日期：2012-9-12</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="item_left">
                                <img style=" height:100%; width:100%;" src="${ctx}/front/images/activit2.PNG"/>
                            </div>
                            <div class="item_right">
                                <div class="title_wrapper">今日活动</div>
                                <div class="content_wrapper">日活动日活动日活动日活日活活动日活动日日活动日活动日活动</div>
                                <div class="date_wrapper">开始日期：2012-9-12</div>
                            </div>
                        </div>
                    </div>
                    <!--右边内容结束-->


                </div>

            </div>
            <!--活动栏目结束-->
            <!--兴趣小组开始-->
            <div class="club">
                <!--兴趣小组导航开始-->
                <div class="club_nav">
                    <div class="left">
                        <span>兴趣新组</span>
                    </div>
                    <div class="right">
                    </div>
                </div>
                <!--兴趣小组导航结束-->
                <!--兴趣小组内容-->
                <div class="club_content">
                    <ul>
                        <li>
                            <div class="first">
                                <div class="log_wrapper"><img style="height:100%; width:100%;" src="${ctx}/front/images/club.PNG"/>
                                </div>
                                <div class="content_wrapper">
                                    <div class="name">护牙组</div>
                                    <div class="description">我们是口腔一族在这里大家一起分享我们的兴趣爱好</div>
                                    <div class="other"><span class="glyphicon glyphicon-user">成员(50)</span><span
                                            class="glyphicon glyphicon-share">分享(58)</span></div>
                                </div>
                            </div>
                            <div class="second">
                                <div class="label_wrapper">正在讨论</div>
                                <div class="content_wrapper">1sdfsdf23</div>
                            </div>
                        </li>
                        <li>
                            <div class="first">
                                <div class="log_wrapper"><img style="height:100%; width:100%;" src="${ctx}/front/images/club.PNG"/>
                                </div>
                                <div class="content_wrapper">
                                    <div class="name">护牙组</div>
                                    <div class="description">护牙组护牙组护牙牙组护牙组护牙组护牙组护牙组护牙组护牙组护牙组护牙组</div>
                                    <div class="other"><i class="people"></i>成员(50)分享(58)</div>
                                </div>
                            </div>
                            <div class="second">
                                <div class="label_wrapper">正在讨论</div>
                                <div class="content_wrapper">1sdfsdf23</div>
                            </div>
                        </li>
                        <li>
                            <div class="first">
                                <div class="log_wrapper"><img style="height:100%; width:100%;" src="${ctx}/front/images/club2.jpg"/>
                                </div>
                                <div class="content_wrapper">
                                    <div class="name">护牙组</div>
                                    <div class="description">护牙组护牙组护牙牙组护牙组护牙组护牙组护牙组护牙组护牙组护牙组护牙组</div>
                                    <div class="other"><i class="people"></i>成员(50)分享(58)</div>
                                </div>
                            </div>
                            <div class="second">
                                <div class="label_wrapper">正在讨论</div>
                                <div class="content_wrapper">1sdfsdf23</div>
                            </div>
                        </li>
                        <li>
                            <div class="first">
                                <div class="log_wrapper"><img style="height:100%; width:100%;" src="${ctx}/front/images/club1.jpg"/>
                                </div>
                                <div class="content_wrapper">
                                    <div class="name">护牙组</div>
                                    <div class="description">护牙组护牙组护牙牙组护牙组护牙组护牙组护牙组护牙组护牙组护牙组护牙组</div>
                                    <div class="other"><i class="people"></i>成员(50)分享(58)</div>
                                </div>
                            </div>
                            <div class="second">
                                <div class="label_wrapper">正在讨论</div>
                                <div class="content_wrapper">1sdfsdf23</div>
                            </div>
                        </li>
                    </ul>
                </div>
                <!--兴趣小组内容结束-->


            </div>
            <!--兴趣小组结束-->

        </div>
        <div class="content_right">
            <!--最新导读-->
            <div class="latestreview">
                <div class="label_wrapper">
                    <i>最新导读</i>
                    <div style="height:2px; background:#16c09b; width:120px;"></div>
                </div>
                <div class="content_wrapper">
                    <ul>
                        <li>
                            <div class="title_txt">
                                <div class="catagory">视频</div>
                                <div class="title_content">损失转嫁做出华丽成绩单，乐视网</div>
                            </div>
                            <div class="content_txt">&nbsp;&nbsp;&nbsp;&nbsp;
                                2015年以来，已有包括完美世界，中国手游，世纪佳缘，易居中国，淘米,就磅数
                                2015年以来，已有包括完美世界，中国手游，世纪佳缘，易居中国，淘米, 2015年以来，已有包括完美世界，中国手游，世纪佳缘，易居中，淘米…………
                            </div>
                        </li>
                        <li>
                            <div class="title_txt">
                                <div class="catagory">视频</div>
                                <div class="title_content">损失转嫁做出华丽成绩单，乐视网</div>
                            </div>
                            <div class="content_txt">损失转嫁做出华丽成绩单，乐视网</div>
                        </li>
                        <li>
                            <div class="title_txt">
                                <div class="catagory">视频</div>
                                <div class="title_content">损失转嫁做出华丽成绩单，乐视网</div>
                            </div>
                            <div class="content_txt">损失转嫁做出华丽成绩单，乐视网</div>
                        </li>
                        <li>
                            <div class="title_txt">
                                <div class="catagory">视频</div>
                                <div class="title_content">损失转嫁做出华丽成绩单，乐视网</div>
                            </div>
                            <div class="content_txt">损失转嫁做出华丽成绩单，乐视网</div>
                        </li>
                        <li>
                            <div class="title_txt">
                                <div class="catagory">视频</div>
                                <div class="title_content">损失转嫁做出华丽成绩单，乐视网读今</div>
                            </div>
                            <div class="content_txt">损失转嫁做出华丽成绩单，乐视网</div>
                        </li>
                    </ul>
                </div>
            </div>
            <!--最新导读结束-->
            <!---->
            <div class="sign">
                <img src="${ctx}/front/images/sign.png" style=" height:90px;width:100%;"/>
                <div class="intro">连续签到<span style="color:#FFCC00;">100</span>天</div>
                <div class="sign_opertate">签到</div>
            </div>
            <!--活动日历-->
            <div class="calendar">
                <div class="label_wrapper">
                    <div class="label_title">
                        活动日历
                    </div>


                    <div style="height:2px; background:#16c09b; width:110px; position:absolute; bottom:-2px;"></div>


                    <div class="label_act">提交活动</div>
                </div>

                <!--活动日历内容-->
                <div class="calendar_content">
                    <div class="calendar_content_nav">
                        <div class="left"></div>
                        <div class="middle">2015年6月</div>
                        <div class="right"></div>
                    </div>
                    <div class="calendar_content_content">
                        <table>
                            <tr>
                                <td>日</td>
                                <td>一</td>
                                <td>二</td>
                                <td>三</td>
                                <td>四</td>
                                <td>五</td>
                                <td>六</td>
                            </tr>
                            <tr>
                                <td class="othermonth">28</td>
                                <td class="othermonth">29</td>
                                <td class="othermonth">30</td>
                                <td>
                                    <div class="cir">1</div>
                                </td>
                                <td>2</td>
                                <td>3</td>
                                <td>4</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>6</td>
                                <td>7</td>
                                <td>8</td>
                                <td>9</td>
                                <td>
                                    <div class="cir">23</div>
                                </td>
                                <td>11</td>
                            </tr>
                            <tr>
                                <td>12</td>
                                <td>13</td>
                                <td>14</td>
                                <td>15</td>
                                <td>16</td>
                                <td>17</td>
                                <td>18</td>
                            </tr>
                            <tr>
                                <td>19</td>
                                <td>20</td>
                                <td>21</td>
                                <td>22</td>
                                <td>23</td>
                                <td>24</td>
                                <td>25</td>
                            </tr>
                            <tr>
                                <td>26</td>
                                <td>27</td>
                                <td>28</td>
                                <td>29</td>
                                <td>30</td>
                                <td>31</td>
                                <td class="othermonth">1</td>
                            </tr>
                            <tr>
                                <td class="othermonth">2</td>
                                <td class="othermonth">3</td>
                                <td class="othermonth">4</td>
                                <td class="othermonth">5</td>
                                <td class="othermonth">6</td>
                                <td class="othermonth">7</td>
                                <td class="othermonth">8</td>
                            </tr>
                        </table>
                    </div>
                </div>

            </div>
            <!--活动日历结束-->


            <!--关于我们开始-->
            <div class="about">
                <div class="label_wrapper">
                    <div class="label_title">
                        关注我们
                    </div>
                    <div style="height:2px; background:#16c09b; width:110px; position:absolute; bottom:-2px;"></div>
                </div>

                <!--活动日历内容-->
                <div class="about_content">
                    <div class="pig"><img style="height:100%; width:100%;" src="${ctx}/front/images/pig.png"/></div>
                    <div class="qrcode">
                        <img src="${ctx}/front/images/qr.PNG"/>
                    </div>
                    <div class="intro">微信扫一扫&nbsp;&nbsp;&nbsp;精彩早知道</div>
                </div>
            </div>
            <!--关于我们结束-->
            <!--热搜索标签-->
            <div class="hotlabel">
                <div class="label_wrapper">
                    <div class="label_title">
                        热门标签
                    </div>
                    <div style="height:2px; background:#16c09b; width:110px; position:absolute; bottom:-2px;"></div>
                </div>
                <div class="hotlabel_content">
                    <ul>
                        <li>标签</li>
                        <li>标签</li>
                        <li>标签</li>
                        <li>标签</li>
                        <li>标签</li>
                        <li>标签</li>
                        <li>标签</li>
                        <li>标签</li>
                        <li>标签</li>
                        <li>标签</li>
                        <li>标签</li>
                        <li>标签</li>
                        <li>标签</li>
                        <li>标签</li>
                        <li>标签</li>
                        <li>标签</li>
                        <li>标签</li>
                        <li>标签</li>
                        <li>标签</li>
                        <li>标签</li>
                    </ul>
                </div>
            </div>
            <!--热搜索标签结束-->
            <!--问与答-->
            <div class="question">
                <div class="label_wrapper">
                    <div class="label_title">
                        问与答
                    </div>
                    <div style="height:2px; background:#16c09b; width:110px; position:absolute; bottom:-2px;"></div>
                </div>
                <div class="question_content">
                    <img src="${ctx}/front/images/question.PNG" style="height:100%;; width:100%;"/>
                </div>
            </div>
            <!--问与答结束-->
            <!--活跃用户-->
            <div class="activeusers">
                <div class="label_wrapper">
                    <div class="label_title">
                        活跃用户
                    </div>
                    <div style="height:2px; background:#16c09b; width:110px; position:absolute; bottom:-2px;"></div>
                </div>
                <div class="activeusers_content">
                    <div class="item">
                        <div class="img_wrapper">
                            <img src="${ctx}/front/images/head1.jpg" style=" height:100%; width:100%;"/>
                        </div>
                        <div class="description_wrapper">
                            <div class="name">赵文涛</div>
                            <div class="liveness">活跃度---30%</div>
                            <div class="prog" style="width:20%;"></div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="img_wrapper">
                            <img src="${ctx}/front/images/head2.jpg" style=" height:100%; width:100%;"/>
                        </div>
                        <div class="description_wrapper">
                            <div class="name">赵文涛</div>
                            <div class="liveness">活跃度---30%</div>
                            <div class="prog" style="width:20%;"></div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="img_wrapper">
                            <img src="${ctx}/front/images/head1.jpg" style=" height:100%; width:100%;"/>
                        </div>
                        <div class="description_wrapper">
                            <div class="name">赵文涛</div>
                            <div class="liveness">活跃度---30%</div>
                            <div class="prog" style="width:80%;"></div>
                        </div>
                    </div>


                    <div class="item">
                        <div class="img_wrapper">
                            <img src="${ctx}/front/images/head3.jpg" style=" height:100%; width:100%;"/>
                        </div>
                        <div class="description_wrapper">
                            <div class="name">赵文涛</div>
                            <div class="liveness">活跃度---30%</div>
                            <div class="prog" style="width:20%;"></div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="img_wrapper">
                            <img src="${ctx}/front/images/head2.jpg" style=" height:100%; width:100%;"/>
                        </div>
                        <div class="description_wrapper">
                            <div class="name">赵文涛</div>
                            <div class="liveness">活跃度---30%</div>
                            <div class="prog" style="width:20%;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--活跃用户结束-->
            <!--精品推荐-->
            <div class="recommend">
                <div class="label_wrapper">
                    <div class="label_title">
                        精品推荐
                    </div>
                    <div style="height:2px; background:#16c09b; width:110px; position:absolute; bottom:-2px;"></div>
                </div>
                <div class="recommend_content">
                    <ul>
                        <li>
                            <div class="img_wrapper">
                                <img style="height:100%; width:100%;" src="${ctx}/front/images/yangya.PNG"/>
                            </div>
                            <div class="description_wrapper">三分钟叫你如何养牙</div>
                        </li>
                        <li>
                            <div class="img_wrapper">
                                <img style="height:100%; width:100%;" src="${ctx}/front/images/yangya.PNG"/>
                            </div>
                            <div class="description_wrapper">三分钟叫你如何养牙</div>
                        </li>
                        <li>
                            <div class="img_wrapper">
                                <img style="height:100%; width:100%;" src="${ctx}/front/images/yangya.PNG"/>
                            </div>
                            <div class="description_wrapper">三分钟叫你如何养牙</div>
                        </li>
                        <li>
                            <div class="img_wrapper">
                                <img style="height:100%; width:100%;" src="${ctx}/front/images/yangya.PNG"/>
                            </div>
                            <div class="description_wrapper">三分钟叫你如何养牙</div>
                        </li>
                    </ul>
                </div>
            </div>
            <!--精品推荐结束-->
            <!--最新评论-->
            <div class="lastcomment">
                <div class="label_wrapper">
                    <div class="label_title">
                        最新评论
                    </div>
                    <div style="height:2px; background:#16c09b; width:110px; position:absolute; bottom:-2px;"></div>
                </div>

                <div class="lastcomment_content">
                    <div class="item">
                        <div class="head_wrapper">
                            <img src="${ctx}/front/images/head2.jpg" style="height:100%; width:100%;"/>
                        </div>
                        <div class="comment_wrapper">
                            <div class="name_wrapper"><i>赵文涛</i>[2015-10-01 19:11:22]</div>
                            <div class="comment_content">
                                最新评论最新评论最新评论最新评论最新评论……
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="head_wrapper">
                            <img src="${ctx}/front/images/head2.jpg" style="height:100%; width:100%;"/>
                        </div>
                        <div class="comment_wrapper">
                            <div class="name_wrapper"><i>赵文涛</i>[2015-10-01 19:11:22]</div>
                            <div class="comment_content">
                                最新评论最新评论最新评论最新评论最……
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="head_wrapper">
                            <img src="${ctx}/front/images/head2.jpg" style="height:100%; width:100%;"/>
                        </div>
                        <div class="comment_wrapper">
                            <div class="name_wrapper"><i>赵文涛</i>[2015-10-01 19:11:22]</div>
                            <div class="comment_content">
                                最新评论最新评论最新评论最新评论最……
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="head_wrapper">
                            <img src="${ctx}/front/images/head2.jpg" style="height:100%; width:100%;"/>
                        </div>
                        <div class="comment_wrapper">
                            <div class="name_wrapper"><i>赵文涛</i>[2015-10-01 19:11:22]</div>
                            <div class="comment_content">
                                最新评论最新评论最新评论最新评论最新评论……
                            </div>
                        </div>
                    </div>


                    <div class="item">
                        <div class="head_wrapper">
                            <img src="${ctx}/front/images/head2.jpg" style="height:100%; width:100%;"/>
                        </div>
                        <div class="comment_wrapper">
                            <div class="name_wrapper"><i>赵文涛</i>[2015-10-01 19:11:22]</div>
                            <div class="comment_content">
                                最新评论最新评论最新评论……
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="head_wrapper">
                            <img src="${ctx}/front/images/head2.jpg" style="height:100%; width:100%;"/>
                        </div>
                        <div class="comment_wrapper">
                            <div class="name_wrapper"><i>赵文涛</i>[2015-10-01 19:11:22]</div>
                            <div class="comment_content">
                                最新评论最新评论最新评论最新评论最新评论
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--最新评论结束-->
            <!--每日荐书开始-->
            <div class="reommand_book">
                <div class="label_wrapper">
                    <div class="label_title">
                        每日荐书
                    </div>
                    <div style="height:2px; background:#16c09b; width:110px; position:absolute; bottom:-2px;"></div>
                </div>
                <div class="reommand_book_content">
                    <div class="item">
                        <div class="book_img">
                            <img src="${ctx}/front/images/book.PNG" style="height:100%; width:100%;"/>
                        </div>
                        <div class="book_content">
                            <h5 style="color:#333333; font-weight:700px;">书名</h2>
                                推荐理由推荐理由推荐理由推荐理由推荐理由推荐理由推……
                        </div>
                    </div>

                    <div class="item">
                        <div class="book_img">
                            <img src="${ctx}/front/images/book.PNG" style="height:100%; width:100%;"/>
                        </div>
                        <div class="book_content">
                            <h5 style="color:#666666;">每日荐书</h2>
                                推荐理由推荐理由理由推荐理由推荐理由推荐理由推荐理…………
                        </div>
                    </div>

                    <div class="item">
                        <div class="book_img">
                            <img src="${ctx}/front/images/book.PNG" style="height:100%; width:100%;"/>
                        </div>
                        <div class="book_content">
                            <h5 style="color:#666666;">每日荐书</h2>
                                推荐理由推荐理由推荐理由推荐……
                        </div>
                    </div>
                </div>
            </div>
            <!--每日荐书结束-->
            <!--会员统计-->
            <div class="usercount">
                <div class="label_wrapper">
                    <div class="label_title">
                        会员统计
                    </div>
                    <div style="height:2px; background:#16c09b; width:110px; position:absolute; bottom:-2px;"></div>
                </div>
                <div class="usercount_content">
                    <img src="${ctx}/front/images/usercount.jpg" style="height:100%; width:100%;"/>
                    <div class="descrption">本站会员数</div>
                    <div class="userofcount">100人</div>
                </div>
            </div>
            <!--会员统计结束-->
        </div>
        <!--右边结束-->
    </div>
</div>
</@layout>