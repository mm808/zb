<#include "/layout/front/default/default-layout.ftl">
<@layout "成功提示">
<div class="main" id="center">
    <div class="main_top">
        <span class="label label-default">HI,赶紧登录口腔盒子大家庭，一起交流知识分享快乐吧！</span>
    </div>
    <div class="main_lable">
        <div class="button_c"></div>
    </div>
    <div class="main_login">
        <div style="height: 40px; line-height: 40px; font-size: 18px;">
            <img style="height: 40px; width: 40px;" src="${ctx}/front/images/12323.png" />${(msg)!}
        </div>
        <div style="height: 40px; margin-top: 25px;">
            <#if nextUrl??>
                <script>setTimeout("window.location.href ='${nextUrl}';", 5000);</script>
                <a href="${nextUrl}" class="btn btn-default">如果您的浏览器没有自动跳转，请点击此链接</a>
            <#else>
                <a href="javascript:history.back();" class="btn btn-default">点击这里返回上一页</a>
            </#if>
        </div>

    </div>
</div>
</@layout>