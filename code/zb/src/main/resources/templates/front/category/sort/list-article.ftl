﻿<#include "/layout/front/category/category-layout.ftl">
<@articleLayout sort.name>
<div class="main">
    <div class="nav">
        <div class="left"></div>
        <div class="middle_wrapper">
            <div id="lastnews_slideup" class="middle">
                <ul class="slideup">
                    <li onclick='location.href="#"'><i class="point">回首历史：拉手网的命运早就注1</i></li>
                    <li><i class="point">回首历史：拉手网的命运早就注2</i></li>
                    <li><i class="point">回首历史：拉手网的命运早就注3</i></li>
                    <li><i class="point">回首历史：拉手网的命运早就注4</i></li>
                    <li><i class="point">回首历史：拉手网的命运早就注5</i></li>
                    <li><i class="point">回首历史：拉手网的命运早就注6</i></li>
                    <li><i class="point">回首历史：拉手网的命运早就注7</i></li>
                    <li><i class="point">回首历史：拉手网的命运早就注8</i></li>
                </ul>
            </div>
        </div>
        <div class="right">
            <div class="myselect">
                <div class="select_txt">
                    <span id="select_value">文章</span>
                    <img class="downlog" src="${ctx}/front/images/xiala.png"/>
                </div>
                <ul>
                    <li class="selected">文章</li>
                    <li>视频</li>
                    <li>产品</li>
                    <li>测试</li>
                </ul>
            </div>
            <div class="keyword_wrapper">
                <input placeholder="输入关键词" class="keyword" type="text"/>
            </div>
            <div class="search_btn">搜索</div>
        </div>
    </div>

    <div class=" content ">
        <div class="article content_left  ">
            <!--轮播-->
            <div class="examples_body">

                <div class="examples_bg">
                    <@content_list categoryId=category.id modelId=1 limit=4 recomendId=4 coverFlag=true>
                        <#if contentPage??>
                            <#assign list0=contentPage.content.content[0]>
                            <div class="examples_image">
                                <img src="${(list0.content.cover)!}"/>
                                <div class="desc">
                                    <div class="block">
                                        <h6>${(list0.content.title)!}</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="mune_thumb">
                                <ul>
                                    <#list contentPage.content.content as item>
                                        <li>
                                            <a href="${(item.content.cover)!}"></a>
                                            <div class="block">
                                                <h6>${(item.content.title)!}</h6>
                                            </div>
                                        </li>
                                    </#list>
                                </ul>
                            </div>
                        </#if>
                    </@content_list>
                </div>
            </div>

            <!--菜单-->
            <div class="menu_wrapper">
                <ul>
                    <li>
                        <a href="${ctx}/category/${category.id}/1">全部</a>
                    </li>
                    <#if sorts??>
                        <#list sorts as sortItem>
                            <li <#if sortItem.id==sort.id>class="selected"</#if>>
                                <a href="${ctx}/sort/${sortItem.id}/1">${sortItem.name}</a></li>
                        </#list>
                    </#if>
                </ul>
            </div>
            <div class="v_mune">
                <span class="video_fl video_c">子分类</span> <#if subSorts??> <#list subSorts as sub>
                <a class="video_fl" href="${ctx}/sort/${sub.id}/1"> <#-- 备用高亮子二级分类 -->
          <#--<#if sub.id == sort.id></#if>--> ${sub.name} </a> </#list> </#if>
            </div>


            <!--列表-->
            <@content_list_page categoryId=category.id modelId=1 sortId=sort.id>
                <#if contentPage??>
                    <div class="article_content">
                        <#list contentPage.content.content as item>
                            <div class="article_item">
                                <#if (item.content.cover)?? && (item.content.cover)?length gt 0>
                                    <div class="left">
                                        <img style="height: 168px; width: 248px;" src="${(item.content.cover)!}"/>
                                    </div>
                                </#if>
                                <div class="right">
                                    <div class="title_wrapper">
                                        <a target="_BLANK" href="${ctx}/content/${(item.content.id)!}/1">${(item.content.title)!}</a>
                                    </div>
                                    <div class="writer_wrapper">${(item.content.author)!} ${(item.content.issueTime?string("yyyy年MM月dd日 HH:mm"))!}</div>
                                    <div class="content_wrapper">${(item.content.summary)!}</div>
                                    <div class="label_wrapper">
                                        <#list item.tags as tag>
                                            <span class="label_item">${(tag.name)!}</span>
                                        </#list>
                                    </div>
                                    <div class="other_wrapper">
                                        <span class="glyphicon glyphicon-eye-open"></span> 浏览(100)
                                        <span class="glyphicon glyphicon-comment"></span> 回复（100）
                                        <span class="glyphicon glyphicon-heart" style="color: rgb(255, 0, 0);"></span>
                                        喜欢（100）
                                    </div>
                                </div>
                            </div>
                        </#list>
                        <!--item结束-->
                    </div>

                    <div class="pagination_wrapper">
                        <#include "/macro/pagination.ftl">
                        <@pagination list=contentPage.content url=url/>
                    </div>
                </#if>
            </@content_list_page>


        </div>
        <div class="content_right" style="margin-top: 20px;">
            <!--最新导读-->
            <div class="latestreview">
                <div class="label_wrapper">
                    <i>最新导读</i>
                    <div style="height: 2px; background: #16c09b; width: 120px;"></div>
                </div>
                <div class="content_wrapper">
                    <ul>
                        <@content_list categoryId=category.id modelId=1 limit=5 recomendId=4>
                            <#if contentPage??>
                                <#list contentPage.content.content as item>
                                    <li>
                                        <div class="title_txt">
                                            <div class="catagory">文章</div>
                                            <div class="title_content">
                                                <a href="${ctx}/content/${(item.content.id)!}/1">${(item.content.title)!}</a>
                                            </div>
                                        </div>
                                        <div class="content_txt">${(item.content.summary)!}</div>
                                    </li>
                                </#list>
                            </#if>
                        </@content_list>
                    </ul>
                </div>
            </div>
            <!--最新导读结束-->
            <div class=" wantsay">
                <a href="#" rel="点击投稿">
                    <img src="${ctx}/front/images/tg.png"/>
                </a>
            </div>


            <!--关于我们开始-->
            <div class="about">
                <div class="label_wrapper">
                    <div class="label_title">关注我们</div>
                    <div style="height: 2px; background: #16c09b; width: 110px; position: absolute; bottom: -2px;"></div>
                </div>

                <!--活动日历内容-->
                <div class="about_content">
                    <div class="pig">
                        <img style="height: 100%; width: 100%;" src="${ctx}/front/images/pig.png"/>
                    </div>
                    <div class="qrcode">
                        <img src="${ctx}/front/images/qr.PNG"/>
                    </div>
                    <div class="intro">微信扫一扫&nbsp;&nbsp;&nbsp;精彩早知道</div>
                </div>
            </div>
            <!--关于我们结束-->
            <!--最新评论-->
            <div class="lastcomment">
                <div class="label_wrapper">
                    <div class="label_title">最新评论</div>
                    <div style="height: 2px; background: #16c09b; width: 110px; position: absolute; bottom: -2px;"></div>
                </div>

                <div class="lastcomment_content">
                    <div class="item">
                        <div class="head_wrapper">
                            <img src="${ctx}/front/images/head2.jpg" style="height: 100%; width: 100%;"/>
                        </div>
                        <div class="comment_wrapper">
                            <div class="name_wrapper">
                                <i>赵文涛</i>[2015-10-01 19:11:22]
                            </div>
                            <div class="comment_content">最新评论最新评论最新评论最新评论最新评论……</div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="head_wrapper">
                            <img src="${ctx}/front/images/head2.jpg" style="height: 100%; width: 100%;"/>
                        </div>
                        <div class="comment_wrapper">
                            <div class="name_wrapper">
                                <i>赵文涛</i>[2015-10-01 19:11:22]
                            </div>
                            <div class="comment_content">最新评论最新评论最新评论最新评论最……</div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="head_wrapper">
                            <img src="${ctx}/front/images/head2.jpg" style="height: 100%; width: 100%;"/>
                        </div>
                        <div class="comment_wrapper">
                            <div class="name_wrapper">
                                <i>赵文涛</i>[2015-10-01 19:11:22]
                            </div>
                            <div class="comment_content">最新评论最新评论最新评论最新评论最……</div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="head_wrapper">
                            <img src="${ctx}/front/images/head2.jpg" style="height: 100%; width: 100%;"/>
                        </div>
                        <div class="comment_wrapper">
                            <div class="name_wrapper">
                                <i>赵文涛</i>[2015-10-01 19:11:22]
                            </div>
                            <div class="comment_content">最新评论最新评论最新评论最新评论最新评论……</div>
                        </div>
                    </div>


                    <div class="item">
                        <div class="head_wrapper">
                            <img src="${ctx}/front/images/head2.jpg" style="height: 100%; width: 100%;"/>
                        </div>
                        <div class="comment_wrapper">
                            <div class="name_wrapper">
                                <i>赵文涛</i>[2015-10-01 19:11:22]
                            </div>
                            <div class="comment_content">最新评论最新评论最新评论……</div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="head_wrapper">
                            <img src="${ctx}/front/images/head2.jpg" style="height: 100%; width: 100%;"/>
                        </div>
                        <div class="comment_wrapper">
                            <div class="name_wrapper">
                                <i>赵文涛</i>[2015-10-01 19:11:22]
                            </div>
                            <div class="comment_content">最新评论最新评论最新评论最新评论最新评论</div>
                        </div>
                    </div>
                </div>
            </div>
            <!--最新评论结束-->
            <div class="wantsay">
                <a href="#">
                    <img src="${ctx}/front/images/wantsay.png"/>
                </a>
            </div>
        </div>
    </div>
</div>
</@articleLayout>
