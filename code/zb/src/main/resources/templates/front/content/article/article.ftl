<#include "/layout/front/article/article-layout.ftl">
<@articleLayout article.content.title>
<div class="main">
    <div class="nav">
        <div class="left"></div>
        <div class="middle_wrapper">
            <div id="lastnews_slideup" class="middle">
                <ul class="slideup">
                    <li>
                        <i class="point">回首历史：拉手网的命运早就注1</i>
                    </li>
                    <li>
                        <i class="point">回首历史：拉手网的命运早就注2</i>
                    </li>
                    <li>
                        <i class="point">回首历史：拉手网的命运早就注3</i>
                    </li>
                    <li>
                        <i class="point">回首历史：拉手网的命运早就注4</i>
                    </li>
                    <li>
                        <i class="point">回首历史：拉手网的命运早就注5</i>
                    </li>
                    <li>
                        <i class="point">回首历史：拉手网的命运早就注6</i>
                    </li>
                    <li>
                        <i class="point">回首历史：拉手网的命运早就注7</i>
                    </li>
                    <li>
                        <i class="point">回首历史：拉手网的命运早就注8</i>
                    </li>
                </ul>
            </div>
        </div>
        <div class="right">
            <div class="myselect">
                <div class="select_txt">
                    <span id="select_value">文章</span>
                    <img class="downlog" src="${ctx}/front/images/xiala.png"/>
                </div>
                <ul>
                    <li class="selected">文章</li>
                    <li>视频</li>
                    <li>产品</li>
                    <li>测试</li>
                </ul>
            </div>
            <div class="keyword_wrapper">
                <input placeholder="输入关键词" class="keyword" type="text"/>
            </div>
            <div class="search_btn">搜索</div>
        </div>
    </div>
    <div class="content">
        <!--左边内容开始-->
        <div class="content_left">

            <div class="goods_info" style="height: auto;">
                <div class="article_all_t">
                    <h3>${(article.content.title)!}</h3>
                </div>
                <div class="article_all_h">
                    <span class="all_left">作者：</span>
                    <span class="all_right">${(article.content.author)!}</span>
                    <span class="all_left glyphicon glyphicon-share-alt" style="color: #16c09b"></span>
                    <span>分享（100）</span>
                    <span class="glyphicon glyphicon-eye-open"></span>
                    <span class="all_right" style="color: black;">浏览（100）</span>
                    <span class="all_left">来源：</span>
                    <span class="all_right">${(article.content.source)!}</span>
                    <span class="all_left">发布时间：</span>
                    <span class="all_right" style="border: none">${(article.content.issueTime?string("yyyy-MM-dd HH:mm"))!}</span>
                </div>
                <div class="article_all_cotent">${(article.article.txt)!}</div>
                <div class="article_all_b">
                    <button type="button" class="btn btn-danger">
                        <span class="glyphicon glyphicon-heart"></span>
                        喜欢
                    </button>
                    <button type="button" class="btn btn-success">
                        <span class="glyphicon glyphicon-share"></span>
                        分享
                    </button>
                </div>
            </div>


            <div class="usual_title">
                标签 <#if article.tags??> <#list article.tags as tag>
                <span class="video_bq2">${(tag.name)!}</span>
            </#list> </#if>
            </div>

            <div class="all_fy">
                <#if article.previous??>
                    <span>
                        前一篇：
                        <a href="${ctx}/content/${(article.previous.id)!}/1">${(article.previous.title)!}</a>
                    </span>
                </#if> <#if article.next??>
                <span>
                        后一篇：
                        <a href="${ctx}/content/${(article.next.id)!}/1">${(article.next.title)!}</a>
                    </span>
            </#if>
            </div>


            <hr style="height: 2px; background: #f3f3f3; margin: 0px; margin-top: 2px;"/>
            <!--产品介绍结束-->

            <!--声明开始-->
            <div class="statement">
                <div class="left">声明</div>
                <div class="right">
                    1.口腔盒子遵循行业规范，任何转载的稿件都会明确标注作者和来源。2.口腔盒子的原创文章，请转载时务必注明文章作者和"来源：口腔盒子“，不尊重原创的行为可穷盒子或将追究责任。3.作者投稿可能会经口腔盒子编辑修改或补充。
                </div>
            </div>
            <!--声明结束-->
            <!--相关产品开始-->
            <#if article.relations??>
                <div class="usual_title">相关文章</div>
                <div id="slides" style="height: 170px; margin: 0px;">

                    <ul class="slides_container">
                        <#list article.relations as relation> <#if relation_index%4==0>
                        <li>
                        </#if>
                            <a href="${ctx}/content/${(relation.id)!}" target="_blank">
                                <img src="${attachDomain}/${(relation.cover)!}"/>
                            </a>
                            <#if relation_index%4==3>
                            </li>
                            </#if></#list>
                    </ul>
                    <a href="javascript:" class="prev">prev</a>
                    <a href="javascript:" class="next">next</a>
                </div>
            </#if>

            <!--评论部分开始-->
            <div class="comment">
                <!--网友评论开始-->
                <div class="usual_title" style="margin: 0px;">网友评论</div>
                <div class="comment_wrapper">
                    <form action="${ctx}/comment/add" method="post">
                        <div class="comment_content">
                            <input type="hidden" value="${(article.content.id)!}" name="contentId"/>
                            <textarea id="content0" name="txt" class="form-control"></textarea>
                        </div>
                        <div class="comment_other">
                            <div target-id="content0" class="comment_face bq">表情</div>
                            <div class="comment_operate">
                                <input type="submit" value="发表内容"/>
                            </div>
                        </div>
                    </form>
                </div>
                <!--网友评论结束-->
                <!--评论内容页面开始-->
                <div class="comment_heading">
                    <div class="comment_nav1">
                        <a href="#">全部评论</a>
                    </div>
                    <div class="comment_nav2">
                        <div class="selected">
                            <a href="#">时间</a>
                        </div>
                        <div>
                            <a href="#">最热</a>
                        </div>
                    </div>
                </div>
                <!--评论列表开始-->
                <div class="comment_list">
                    <!--评论item开始-->
                    <#if commentPage.list??> <#list commentPage.list as item>
                        <div class="comment_item">
                            <div class="comment_item_left">
                                <img src="${ctx}/static/images/231.jpg"/>
                            </div>
                            <div class="comment_item_right">
                                <div class="comment_person">
                                    <a href="#" class="username"></a>
                                ${(item.replyTime?string("yyyy-MM-dd"))!}
                                </div>
                                <div class="comment_txt">${item.txt}</div>
                                <div class="comment_other">
                                    <a class="love" href="javascript:">(100)</a>
                                    <a class="huifu" href="javascript:">回复</a>
                                    &nbsp; &nbsp;
                                </div>
                                <!--子回复开始-->
                                <div class="sub_comment">
                                    <div class="triangle"></div>
                                    <!--子回复列表页结束-->
                                    <div class="sub_comment_list">
                                        <!--子条目开始-->
                                        <div class="sub_comment_item">
                                            <div class="sub_comment_item_left">
                                                <img src="${ctx}/static/images/231.jpg"/>
                                            </div>
                                            <div class="sub_comment_item_right">
                                                <div class="sub_comment_person">
                                                    <a href="#" style="color: #16c09b;">刘明明</a>
                                                    &nbsp;&nbsp;2小时前
                                                </div>
                                                <div class="sub_comment_txt">
                                                    回复内回复内容回复内容回复内容回复内容回复内容回复内容回复内容回复内容回复内容回复内容回复内容回复内容回复内容回复内容回复内容123123123容
                                                </div>
                                                <div class="sub_comment_ohter">
                                                    <a class="love1" href="javascript:">(100)</a>
                                                    <a class="huifu1" href="javascript:">回复</a>
                                                    &nbsp; &nbsp;
                                                </div>
                                            </div>
                                        </div>
                                        <!--子条目结束-->
                                        <!--分页开始-->
                                        <div class="pagination_wrapper">
                                            <ul class="pagination pagination-sm">
                                                <li>
                                                    <a href="#">&laquo;</a>
                                                </li>
                                                <li>
                                                    <a href="#">1</a>
                                                </li>
                                                <li>
                                                    <a href="#">&raquo;</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!--分页结束-->
                                        <!--子回复底部开始-->
                                        <div class="sub_comment_footer">
                                            <div class="sub_comment_wrapper">
                                                <form action="#" method="post">
                                                    <div class="sub_comment_content">
                                                        <textarea id="content1" class="form-control"></textarea>
                                                    </div>
                                                    <div class="sub_comment_other">
                                                        <div target-id="content1" class="sub_comment_face bq">表情</div>
                                                        <div class="sub_comment_operate">
                                                            <input type="submit" value="发表内容"/>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <!--子回复底部结束-->
                                    </div>
                                    <!--子回复列表页开始-->
                                </div>
                                <!--子回复结束-->
                            </div>
                        </div>
                    </#list> </#if>
                    <!--评论item结束-->
                </div>
                <!--评论列表结束-->
                <!--评论内容页面结束-->
                <!--评论分页-->
                <!-- <div class="load">加载更多</div> -->
                <div class="pagination_wrapper">
                    <#include "/macro/pagination.ftl">
                    <@pagination list=commentPage url="${ctx}/content/${(article.content.id)!}" />
                </div>
            </div>
            <!--评论部分结束-->
        </div>
        <!--左边内容结束-->
        <!--右边内容开始-->
        <div class="content_right">

            <!--        /**/-->
            <div class="grxx_all">
                <div class="grxx_left">
                    <#if (member.face)?? && (member.face!='')>
                        <img src="${(member.face)!}" style="height: 100%; width: 100%;"/>
                    <#else>
                        <img src="${ctx}/front/images/231.jpg" style="height: 100%; width: 100%;"/>
                    </#if>
                </div>
                <div class="grxx_right">
                    <h4>${(member.username)!}</h4>
                ${(member.sign)!}
                </div>
                <div class="video_wz">
                    <div style="color: #16c09b">${(pageInfo.totalElements)!}</div>
                    <div>文章</div>
                </div>
                <div class="video_wz" style="border: none">

                    <div style="color: #16c09b">68</div>
                    <div>未完成</div>
                </div>
                <div class="video_ckgd">
                ${(member.username)!}&nbsp;&nbsp;&nbsp;&nbsp;
                    <a>更多文章>></a>
                </div>
            </div>
            <div class="grxx_all grxx_c1">
                <#if (pageInfo.content)??>
                    <#list pageInfo.content as item>
                        <div class="grxx_list">
                            <span>${item_index+1}</span>
                            <a href="${ctx}/content/${(item.id)!}/1">${(item.title)!}</a>
                        </div>
                    </#list>
                </#if>
            </div>
            <!--最新导读结束-->

            <div class=" wantsay" style="margin-top: 20px;">
                <a href="#" rel="点击投稿">
                    <img src="${ctx}/front/images/tg.png"/>
                </a>
            </div>


            <!--关于我们开始-->
            <div class="about">
                <div class="label_wrapper">
                    <div class="label_title">关注我们</div>
                    <div style="height: 2px; background: #16c09b; width: 110px; position: absolute; bottom: -2px;"></div>
                </div>

                <!--活动日历内容-->
                <div class="about_content">
                    <div class="pig">
                        <img style="height: 100%; width: 100%;" src="${ctx}/front/images/pig.png"/>
                    </div>
                    <div class="qrcode">
                        <img src="${ctx}/front/images/qr.PNG"/>
                    </div>
                    <div class="intro">微信扫一扫&nbsp;&nbsp;&nbsp;精彩早知道</div>
                </div>
            </div>
            <!--关于我们结束-->
            <!--最新评论-->
            <div class="lastcomment">
                <div class="label_wrapper">
                    <div class="label_title">最新评论</div>
                    <div style="height: 2px; background: #16c09b; width: 110px; position: absolute; bottom: -2px;"></div>
                </div>

                <div class="lastcomment_content">
                    <div class="item">
                        <div class="head_wrapper">
                            <img src="${ctx}/front/images/head2.jpg" style="height: 100%; width: 100%;"/>
                        </div>
                        <div class="comment_wrapper">
                            <div class="name_wrapper">
                                <i>赵文涛</i>[2015-10-01 19:11:22]
                            </div>
                            <div class="comment_content">最新评论最新评论最新评论最新评论最新评论……</div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="head_wrapper">
                            <img src="${ctx}/front/images/head2.jpg" style="height: 100%; width: 100%;"/>
                        </div>
                        <div class="comment_wrapper">
                            <div class="name_wrapper">
                                <i>赵文涛1111</i>[2015-10-01 19:11:22]
                            </div>
                            <div class="comment_content">最新评论最新评论最新评论最新评论最……</div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="head_wrapper">
                            <img src="${ctx}/front/images/head2.jpg" style="height: 100%; width: 100%;"/>
                        </div>
                        <div class="comment_wrapper">
                            <div class="name_wrapper">
                                <i>赵文涛</i>[2015-10-01 19:11:22]
                            </div>
                            <div class="comment_content">最新评论最新评论最新评论最新评论最……</div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="head_wrapper">
                            <img src="${ctx}/front/images/head2.jpg" style="height: 100%; width: 100%;"/>
                        </div>
                        <div class="comment_wrapper">
                            <div class="name_wrapper">
                                <i>赵文涛</i>[2015-10-01 19:11:22]
                            </div>
                            <div class="comment_content">最新评论最新评论最新评论最新评论最新评论……</div>
                        </div>
                    </div>


                    <div class="item">
                        <div class="head_wrapper">
                            <img src="${ctx}/front/images/head2.jpg" style="height: 100%; width: 100%;"/>
                        </div>
                        <div class="comment_wrapper">
                            <div class="name_wrapper">
                                <i>赵文涛</i>[2015-10-01 19:11:22]
                            </div>
                            <div class="comment_content">最新评论最新评论最新评论……</div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="head_wrapper">
                            <img src="${ctx}/front/images/head2.jpg" style="height: 100%; width: 100%;"/>
                        </div>
                        <div class="comment_wrapper">
                            <div class="name_wrapper">
                                <i>赵文涛</i>[2015-10-01 19:11:22]
                            </div>
                            <div class="comment_content">最新评论最新评论最新评论最新评论最新评论</div>
                        </div>
                    </div>
                </div>
            </div>
            <!--最新评论结束-->
            <div class="wantsay" style="margin-top: 20px;">
                <a href="#">
                    <img src="${ctx}/front/images/wantsay.png"/>
                </a>
            </div>


        </div>
        <!--右边结束-->
    </div>
</div>
</@articleLayout>