$('.table').treegrid();

function add(parentId, url) {
    $('#mainModal .modal-header .modal-title').empty().text('添加栏目');
    // 清空form
    $('#mainForm')[0].reset();
    // 将所有select option设为可用
    $('#parentId').find('option:disabled').prop('disabled', false);
    $('#parentId').val(parentId);
    $('#parentId').selectpicker('refresh');


    $('#submitUrl').val(url);
    $("#mainModal").modal('show');
}

function edit(id, url) {
    $('#mainModal .modal-header .modal-title').empty().text('修改栏目');
    $('#submitUrl').val(url);
    $.ajax({
        url: ADMIN_PATH + '/category/get/' + id,
        dataType: 'json',
        type: 'GET',
        contentType: 'application/json;charset=utf-8',
        success: function (data) {
            if (data.success && data.data != null) {
                var category = data.data;
                $('#id').val(category.id);
                $('#parentId').val(category.parentId);
                $('#parentId').find('option:disabled').prop('disabled', false);
                $("#parentId option[value=" + category.id + "]").attr('disabled', 'disabled');
                $('#parentId').selectpicker('refresh');

                $('#name').val(category.name);
                $('#title').val(category.title);
                $('#keyword').val(category.keyword);
                $('#description').val(category.description);
                return;
            } else {
                swal(data.title, data.msg, 'error');
            }
        },
        error: function (data) {
            swal('错误', '网络或者连接异常,请稍后再试.', 'error');
        }
    });
    $("#mainModal").modal('show');
};

function del(id, url) {
    swal({
        title: "确认",
        text: "确定删除选中的项吗?",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: '取消',
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "删除",
        closeOnConfirm: false
    }, function () {
        $.ajax({
            url: url + id,
            dataType: "json",
            type: "DELETE",
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                if (data.success) {
                    swal(data.title, data.msg, "success");
                    setTimeout(function () {
                        window.location.reload();
                    }, 3000);
                    return;
                } else {
                    swal(data.title, data.msg, 'error');
                }
            },
            error: function (data) {
                swal('错误', '网络或者连接异常,请稍后再试.', "error");
            }
        });
    });
}

$('#mainForm').validate({
    rules: {
        moduleId: 'required',
        name: {
            required: true,
            minlength: 2,
            maxlength: 255
        },
        title: {
            required: true,
            minlength: 2,
            maxlength: 255
        },
        keyword: {
            required: true,
            minlength: 2,
            maxlength: 255
        },
        description: {
            required: true,
            minlength: 2,
            maxlength: 255
        }
    },
    messages: {
        moduleId: {
            required: "请选择栏目类型"
        },
        name: {
            required: "请输入栏目名称.",
            minlength: '名称至少2个字符(汉字也为1个字符)',
            maxlength: '名称至多255个字符(汉字也为1个字符)'
        },
        title: {
            required: '请输入栏目标题',
            minlength: '标题至少2个字符(汉字也为1个字符)',
            maxlength: '标题最多255个字符(汉字也为1个字符)'
        },
        keyword: {
            required: '请输入栏目关键词',
            minlength: '关键词最少2个字符(汉字也为1个字符)',
            maxlength: '关键词最多255个字符(汉字也为1个字符)'
        },
        description: {
            required: '请输入栏目描述',
            minlength: '描述最少255个字符(汉字也为1个字符)',
            maxlength: '描述最多255个字符(汉字也为1个字符)'
        }
    },
    submitHandler: function (form) {
        var formJson = $('#mainForm').serializeJSON();
        var url = $('#submitUrl').val();
        // alert(JSON.stringify(formJson));
        $.ajax({
            url: url,
            dataType: 'json',
            data: JSON.stringify(formJson),
            type: 'POST',
            contentType: 'application/json;charset=utf-8',
            success: function (data) {
                if (data.success) {
                    $('#mainModal').modal('hide');
                    swal({
                        title: data.title + '3秒后刷新该页面.',
                        text: data.msg,
                        type: "success"
                    });
                    setTimeout(function () {
                        window.location.reload();
                    }, 3000);
                    return;
                } else {
                    swal(data.title, JSON.stringify(data.data), 'error');
                }
            },
            error: function (data) {
                swal('错误', '网络或者连接异常,请稍后再试.', 'error');
            }
        });
    }
});