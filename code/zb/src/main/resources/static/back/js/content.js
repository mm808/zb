var $table = $('#table');
selections = [];

// 初始化时间拾取
$('.input-daterange').datepicker({
    format: 'yyyy-mm-dd',
    keyboardNavigation: false
});

// ztree的点击事件
function onClick(event, treeId, treeNode, clickFlag) {
    var categoryId = treeNode.id;
    $('#categoryId').val(categoryId);
    $table.bootstrapTable('refresh');
}

$(function () {
    // 左侧ztree
    $.ajax({
        url: ADMIN_PATH + '/category/get-all',
        dataType: 'json',
        type: 'GET',
        contentType: 'application/json;charset=utf-8',
        success: function (data) {
            if (data.success) {
                var zNodes = data.data;
                var zArr = new Array();
                if (zNodes != null && zNodes.length > 0) {
                    var setting = {
                        data: {
                            simpleData: {
                                enable: true,
                                pIdKey: 'parentId'
                            }
                        },
                        callback: {
                            onClick: onClick
                        }
                    };
                    var treeObj = $.fn.zTree.init($("#mainTree"), setting, zNodes);
                    treeObj.expandAll(true);
                }
                return;
            } else {
                swal(data.title, data.msg, 'error');
            }
        },
        error: function (data) {
            swal('错误', '网络或者连接异常,请稍后再试.', 'error');
        }
    });

    // 初始化表格
    $table.bootstrapTable({
        sidePagination: 'server',
        pagination: 'true',
        url: ADMIN_PATH + '/content/get-by-con',
        // toolbar : '#toolbar',
        queryParams: function (params) {
            var limit = params.limit;
            var page = params.offset / limit + 1;
            var categoryId = $('#categoryId').val();
            var title = $('#searchTitle').val();
            var startTime = $('#searchStartTime').val();
            var endTime = $('#searchEndTime').val();

            var par = {};
            par.page = page;
            par.rows = limit;
            par.categoryId = categoryId;
            if (title != null && title != '') {
                par.title = title;
            }
            par.startTime = startTime;
            par.endTime = endTime;
            // alert(JSON.stringify(par));
            return par;
        },
        columns: [{
            field: 'state',
            checkbox: true,
        }, {
            title: 'ID',
            field: 'id',
        }, {
            field: 'title',
            title: '标题',
        }, {
            field: 'url',
            title: '访问',
            formatter: function (index, row) {
                var href = FRONT_PATH + '/content/' + row.id;
                return '<a target="_BLANK" href="' + href + '">访问</a>';
            }
        }],
        responseHandler: function (res) {
            res.rows = res.content;
            res.total = res.totalElements;
            return res;
        }
    });

    // 搜索按钮点击事件
    $('#searchBtn').click(function () {
        var formJson = $('#searchForm').serializeJSON();
        // alert(JSON.stringify(formJson));
        $table.bootstrapTable('refresh');
    });
});

// 获得表格的选中项的id数组
function getIdSelections() {
    return $.map($table.bootstrapTable('getSelections'), function (row) {
        return row.id;
    });
}

// 点击添加按钮
function add() {
    var categoryId = $('#categoryId').val();
    if (categoryId == null || categoryId == '') {
        swal('错误', '请选择栏目(点击左侧选择栏目).', "error");
        return;
    } else {
        var url = ADMIN_PATH + '/content/add/' + categoryId;
        window.open(url);
    }
}

// 点击修改按钮
function edit() {
    var selectId = getIdSelections();
    if (selectId != null) {
        if (selectId.length == 1) {
            var url = ADMIN_PATH + '/content/edit/' + selectId;
            window.open(url);
            return;
        } else {
            swal('错误', '请勾选, 并确保仅勾选了一篇内容.', "error");
            return;
        }
    } else {
        swal('错误', '请勾选要修改的文章', "error");
        return;
    }
}

// 批量删除
function batchRemove() {
    var ids = getIdSelections();

    if (ids == null || ids.length == 0) {
        swal('错误', '请勾选想删除的项.', "error");
        return;
    } else {
        swal({
            title: "确认",
            text: "确定删除选中的项吗?",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: '取消',
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "删除",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                url: ADMIN_PATH + '/content/del-by-ids',
                dataType: "json",
                data: '[' + ids + ']',
                type: "DELETE",
                contentType: "application/json;charset=utf-8",
                success: function (data) {
                    swal(data.title, data.msg, "success");
                },
                error: function (data) {
                    swal('错误', '网络或者连接异常,请稍后再试.', "error");
                }
            });
            $table.bootstrapTable('remove', {
                field: 'id',
                values: ids
            });
        });
    }
}