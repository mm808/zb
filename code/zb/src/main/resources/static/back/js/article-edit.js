var coverURL = $('#cover').val();
// 有封面, 得预览封面
if (coverURL != null && coverURL != '') {
    $("#file").fileinput({
        uploadUrl: COMMON_PATH + '/upload/upload-ret-url',
        overwriteInitial: false,
        language: 'zh',
        allowedFileTypes: ['image'],
        allowedFileExtensions: ['jpg', 'png', 'gif'],
        maxFilesNum: 1,
        maxFileSize: 2000,
        autoReplace: true,
        initialPreview: [ // 预览图片的设置
            '<img src="' + coverURL + '" class="file-preview-image">']
    });
}
// 没封面, 不预览
else {
    $("#file").fileinput({
        uploadUrl: COMMON_PATH + '/upload/upload-ret-url',
        overwriteInitial: false,
        language: 'zh',
        allowedFileTypes: ['image'],
        allowedFileExtensions: ['jpg', 'png', 'gif'],
        maxFilesNum: 1,
        autoReplace: true,
        maxFileSize: 2000
    });
}

$('#file').on('fileuploaded', function (event, data, previewId, index) {
    var response = data.response;
    if (response.success) {
        var url = response.data;
        // alert(url);
        $('#cover').val(url);
    }
});

// 初始化时间拾取
$('#issueTime').datepicker({
    format: 'yyyy-mm-dd'
});

$(function () {
    var ue = UE.getEditor('txt');

    // 添加文章时, 校验并提交
    $('#mainForm').submit(function () {
        UE.getEditor('txt').sync();
    }).validate({
        ignore: "",
        rules: {},
        messages: {},
        errorPlacement: function (label, element) {
            // alert(label);
            label.insertAfter(element.is("#txt") ? element.next() : element);
        }
    });
});

