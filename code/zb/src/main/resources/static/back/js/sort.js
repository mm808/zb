$('.table').treegrid();

function add(parentId, url) {
    $('#mainModal .modal-header .modal-title').empty().text('添加二级分类');
    // 清空form
    $('#mainForm')[0].reset();
    // 将所有select option设为可用
    $('#parentId').find('option:disabled').prop('disabled', false);
    $('#parentId').val(parentId);
    $('#parentId').selectpicker('refresh');

    $('#id').val('');
    $('#submitUrl').val(url);
    $("#addModal").modal('show');
}

function edit(id, url) {
    $('#mainModal .modal-header .modal-title').empty().text('修改二级分类');
    $('#submitUrl').val(url);
    $.ajax({
        url: ADMIN_PATH + '/sort/get/' + id,
        dataType: 'json',
        type: 'GET',
        contentType: 'application/json;charset=utf-8',
        success: function (data) {
            if (data.success && data.data != null) {
                var sort = data.data;
                $('#id').val(sort.id);
                $('#parentId').val(sort.parentId);
                $('#parentId').find('option:disabled').prop('disabled', false);
                $("#parentId option[value=" + sort.id + "]").attr('disabled', 'disabled');
                // $('#parentId').prop('disabled', true);
                $('#parentId').selectpicker('refresh');

                $('#name').val(sort.name);
                $('#title').val(sort.title);
                $('#keyword').val(sort.keyword);
                $('#description').val(sort.description);
                return;
            } else {
                swal(data.title, data.msg, 'error');
            }
        },
        error: function (data) {
            swal('错误', '网络或者连接异常,请稍后再试.', 'error');
        }
    });
    $("#addModal").modal('show');
};

function del(id, url) {
    swal({
        title: "确认",
        text: "确定删除选中的项吗?",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: '取消',
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "删除",
        closeOnConfirm: false
    }, function () {
        $.ajax({
            url: url,
            dataType: "json",
            type: "DELETE",
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                if (data.success) {
                    swal(data.title, data.msg, "success");
                    setTimeout(function () {
                        window.location.reload();
                    }, 3000);
                    return;
                } else {
                    swal(data.title, data.msg, 'error');
                }
            },
            error: function (data) {
                swal('错误', '网络或者连接异常,请稍后再试.', "error");
            }
        });
    });
}

$('#mainForm').validate({
    rules: {},
    messages: {},
    submitHandler: function (form) {
        // 不能用这个, 否则select会取不到值
        // var formJson = $('#mainForm').find('input').not('[value=""]').serializeJSON();
        var formJson = $('#mainForm').serializeJSON();
        var url = $('#submitUrl').val();
        // alert(JSON.stringify(formJson));
        $.ajax({
            url: url,
            dataType: 'json',
            data: JSON.stringify(formJson),
            type: 'POST',
            contentType: 'application/json;charset=utf-8',
            success: function (data) {
                if (data.success) {
                    $('#mainModal').modal('hide');
                    swal({
                        title: data.title + '3秒后刷新该页面.',
                        text: data.msg,
                        type: "success"
                    });
                    setTimeout(function () {
                        window.location.reload();
                    }, 3000);
                    return;
                } else {
                    swal(data.title, JSON.stringify(data.data), 'error');
                }
            },
            error: function (data) {
                swal('错误', '网络或者连接异常,请稍后再试.', 'error');
            }
        });
    }
});