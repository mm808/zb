$("#file").fileinput({
    uploadUrl: COMMON_PATH + '/upload/upload-ret-url',
    overwriteInitial: false,
    language: 'zh',
    allowedFileTypes: ['image'],
    allowedFileExtensions: ['jpg', 'png', 'gif'],
    maxFilesNum: 1,
    maxFileSize: 2000
});

$('#file').on('fileuploaded', function (event, data, previewId, index) {
    var response = data.response;
    if (response.success) {
        var url = response.data;
        // alert(url);
        $('#cover').val(url);
    }
});

// 初始化时间拾取
$('#issueTime').datepicker({
    format: 'yyyy-mm-dd'
});

$(function () {
    var ue = UE.getEditor('txt');

    // 添加文章时, 校验并提交
    $('#mainForm').submit(function () {
        UE.getEditor('txt').sync();
    }).validate({
        ignore: "",
        rules: {
           title: {
                required: true,
                minlength: 2,
                maxlength: 255
            },
            keyword: {
                minlength: 2,
                maxlength: 255
            },
            description: {
                minlength: 2,
                maxlength: 255
            },
            tags: {
                minlength: 2,
                maxlength: 255
            },
            source: {
                minlength: 2,
                maxlength: 255
            },
            relationIds: {
                minlength: 1,
                maxlength: 255
            },
            summary: {
                minlength: 2,
                maxlength: 255
            },
            txt: {
                required: true,
                minlength: 10,
                maxlength: 10000
            }
        },
        messages: {
            title: {
                required: '请输入标题',
                minlength: '至少2个字符(汉字也为1个字符)',
                maxlength: '最多255个字符(汉字也为1个字符)'
            },
            keyword: {
                minlength: '至少2个字符(汉字也为1个字符)',
                maxlength: '至多255个字符(汉字也为1个字符)'
            },
            description: {
                minlength: '至少2个字符(汉字也为1个字符)',
                maxlength: '最多255个字符(汉字也为1个字符)'
            },
            tags: {
                minlength: '最少2个字符(汉字也为1个字符)',
                maxlength: '最多255个字符(汉字也为1个字符)'
            },
            source: {
                minlength: '最少2个字符(汉字也为1个字符)',
                maxlength: '最多255个字符(汉字也为1个字符)'
            },
            relationIds: {
                minlength: '最少1个字符',
                maxlength: '最多255个字符'
            },
            summary: {
                minlength: '最少2个字符(汉字也为1个字符)',
                maxlength: '最多255个字符(汉字也为1个字符)'
            },
            txt: {
                required: '请输入内容',
                minlength: '最少10个字符(指的是html字符)',
                maxlength: '最多10000个字符(指的是html字符)'
            }
        },
        errorPlacement: function (label, element) {
            // alert(label);
            label.insertAfter(element.is("#txt") ? element.next() : element);
        }
    });
});