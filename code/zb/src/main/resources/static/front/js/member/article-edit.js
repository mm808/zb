$('#sort').hide();

var coverURL = $('#cover').val();
// 有封面, 得预览封面
if (coverURL != null && coverURL != '') {
    $("#file").fileinput({
        uploadUrl: COMMON_PATH + '/upload/upload-ret-url',
        overwriteInitial: false,
        language: 'zh',
        allowedFileTypes: ['image'],
        allowedFileExtensions: ['jpg', 'png', 'gif'],
        maxFilesNum: 1,
        maxFileSize: 2000,
        initialPreview: [ // 预览图片的设置
            '<img src="' + coverURL + '" class="file-preview-image">']
    });
}
// 没封面, 不预览
else {
    $("#file").fileinput({
        uploadUrl: COMMON_PATH + '/upload/upload-ret-url',
        overwriteInitial: false,
        language: 'zh',
        allowedFileTypes: ['image'],
        allowedFileExtensions: ['jpg', 'png', 'gif'],
        maxFilesNum: 1,
        maxFileSize: 2000
    });
}


$('#file').on('fileuploaded', function (event, data, previewId, index) {
    var response = data.response;
    if (response.success) {
        var url = response.data;
        // alert(url);
        $('#cover').val(url);
    }
});

$(function () {
    //顶级的二级分类
    $('#categoryId').bind('change', function () {
        var categoryId = $('#categoryId').val();
        $('#sort').hide();
        $('#topSort').hide();
        $('#sonSort').hide();
        $.ajax({
            url: FRONT_PATH + '/sort/get-top-by-cat-id',
            dataType: "json",
            data: {
                categoryId: categoryId
            },
            type: "GET",
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                if (data.success) {
                    $('#topSort').html("");
                    var selects = data.data;
                    var optionDef = '<option value="0">请选择</option>';
                    $('#topSort').append(optionDef);
                    $.each(selects, function (i, val) {
                        var option = '<option value="' + val.id + '">' + val.name + '</option>';
                        $('#topSort').append(option);
                    });
                    $('#sort').show();
                    $('#topSort').show();
                    return;
                } else {
                    return;
                }
            },
            error: function () {
                alert('网络异常, 请稍后再试.');
            }
        });
    });
    // 二级分类子分类
    $('#topSort').bind('change', function () {
        $('#sonSort').hide();

        var sortId = $('#topSort').val();
        $('#sortId').val(sortId);
        $.ajax({
            url: FRONT_PATH + '/sort/get-son-by-id',
            dataType: "json",
            data: {
                id: sortId
            },
            type: "GET",
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                if (data.success) {
                    $('#sonSort').html("");
                    var selects = data.data;
                    var optionDef = '<option value="0">请选择</option>';
                    $('#sonSort').append(optionDef);
                    $.each(selects, function (i, val) {
                        var option = '<option value="' + val.id + '">' + val.name + '</option>';
                        $('#sonSort').append(option);
                    });
                    $('#sonSort').show();
                    return;
                } else {
                    return;
                }
            },
            error: function () {
                alert('网络异常, 请稍后再试.');
            }
        });
    });

    $('#sonSort').bind('change', function () {
        var sortId = $('#sonSort').val();
        $('#sortId').val(sortId);
    });

});

UE.getEditor('editor');

$('.video_bq2').click(function () {
    var html = $(this).html();
    var tags = $('#tags').val();
    if (tags == '') {
        tags = html;
    } else {
        tags = tags + "," + html;
    }
    $('#tags').val(tags);
});