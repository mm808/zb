package com.itmuch.box.dao.config;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.itmuch.box.domain.config.Config;

public interface ConfigRepository extends JpaRepository<Config, Integer> {

    List<Config> findByModuleAndName(String module, String name);

    Config findByModuleAndK(String module, String k);
}
