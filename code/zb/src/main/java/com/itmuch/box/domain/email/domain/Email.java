package com.itmuch.box.domain.email.domain;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public class Email {
    // SMTP主机
    private String host;
    // SMTP端口
    private int port;
    // SMTP用户名
    private String username;
    // SMTP密码
    private String password;
    // SMTP认证
    private String auth;
    // 发信人
    private String from;
    // 收信人
    private String to;
    // 邮件主题
    private String subject;
    // 抄送
    private String cc;
    // 密送
    private String bcc;
    // 回复到
    private String replyTo;
    // 邮件内容
    private String content;
    // 附件
    List<MultipartFile> attachments;

    public String getHost() {
        return this.host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return this.port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAuth() {
        return this.auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public String getFrom() {
        return this.from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return this.to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return this.subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getCc() {
        return this.cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getBcc() {
        return this.bcc;
    }

    public void setBcc(String bcc) {
        this.bcc = bcc;
    }

    public String getReplyTo() {
        return this.replyTo;
    }

    public void setReplyTo(String replyTo) {
        this.replyTo = replyTo;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<MultipartFile> getAttachments() {
        return this.attachments;
    }

    public void setAttachments(List<MultipartFile> attachments) {
        this.attachments = attachments;
    }

}
