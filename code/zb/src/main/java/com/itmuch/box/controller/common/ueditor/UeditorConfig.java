package com.itmuch.box.controller.common.ueditor;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;

public class UeditorConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(UeditorConfig.class);
    private static ObjectMapper mapper = new ObjectMapper();
    private static String configString;
    private static JsonNode node;
    private static ConfigItem configItem;

    public static String getConfigString() throws IOException {
        if (StringUtils.isEmpty(configString)) {
            URL url = UeditorConfig.class.getClassLoader().getResource("config.json");
            configString = FileUtils.readFileToString(new File(url.getPath()));
        }
        return configString;
    }

    private static JsonNode getJsonConfig() {
        try {
            String configString2 = getConfigString();
            mapper.configure(Feature.ALLOW_COMMENTS, true);
            node = mapper.readTree(configString2);
        } catch (JsonProcessingException e) {
            LOGGER.debug("读取config.json时, 发生json异常");
        } catch (IOException e) {
            LOGGER.debug("读取config.json时, 发生IO异常");
        }
        return node;
    }

    public static ConfigItem getConfigItems() {
        mapper.configure(Feature.ALLOW_COMMENTS, true);

        if (configItem == null) {
            configItem = new ConfigItem();
            JsonNode jsonNode = getJsonConfig();
            int imageMaxSize = jsonNode.get("imageMaxSize").asInt();
            int videoMaxSize = jsonNode.get("videoMaxSize").asInt();
            int fileMaxSize = jsonNode.get("fileMaxSize").asInt();

            JsonNode imageAllowFilesNode = jsonNode.get("imageAllowFiles");
            JsonNode videoAllowFilesNode = jsonNode.get("videoAllowFiles");
            JsonNode fileAllowFilesNode = jsonNode.get("fileAllowFiles");

            configItem.setImageMaxSize(imageMaxSize);
            configItem.setVideoMaxSize(videoMaxSize);
            configItem.setFileMaxSize(fileMaxSize);
            configItem.setImageAllowFiles(parse(imageAllowFilesNode));
            configItem.setVideoAllowFiles(parse(videoAllowFilesNode));
            configItem.setFileAllowFiles(parse(fileAllowFilesNode));

            try {
                String configString2 = getConfigString();
                configItem = mapper.readValue(configString2, ConfigItem.class);
            } catch (JsonParseException e) {
                LOGGER.debug("读取config.json时, 发生json转换异常");
            } catch (JsonMappingException e) {
                LOGGER.debug("读取config.json时, 发生json映射异常");
            } catch (IOException e) {
                LOGGER.debug("读取config.json时, 发生IO异常");
            }
        }
        return configItem;
    }

    private static List<String> parse(JsonNode node) {
        if (node.isArray()) {
            List<String> list = Lists.newArrayList();
            for (JsonNode item : node) {
                list.add(item.asText());
            }
            return list;
        }
        return null;
    }

}
