package com.itmuch.box.controller.front.index;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
    @RequestMapping("")
    public String home() {
        return "front/index/index";
    }
}
