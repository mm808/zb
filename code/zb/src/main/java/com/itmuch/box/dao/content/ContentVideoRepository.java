package com.itmuch.box.dao.content;

import org.springframework.data.jpa.repository.JpaRepository;

import com.itmuch.box.domain.content.entity.ContentVideo;

public interface ContentVideoRepository extends JpaRepository<ContentVideo, Integer> {
}
