package com.itmuch.box.dao.tag;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.itmuch.box.domain.tag.entity.Tag;

public interface TagRepository extends JpaRepository<Tag, Integer> {
    Tag findTopByName(String name);

    Set<Tag> findByIdIn(Set<Integer> ids);

    Set<Tag> findByDefaultFlag(Boolean defaultFlag);
}
