package com.itmuch.box.util.mail;

import java.util.Map;

import javax.annotation.Resource;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Maps;
import com.itmuch.box.core.exception.BusinessException;
import com.itmuch.box.service.config.ConfigService;

@Component
public class MailSendUtil {
    @Resource
    private MailUtil emailUtil;
    @Autowired
    private ConfigService configService;

    /**
     * 发送注册邮件, 并放入缓存
     * @param request
     * @param email
     * @param vcode
     * @throws BusinessException
     */
    public void sendRegEmail(String email, String vcode) {
        Map<String, Object> root = Maps.newHashMap();
        Map<String, String> map = this.configService.findByModuleAndName4Map("site", "site");
        String frontDomain = map.get("front.domain");
        String url = frontDomain + "/reg-valid/" + vcode;
        root.put("url", url);

        DateTime date = new DateTime();
        root.put("date", date.toDate());
        DateTime endDate = date.plusDays(1);
        root.put("endDate", endDate.toDate());

        root.put("email", email);

        this.emailUtil.sendMail(email, root, MailUtil.Type.REG);
    }
}
