package com.itmuch.box.domain.sort.vo;

public class SortVo {
    private Integer id;
    /**
     * 父id.
     */
    private Integer parentId;

    /**
     * f_category.id.
     */
    private Integer categoryId;

    /**
     * 名称.
     */
    private String name;

    /**
     * 标题.
     */
    private String title;

    /**
     * 关键词.
     */
    private String keyword;

    /**
     * 描述.
     */
    private String description;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentId() {
        return this.parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getCategoryId() {
        return this.categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKeyword() {
        return this.keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
