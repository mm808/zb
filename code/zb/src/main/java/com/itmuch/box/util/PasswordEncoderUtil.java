package com.itmuch.box.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.itmuch.box.core.constant.ConstantCommon;

public class PasswordEncoderUtil {
    public static BCryptPasswordEncoder getEncoder() {
        return new BCryptPasswordEncoder(ConstantCommon.BCrypt_Password_STRENGTH);
    }

    public static String encode(String rawPassword) {
        return PasswordEncoderUtil.getEncoder().encode(rawPassword);
    }

    public static boolean matches(String rawPassword, String encodedPassword) {
        return getEncoder().matches(rawPassword, encodedPassword);
    }
}
