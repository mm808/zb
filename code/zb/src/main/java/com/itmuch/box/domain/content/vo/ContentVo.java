package com.itmuch.box.domain.content.vo;

import java.util.List;
import java.util.Set;

import com.itmuch.box.domain.attach.entity.Attach;
import com.itmuch.box.domain.category.entity.Category;
import com.itmuch.box.domain.content.entity.Content;
import com.itmuch.box.domain.content.entity.ContentArticle;
import com.itmuch.box.domain.tag.entity.Tag;

public class ContentVo {
    // 文章所属栏目
    private Category category;
    //    // 二级分类
    //    private List<Sort> sorts;

    // 内容主表
    private Content content;
    // 文章内容
    private ContentArticle article;
    // 标签
    private Set<Tag> tags;
    // 附件
    private Set<Attach> attachs;
    // 推荐种类 f_recomend.id
    private Set<Integer> recomendIds;
    // 相关文章/视频
    private List<Content> relations;
    // 上一篇
    private Content previous;
    // 下一篇
    private Content next;

    public Category getCategory() {
        return this.category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    //    public List<Sort> getSorts() {
    //        return this.sorts;
    //    }
    //
    //    public void setSorts(List<Sort> sorts) {
    //        this.sorts = sorts;
    //    }

    public Content getContent() {
        return this.content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public ContentArticle getArticle() {
        return this.article;
    }

    public void setArticle(ContentArticle article) {
        this.article = article;
    }

    public Set<Tag> getTags() {
        return this.tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Set<Attach> getAttachs() {
        return this.attachs;
    }

    public void setAttachs(Set<Attach> attachs) {
        this.attachs = attachs;
    }

    public Set<Integer> getRecomendIds() {
        return this.recomendIds;
    }

    public void setRecomendIds(Set<Integer> recomendIds) {
        this.recomendIds = recomendIds;
    }

    public List<Content> getRelations() {
        return this.relations;
    }

    public void setRelations(List<Content> relations) {
        this.relations = relations;
    }

    public Content getPrevious() {
        return this.previous;
    }

    public void setPrevious(Content previous) {
        this.previous = previous;
    }

    public Content getNext() {
        return this.next;
    }

    public void setNext(Content next) {
        this.next = next;
    }

}
