package com.itmuch.box.core.base;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public class BaseElasticService<T extends ElasticsearchRepository<D, Integer>, D> {
    @Autowired
    protected T dao;

    public Iterable<D> findAll() {
        return this.dao.findAll();
    }

    public D findOne(Integer id) {
        return this.dao.findOne(id);
    }

    public D save(D d) {
        return this.dao.save(d);
    }

    public void delete(Integer id) {
        this.dao.delete(id);
    }

}
