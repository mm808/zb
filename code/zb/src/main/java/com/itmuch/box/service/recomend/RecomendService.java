package com.itmuch.box.service.recomend;

import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Sets;
import com.itmuch.box.core.base.BaseService;
import com.itmuch.box.dao.mid.MidContentRecomendRepository;
import com.itmuch.box.dao.recomend.RecomendRepository;
import com.itmuch.box.domain.mid.entity.MidContentRecomend;
import com.itmuch.box.domain.recomend.entity.Recomend;

@Service
@Transactional
@CacheConfig(cacheNames = "recomend")
public class RecomendService extends BaseService<RecomendRepository, Recomend> {
    @Autowired
    private MidContentRecomendRepository midContentRecomendRepository;

    @Cacheable
    @Override
    public List<Recomend> findAll() {
        return super.findAll();
    }

    public Set<Integer> findRecomendIdsByContentId(Integer contentId) {
        Set<MidContentRecomend> set = this.midContentRecomendRepository.findByContentId(contentId);
        if (CollectionUtils.isNotEmpty(set)) {
            Set<Integer> recomendIds = Sets.newHashSet();
            for (MidContentRecomend item : set) {
                recomendIds.add(item.getRecomendId());
            }
            return recomendIds;
        }
        return null;
    }
}
