package com.itmuch.box.dao.member;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.itmuch.box.domain.member.entity.Member;

public interface MemberRepository extends JpaRepository<Member, Integer> {

    Member findByEmail(String email);

    Member findByEmailAndPassword(String email, String password);

    @Query("select m from Member m where m.username = ?1 or m.email = ?1 or m.username = ?2 or m.email = ?2")
    List<Member> findByKeys(String username, String email);
}
