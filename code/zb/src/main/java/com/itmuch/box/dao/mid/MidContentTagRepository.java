package com.itmuch.box.dao.mid;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.itmuch.box.domain.mid.entity.MidContentTag;

public interface MidContentTagRepository extends JpaRepository<MidContentTag, Integer> {

    Set<MidContentTag> findByContentId(Integer contentId);
}
