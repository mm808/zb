package com.itmuch.box.freemarker.directive;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.itmuch.box.core.exception.ParamInvalidException;

import freemarker.core.Environment;
import freemarker.template.TemplateBooleanModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateNumberModel;
import freemarker.template.TemplateScalarModel;

public class DirectiveUtils {
    public static Integer getInt(String key, Map<String, TemplateModel> params) throws TemplateException {
        TemplateModel model = params.get(key);
        if (model == null) {
            return null;
        }
        if (model instanceof TemplateScalarModel) {
            String s = ((TemplateScalarModel) model).getAsString();
            if (StringUtils.isBlank(s)) {
                return null;
            }
            try {
                return Integer.parseInt(s);
            } catch (NumberFormatException e) {
                throw new NumberFormatException(key);
            }
        } else if (model instanceof TemplateNumberModel) {
            return ((TemplateNumberModel) model).getAsNumber().intValue();
        } else {
            throw new ParamInvalidException(new StringBuffer("参数").append(key).append("不合法").toString());
        }
    }

    public static Boolean getBool(String name, Map<String, TemplateModel> params) throws TemplateException {
        TemplateModel model = params.get(name);
        // 不能返回false, 否则如果用在mybatis 动态查询里面,就会有问题了.
        if (model == null) {
            return null;
        }
        if (model instanceof TemplateBooleanModel) {
            return ((TemplateBooleanModel) model).getAsBoolean();
        } else if (model instanceof TemplateNumberModel) {
            return !(((TemplateNumberModel) model).getAsNumber().intValue() == 0);
        } else if (model instanceof TemplateScalarModel) {
            String s = ((TemplateScalarModel) model).getAsString();
            if (!StringUtils.isBlank(s)) {
                return !(s.equals("0") || s.equalsIgnoreCase("false") || s.equalsIgnoreCase("f"));
            }
            // 字符串为空, 返回null, 不能返回false, 否则如果用在mybatis 动态查询里面,就会有问题了.
            else {
                return null;
            }
        } else {
            throw new ParamInvalidException(new StringBuffer("参数").append(name).append("不合法").toString());
        }
    }

    public static String getString(String name, Map<String, TemplateModel> params) throws TemplateException {
        TemplateModel model = params.get(name);
        if (model == null) {
            return null;
        }
        if (model instanceof TemplateScalarModel) {
            return ((TemplateScalarModel) model).getAsString();
        } else if ((model instanceof TemplateNumberModel)) {
            return ((TemplateNumberModel) model).getAsNumber().toString();
        } else {
            throw new ParamInvalidException(new StringBuffer("参数").append(name).append("不合法").toString());
        }
    }

    public static Integer getIntByEnv(String name, Environment env) throws TemplateModelException {
        TemplateModel model = env.getGlobalVariable(name);

        if (model instanceof TemplateNumberModel) {
            return ((TemplateNumberModel) model).getAsNumber().intValue();
        } else {
            throw new ParamInvalidException(new StringBuffer("参数").append(model).append("不合法").toString());
        }
    }

    public static String getStringByEnv(String mame, Environment env) throws TemplateModelException {
        TemplateModel model = env.getGlobalVariable(mame);

        if (model instanceof TemplateScalarModel) {
            return ((TemplateScalarModel) model).getAsString();
        } else {
            throw new ParamInvalidException(new StringBuffer("参数").append(mame).append("不合法").toString());
        }
    }
}
