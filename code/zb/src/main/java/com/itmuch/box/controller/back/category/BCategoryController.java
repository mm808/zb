package com.itmuch.box.controller.back.category;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.itmuch.box.core.base.BaseController;
import com.itmuch.box.core.constant.ConstantCode;
import com.itmuch.box.core.converter.AjaxResult;
import com.itmuch.box.domain.category.entity.Category;
import com.itmuch.box.domain.category.vo.CategoryVo;
import com.itmuch.box.service.category.CategoryService;
import com.itmuch.box.util.DozerUtil;
import com.itmuch.box.util.ErrorMsgUtil;
import com.itmuch.box.util.OrikaUtil;
import com.itmuch.box.util.UrlUtil;

@Controller
@RequestMapping("${adminPath}/category")
public class BCategoryController extends BaseController {
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private UrlUtil urlUtil;

    // 文章管理页
    @RequestMapping(value = "/article", method = RequestMethod.GET)
    public ModelAndView article() {
        Set<Category> categories = this.categoryService.findByModelId(1);
        String url = this.urlUtil.category("category");
        return new ModelAndView("back/category/article/index").addObject("categories", categories).addObject("url", url);
    }

    // 视频管理页
    @RequestMapping(value = "/video", method = RequestMethod.GET)
    public ModelAndView video() {
        Set<Category> categories = this.categoryService.findByModelId(2);
        String url = this.urlUtil.category("category");
        return new ModelAndView("back/category/video/index").addObject("categories", categories).addObject("url", url);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult add(@Valid @RequestBody CategoryVo vo, BindingResult result) {
        if (!result.hasErrors()) {
            Category category = OrikaUtil.map(vo, Category.class);
            Integer parentId = vo.getParentId();
            // 子栏目继承父栏目的内容类型
            if (parentId != null && parentId != 0) {
                Category parentCategory = this.categoryService.findOne(parentId);
                category.setModelId(parentCategory.getModelId());
            }

            this.categoryService.insert(category);
            return new AjaxResult("添加成功.", "栏目添加成功.");
        }
        return ErrorMsgUtil.jsrValidError(result);
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ResponseBody
    public AjaxResult add(@PathVariable Integer id) {
        Category category = this.categoryService.findOne(id);
        if (category != null) {
            return new AjaxResult(category);
        }
        return ErrorMsgUtil.error("错误", "该栏目不存在", ConstantCode.PARAMTER_ERROR_CODE);
    }

    @RequestMapping(value = "/edit/{id}", method = { RequestMethod.PUT, RequestMethod.POST })
    @ResponseBody
    public AjaxResult edit(@PathVariable Integer id, @Valid @RequestBody CategoryVo vo, BindingResult result) {
        if (!result.hasErrors()) {

            Category category = this.categoryService.findOne(id);
            DozerUtil.map(vo, category);
            // TODO 修改栏目时, 不能将父栏目设为其子栏目 递归..
            this.categoryService.update(category);
            return new AjaxResult("修改成功.", "栏目修改成功.");
        }
        return ErrorMsgUtil.jsrValidError(result);
    }

    @RequestMapping(value = "/del/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public AjaxResult delete(@PathVariable Integer id) {
        return this.categoryService.deleteById4Res(id);
    }

    /**
     * ajax查询所有栏目, 用于文章管理上的属性
     * @return 所有栏目
     */
    @RequestMapping(value = "/get-all", method = RequestMethod.GET)
    @ResponseBody
    public AjaxResult selectAll() {
        List<Category> categories = this.categoryService.findAll();
        return new AjaxResult(categories);
    }
}
