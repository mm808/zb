package com.itmuch.box.controller.back.content;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.itmuch.box.core.base.BaseController;
import com.itmuch.box.core.dynamicSpec.jpa.PageVo;
import com.itmuch.box.domain.category.entity.Category;
import com.itmuch.box.domain.content.entity.Content;
import com.itmuch.box.domain.content.vo.ArticleAddVo;
import com.itmuch.box.domain.content.vo.ContentSearchVo;
import com.itmuch.box.domain.recomend.entity.Recomend;
import com.itmuch.box.domain.sort.entity.Sort;
import com.itmuch.box.service.category.CategoryService;
import com.itmuch.box.service.content.ContentArticleService;
import com.itmuch.box.service.content.ContentService;
import com.itmuch.box.service.recomend.RecomendService;
import com.itmuch.box.service.sort.SortService;

@Controller
@RequestMapping("${adminPath}/content")
public class BContentController extends BaseController {
    @Resource
    private CategoryService categoryService;
    @Resource
    private SortService sortService;
    @Resource
    private ContentService contentService;
    @Resource
    private ContentArticleService contentArticleService;
    @Autowired
    private RecomendService recomendService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView index(Integer categoryId) {
        return new ModelAndView("back/content/index");
    }

    @RequestMapping(value = "/get-by-con", method = RequestMethod.GET)
    @ResponseBody
    public Page<Content> selectByCon(ContentSearchVo searchVo, PageVo pageVo) {

        PageRequest pr = new PageRequest(pageVo.getPage() - 1, pageVo.getRows());
        return this.contentService.findByConditionPaged(searchVo, pr);
    }

    /**
     * 添加页面
     * @param categoryId 栏目id
     * @return 页面
     */
    @RequestMapping(value = "/add/{categoryId}", method = RequestMethod.GET)
    public String add(@PathVariable Integer categoryId, HttpServletRequest req) {
        // 当前栏目信息
        Category category = this.categoryService.findOne(categoryId);
        if (category != null) {
            List<Recomend> recomends = this.recomendService.findAll();
            List<Sort> sorts = this.sortService.findByCategoryId(categoryId);
            req.setAttribute("recomends", recomends);
            req.setAttribute("category", category);
            req.setAttribute("sorts", sorts);

            Integer modelId = category.getModelId();
            // 文章
            if (modelId == 1) {
                ArticleAddVo articleAddVo = new ArticleAddVo();
                articleAddVo.setCategoryId(categoryId);

                req.setAttribute("articleAddVo", articleAddVo);
                return "back/content/article/add";
            }
            // 视频
            else if (modelId == 2) {

                ArticleAddVo articleAddVo = new ArticleAddVo();
                articleAddVo.setCategoryId(categoryId);

                req.setAttribute("articleAddVo", articleAddVo);
                req.setAttribute("recomends", recomends);
                req.setAttribute("category", category);
                req.setAttribute("sorts", sorts);
                return "back/content/video/add";
            }
            // 其他
            else {
                return "";
            }

        } else {
            return "";
        }

    }

    //
    //    @RequestMapping(value = "/del-by-ids", method = RequestMethod.DELETE)
    //    @ResponseBody
    //    public Result deleteByIds(@RequestBody Integer[] ids) {
    //        this.contentService.deleteByIds(ids);
    //        return new Result("成功", "已成功删除");
    //    }
    //

}
