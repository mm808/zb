package com.itmuch.box.domain.mid.entity;
// Generated 2015-12-29 22:06:46 by Hibernate Tools 4.3.1.Final

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

/**
 * MidContentRecomend generated by hbm2java
 */
@Entity
@Table(name = "mid_content_recomend", catalog = "yayi_spring_boot")
@DynamicInsert
@DynamicUpdate
public class MidContentRecomend implements java.io.Serializable {

    private Integer id;
    private Integer contentId;
    private Integer recomendId;

    public MidContentRecomend() {
    }

    public MidContentRecomend(Integer contentId, Integer recomendId) {
        this.contentId = contentId;
        this.recomendId = recomendId;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "content_id", nullable = false)
    public Integer getContentId() {
        return this.contentId;
    }

    public void setContentId(Integer contentId) {
        this.contentId = contentId;
    }

    @Column(name = "recomend_id", nullable = false)
    public Integer getRecomendId() {
        return this.recomendId;
    }

    public void setRecomendId(Integer recomendId) {
        this.recomendId = recomendId;
    }

}
