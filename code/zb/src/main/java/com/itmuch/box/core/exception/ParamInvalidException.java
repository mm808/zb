package com.itmuch.box.core.exception;

public class ParamInvalidException extends IllegalArgumentException {
    private static final long serialVersionUID = -290372483676756273L;

    public ParamInvalidException() {
        super();
    }

    public ParamInvalidException(String message) {
        super(message);
    }
}
