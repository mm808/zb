package com.itmuch.box.core.jsr349;

import java.io.Serializable;

public class PasswordVo implements Serializable {
    private static final long serialVersionUID = 1L;

    private String password;

    private String password2;

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword2() {
        return this.password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

}
