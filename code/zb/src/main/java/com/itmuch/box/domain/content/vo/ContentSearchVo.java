package com.itmuch.box.domain.content.vo;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

public class ContentSearchVo {
    private Integer categoryId;
    private List<Integer> sortIds;
    private String title;
    @DateTimeFormat(iso = ISO.DATE)
    private Date startTime;
    @DateTimeFormat(iso = ISO.DATE)
    private Date endTime;

    public Integer getCategoryId() {
        return this.categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public List<Integer> getSortIds() {
        return this.sortIds;
    }

    public void setSortIds(List<Integer> sortIds) {
        this.sortIds = sortIds;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getStartTime() {
        return this.startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return this.endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

}
