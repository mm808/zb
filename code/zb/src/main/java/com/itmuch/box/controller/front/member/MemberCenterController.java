package com.itmuch.box.controller.front.member;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.itmuch.box.core.security.SubjectUtil;
import com.itmuch.box.domain.member.entity.Member;
import com.itmuch.box.service.member.MemberService;

@Controller
@RequestMapping("/member")
public class MemberCenterController {
    @Resource
    private MemberService memberService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView change() {
        Integer id = SubjectUtil.getSubjectId();
        Member member = this.memberService.findOne(id);
        return new ModelAndView("front/member/center/index").addObject("member", member);
    }
}
