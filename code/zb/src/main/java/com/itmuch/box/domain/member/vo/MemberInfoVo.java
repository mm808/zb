package com.itmuch.box.domain.member.vo;

import java.util.Date;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

public class MemberInfoVo {

    /**
     * 头像.
     */
    @Length(max = 256)
    private String face;

    /**
     * 姓名.
     */
    @Length(min = 2, max = 40)
    private String name;

    /**
     * 性别 0未知 1男 2女.
     */
    @Range(min = 0, max = 2)
    private Byte sex;

    /**
     * 民族.
     */
    @Length(min = 1, max = 40)
    private String nation;
    /**
     * 生日.
     */
    @DateTimeFormat(iso = ISO.DATE)
    private Date birthday;

    /**
     * 籍贯.
     */
    @Length(min = 3, max = 80)
    private String originPlace;

    /**
     * 家庭住址.
     */
    @Length(min = 5, max = 80)
    private String homeAddress;

    public String getFace() {
        return this.face;
    }

    public void setFace(String face) {
        this.face = face;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Byte getSex() {
        return this.sex;
    }

    public void setSex(Byte sex) {
        this.sex = sex;
    }

    public String getNation() {
        return this.nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public Date getBirthday() {
        return this.birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getOriginPlace() {
        return this.originPlace;
    }

    public void setOriginPlace(String originPlace) {
        this.originPlace = originPlace;
    }

    public String getHomeAddress() {
        return this.homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

}