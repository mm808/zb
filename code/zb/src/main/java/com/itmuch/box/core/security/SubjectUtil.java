package com.itmuch.box.core.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.itmuch.box.domain.member.entity.Member;

public class SubjectUtil {
    public static Member getSubject() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        Object principal = authentication.getPrincipal();
        Member member = null;
        if (principal != null) {
            if (principal instanceof SecurityUser) {
                member = ((SecurityUser) principal).getMember();
                if (member != null) {
                    return member;
                }
            }
        }
        member = new Member();
        member.setId(-1);
        return member;

    }

    public static Integer getSubjectId() {
        return getSubject().getId();
    }
}
