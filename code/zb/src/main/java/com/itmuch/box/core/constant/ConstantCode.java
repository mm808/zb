package com.itmuch.box.core.constant;

/**
 * 异常类对应编号.
 *
 * @author zhouli
 *
 */
public class ConstantCode {
    /**
     * 成功.
     */
    public static final int SUCCESS_CODE = 1000;

    /**
     * 没有查到数据.
     */
    public static final int DATA_NOT_FOUND = 1001;
    /**
     * 参数出错编号.
     */
    public static final int PARAMTER_ERROR_CODE = 1002;

    /**
     * 重复的数据.
     */
    public static final int DUPLICATE_DATA = 1003;

    /**
     * 不清楚的错误编号.
     */
    public static final int UNKNOW_ERROR_CODE = 1100;

    private ConstantCode() {
    }
}
