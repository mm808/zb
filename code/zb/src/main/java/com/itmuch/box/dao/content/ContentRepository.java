package com.itmuch.box.dao.content;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import com.itmuch.box.domain.content.entity.Content;
import com.itmuch.box.domain.content.vo.ContentSearchVo;

public interface ContentRepository extends JpaRepository<Content, Integer> {

    Long countByCategoryId(int categoryId);

    Page<Content> findAll(Specification<ContentSearchVo> specification, Pageable pageable);

    Page<Content> findByMemberIdAndModelId(Integer memberId, Integer modelId, Pageable pageable);
}
