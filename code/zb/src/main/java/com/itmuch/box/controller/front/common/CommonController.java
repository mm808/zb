package com.itmuch.box.controller.front.common;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CommonController {
    @RequestMapping(value = "/success", method = RequestMethod.GET)
    public String regValidSuccess() {
        return "front/common/success";
    }

    @RequestMapping(value = "/error", method = RequestMethod.GET)
    public String regValidFail() {
        return "front/common/error";
    }
}
