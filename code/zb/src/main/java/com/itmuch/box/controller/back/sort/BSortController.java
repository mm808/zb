package com.itmuch.box.controller.back.sort;

import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.itmuch.box.core.constant.ConstantCode;
import com.itmuch.box.core.converter.AjaxResult;
import com.itmuch.box.domain.category.entity.Category;
import com.itmuch.box.domain.sort.entity.Sort;
import com.itmuch.box.domain.sort.vo.SortVo;
import com.itmuch.box.service.category.CategoryService;
import com.itmuch.box.service.sort.SortService;
import com.itmuch.box.util.DozerUtil;
import com.itmuch.box.util.ErrorMsgUtil;

@Controller
@RequestMapping("${adminPath}/sort")
public class BSortController {
    @Resource
    private SortService sortService;
    @Resource
    private CategoryService categoryService;

    @RequestMapping(value = "/{categoryId}", method = RequestMethod.GET)
    public ModelAndView add(@PathVariable Integer categoryId) {
        Category category = this.categoryService.findOne(categoryId);
        List<Sort> sorts = null;
        if (category != null) {
            sorts = this.sortService.findByCategoryId(categoryId);
            return new ModelAndView("back/sort/index").addObject("sorts", sorts).addObject("category", category);
        }
        return new ModelAndView();

    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult add(@Valid @RequestBody SortVo sortAddVo, BindingResult result) {
        if (!result.hasErrors()) {
            Sort sort = DozerUtil.map(sortAddVo, Sort.class);
            sort.setCategoryId(sortAddVo.getCategoryId());

            this.sortService.insert(sort);
            return new AjaxResult("添加成功.", "二级分类添加成功.");
        }
        return ErrorMsgUtil.jsrValidError(result);
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ResponseBody
    public AjaxResult selectById(@PathVariable Integer id) {
        Sort sort = this.sortService.findOne(id);
        if (sort != null) {
            return new AjaxResult(sort);
        }
        return ErrorMsgUtil.error("错误", "该分类不存在", ConstantCode.PARAMTER_ERROR_CODE);
    }

    @RequestMapping(value = "/edit/{id}", method = { RequestMethod.PUT, RequestMethod.POST })
    @ResponseBody
    public AjaxResult edit(@PathVariable Integer id, @Valid @RequestBody SortVo editVo, BindingResult result) {
        if (!result.hasErrors()) {
            Sort sort = this.sortService.findOne(id);
            DozerUtil.map(editVo, sort);

            this.sortService.update(sort);
            return new AjaxResult("修改成功.", "二级分类修改成功.");
        }
        return ErrorMsgUtil.jsrValidError(result);
    }

    @RequestMapping(value = "/del/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public AjaxResult delete(@PathVariable Integer id) {
        this.sortService.delete(id);
        return new AjaxResult("删除成功", "删除二级分类成功.");
    }
}
