package com.itmuch.box.service.comment;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.itmuch.box.core.base.BaseService;
import com.itmuch.box.dao.comment.CommentRepository;
import com.itmuch.box.domain.comment.entity.Comment;

@Service
@Transactional
public class CommentService extends BaseService<CommentRepository, Comment> {

    public Page<Comment> findByContentId(Integer id, Pageable pageRequest) {
        return this.dao.findByContentId(id, pageRequest);
    }
}
