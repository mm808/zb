package com.itmuch.box.service.member;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.itmuch.box.core.base.BaseService;
import com.itmuch.box.dao.member.MemberLogRepository;
import com.itmuch.box.domain.member.entity.MemberLog;

@Service
@Transactional
public class MemberLogService extends BaseService<MemberLogRepository, MemberLog> {

}
