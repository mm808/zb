package com.itmuch.box.dao.mid;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.itmuch.box.domain.mid.entity.MidContentRecomend;

public interface MidContentRecomendRepository extends JpaRepository<MidContentRecomend, Integer> {
    Set<MidContentRecomend> findByContentId(Integer contentId);
}
