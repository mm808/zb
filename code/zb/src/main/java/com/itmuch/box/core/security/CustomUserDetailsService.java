package com.itmuch.box.core.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.itmuch.box.domain.member.entity.Member;
import com.itmuch.box.service.member.MemberService;

@Component
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private MemberService memberService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Member member = this.memberService.findByKeys(username, username);
        if (member == null) {
            throw new UsernameNotFoundException("用户不存在");
        }

        return new SecurityUser(member);
    }
}