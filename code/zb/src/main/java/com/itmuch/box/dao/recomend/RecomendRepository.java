package com.itmuch.box.dao.recomend;

import org.springframework.data.jpa.repository.JpaRepository;

import com.itmuch.box.domain.recomend.entity.Recomend;

public interface RecomendRepository extends JpaRepository<Recomend, Integer> {
}
