package com.itmuch.box.util;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import com.google.common.collect.Maps;
import com.itmuch.box.core.constant.ConstantCode;
import com.itmuch.box.core.converter.AjaxResult;

public class ErrorMsgUtil {
    private ErrorMsgUtil() {
    }

    public static String msgConverter(List<ObjectError> errors) {
        StringBuilder msg = new StringBuilder();
        if (errors != null) {
            for (ObjectError error : errors) {
                msg.append(error.getDefaultMessage() + " ");
            }
        }
        return msg.toString();
    }

    public static AjaxResult jsrValidError(BindingResult bindingResult) {
        Map<String, List<String>> fieldErrors = Maps.newHashMap();
        for (FieldError fe : bindingResult.getFieldErrors()) {
            String f = fe.getField();

            if (fieldErrors.get(f) != null) {
                fieldErrors.get(f).add(fe.getDefaultMessage());
            } else {
                List<String> list = new LinkedList<String>();
                list.add(fe.getDefaultMessage());
                fieldErrors.put(f, list);
            }
        }
        // ErrorMsgUtil.error("参数错误.", ErrorMsgUtil.msgConverter(bindingResult.getAllErrors()), CodeConstant.PARAMTER_ERROR_CODE);
        return new AjaxResult(false, ConstantCode.PARAMTER_ERROR_CODE, "参数错误", "参数错误", fieldErrors);
    }

    public static AjaxResult error(String title, String msg, int errorCode) {
        return new AjaxResult(false, errorCode, title, msg, null);
    }
}
