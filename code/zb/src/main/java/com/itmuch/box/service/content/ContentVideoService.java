package com.itmuch.box.service.content;

import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Sets;
import com.itmuch.box.core.base.BaseService;
import com.itmuch.box.core.security.SubjectUtil;
import com.itmuch.box.dao.content.ContentVideoRepository;
import com.itmuch.box.dao.elastic.ArticleElasticRepository;
import com.itmuch.box.dao.mid.MidContentRecomendRepository;
import com.itmuch.box.dao.mid.MidContentTagRepository;
import com.itmuch.box.domain.content.entity.Content;
import com.itmuch.box.domain.content.entity.ContentVideo;
import com.itmuch.box.domain.content.vo.VideoAddVo;
import com.itmuch.box.domain.elastic.entity.ArticleElastic;
import com.itmuch.box.domain.mid.entity.MidContentRecomend;
import com.itmuch.box.domain.mid.entity.MidContentTag;
import com.itmuch.box.domain.tag.entity.Tag;
import com.itmuch.box.service.category.CategoryService;
import com.itmuch.box.service.tag.TagService;
import com.itmuch.box.util.DozerUtil;
import com.itmuch.box.util.OrikaUtil;
import com.itmuch.box.util.StringUtil;

@Service
@Transactional
public class ContentVideoService extends BaseService<ContentVideoRepository, ContentVideo> {
    @Autowired
    private ArticleElasticRepository articleElasticRepository;
    @Autowired
    private TagService tagService;
    @Autowired
    private ContentService contentService;
    @Autowired
    private MidContentTagRepository midContentTagRepository;
    @Autowired
    private MidContentRecomendRepository midContentRecomendRepository;
    @Resource
    private CategoryService categoryService;

    public void insertVideo(VideoAddVo videoAddVo) {
        Integer memberId = SubjectUtil.getSubjectId();

        // f_content
        Content content = OrikaUtil.map(videoAddVo, Content.class);
        content.setMemberId(memberId);
        content.setModelId(2);
        Content content2 = this.contentService.insert(content);

        // content_article
        ContentVideo video = OrikaUtil.map(videoAddVo, ContentVideo.class);
        this.insert(video);

        // tags
        String tagsString = videoAddVo.getTagsString();
        if (!StringUtils.isEmpty(tagsString)) {
            Set<MidContentTag> midContentTags = Sets.newHashSet();
            Set<String> tagSet = StringUtil.splitAndTrim(tagsString, ",");
            if (!tagSet.isEmpty()) {
                for (String tagName : tagSet) {
                    Tag tagInDB = this.tagService.findTopByName(tagName);
                    // 该标签已经存在
                    if (tagInDB != null) {
                        // tags.add(tagInDB);
                        MidContentTag midContentTag = new MidContentTag();
                        midContentTag.setContentId(content2.getId());
                        midContentTag.setTagId(tagInDB.getId());
                        midContentTags.add(midContentTag);
                    }
                    // 该标签不存在
                    else {
                        // ① 新建标签
                        Tag tag = new Tag();
                        tag.setName(tagName);
                        Tag newTag = this.tagService.insert(tag);

                        MidContentTag midContentTag = new MidContentTag();
                        midContentTag.setContentId(content2.getId());
                        midContentTag.setTagId(newTag.getId());
                        midContentTags.add(midContentTag);
                    }
                }
            }
            this.midContentTagRepository.save(midContentTags);
        }

        // 推荐
        Set<Integer> recomendIds = videoAddVo.getRecomendIds();
        if (CollectionUtils.isNotEmpty(recomendIds)) {
            Set<MidContentRecomend> midContentRecomends = Sets.newHashSet();
            for (Integer recomendId : recomendIds) {
                MidContentRecomend midContentRecomend = new MidContentRecomend();
                midContentRecomend.setContentId(content2.getId());
                midContentRecomend.setRecomendId(recomendId);
                midContentRecomends.add(midContentRecomend);
            }
            this.midContentRecomendRepository.save(midContentRecomends);
        }

        // 插入文章到elastic
        ArticleElastic articleElastic = DozerUtil.map(videoAddVo, ArticleElastic.class);
        articleElastic.setId(content.getId());
        this.articleElasticRepository.save(articleElastic);
    }
}
