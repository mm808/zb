package com.itmuch.box.controller.common.ueditor;

import java.util.List;

public class ConfigItem {
    private int imageMaxSize;
    private List<String> imageAllowFiles;

    private int videoMaxSize;
    private List<String> videoAllowFiles;

    private int fileMaxSize;
    private List<String> fileAllowFiles;

    public int getImageMaxSize() {
        return this.imageMaxSize;
    }

    public void setImageMaxSize(int imageMaxSize) {
        this.imageMaxSize = imageMaxSize;
    }

    public List<String> getImageAllowFiles() {
        return this.imageAllowFiles;
    }

    public void setImageAllowFiles(List<String> imageAllowFiles) {
        this.imageAllowFiles = imageAllowFiles;
    }

    public int getVideoMaxSize() {
        return this.videoMaxSize;
    }

    public void setVideoMaxSize(int videoMaxSize) {
        this.videoMaxSize = videoMaxSize;
    }

    public List<String> getVideoAllowFiles() {
        return this.videoAllowFiles;
    }

    public void setVideoAllowFiles(List<String> videoAllowFiles) {
        this.videoAllowFiles = videoAllowFiles;
    }

    public int getFileMaxSize() {
        return this.fileMaxSize;
    }

    public void setFileMaxSize(int fileMaxSize) {
        this.fileMaxSize = fileMaxSize;
    }

    public List<String> getFileAllowFiles() {
        return this.fileAllowFiles;
    }

    public void setFileAllowFiles(List<String> fileAllowFiles) {
        this.fileAllowFiles = fileAllowFiles;
    }

}
