package com.itmuch.box.domain.content.vo;

import java.util.Date;
import java.util.Set;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

public class ArticleEditVo {
    private Integer id;
    // 所在栏目
    private Integer categoryId;
    // 所属二级分类
    private Integer sortId;
    // 标题
    @NotBlank
    private String title;
    // 关键词
    private String keyword;
    // 标签
    private String tagsString;
    // 文章封面
    private String cover;
    // 作者
    private String author;
    // 来源
    private String source;
    // 发表时间
    @DateTimeFormat(iso = ISO.DATE)
    private Date issueTime;
    // 该文章的推荐方式
    private Set<Integer> recomendIds;
    // 摘要
    private String summary;
    // 内容
    @NotBlank
    private String txt;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCategoryId() {
        return this.categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getSortId() {
        return this.sortId;
    }

    public void setSortId(Integer sortId) {
        this.sortId = sortId;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKeyword() {
        return this.keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getTagsString() {
        return this.tagsString;
    }

    public void setTagsString(String tagsString) {
        this.tagsString = tagsString;
    }

    public String getCover() {
        return this.cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getAuthor() {
        return this.author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getSource() {
        return this.source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Date getIssueTime() {
        return this.issueTime;
    }

    public void setIssueTime(Date issueTime) {
        this.issueTime = issueTime;
    }

    public Set<Integer> getRecomendIds() {
        return this.recomendIds;
    }

    public void setRecomendIds(Set<Integer> recomendIds) {
        this.recomendIds = recomendIds;
    }

    public String getSummary() {
        return this.summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getTxt() {
        return this.txt;
    }

    public void setTxt(String txt) {
        this.txt = txt;
    }

}