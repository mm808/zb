package com.itmuch.box.core.dynamicSpec.jpa;

import java.util.Locale;

public class PageVo {
    /**
     * 页码
     */
    private Integer page = 1;
    /**
     * 单页显示多少条
     */
    private Integer rows = 10;
    /**
     * 排序字段
     */
    private String sort;
    /**
     * 排序方式
     */
    private String order;

    public Integer getPage() {
        return this.page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getRows() {
        if (this.rows > 50) {
            this.rows = 50;
        } else if (this.rows <= 0) {
            this.rows = 1;
        }
        return this.rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public String getSort() {
        return this.sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getOrder() {
        return this.order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public enum Direction {
        ASC, DESC;

        private Direction() {
        }

        public static Direction fromString(String value) {
            return Direction.valueOf(value.toUpperCase(Locale.US));
        }
    }
}
