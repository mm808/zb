package com.itmuch.box.dao.member;

import org.springframework.data.jpa.repository.JpaRepository;

import com.itmuch.box.domain.member.entity.MemberLog;

public interface MemberLogRepository extends JpaRepository<MemberLog, Integer> {

}
