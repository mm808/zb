package com.itmuch.box.domain.category.vo;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

public class CategoryVo {
    /**
     * id.
     */
    private Integer id;

    /**
     * f_model.id.
     */
    @NotNull
    private Integer modelId;

    /**
     * 父栏目ID.
     */
    @NotNull
    private Integer parentId;

    /**
     * 名称.
     */
    @NotBlank
    @Length(min = 2, max = 50)
    private String name;

    /**
     * 标题.
     */
    @NotBlank
    @Length(min = 2, max = 50)
    private String title;

    /**
     * 关键词.
     */
    @Length(min = 2, max = 255)
    private String keyword;

    /**
     * 描述.
     */
    @Length(min = 2, max = 255, message = "长度在2-255之间")
    private String description;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getModelId() {
        return this.modelId;
    }

    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

    public Integer getParentId() {
        return this.parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKeyword() {
        return this.keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
