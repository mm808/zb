package com.itmuch.box.core.constant;

public final class ConstantRedis {
    // 注册邮箱前缀
    public static final String REG_EMAIL_PREFIX = "PRE_REG_EMAIL_";

    //忘记密码前缀
    public static final String FORGET_PWD_PREFIX = "PRE_FORGET_PWD_";

    // 修改邮箱前缀
    public static final String CHANGE_EMAIL_PREFIX = "PRE_CHANGE_EMAIL_";

    // 修改手机号前缀, 发送给原先手机号.
    public static final String CHANGE_MOBILE_TO_OLD_PREFIX = "PRE_CHANGE_MOBILE_TO_OLD_";

    // 修改手机号前缀, 发送给新手机号.
    public static final String CHANGE_MOBILE_TO_NEW_PREFIX = "PRE_CHANGE_MOBILE_TO_NEW_";

    private ConstantRedis() {

    }

}