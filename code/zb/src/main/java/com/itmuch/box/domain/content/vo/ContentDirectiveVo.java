package com.itmuch.box.domain.content.vo;

import java.util.Set;

public class ContentDirectiveVo {
    // 栏目id
    private Integer categoryId;
    // 模型id 1文章 2视频
    private Integer modelId;
    // 二级分类id
    private Set<Integer> sortIds;
    // 推荐方式 def_recomend.id
    private Integer recomendId;
    // 是否有封面 true有 false无
    private Boolean coverFlag;

    public Integer getCategoryId() {
        return this.categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getModelId() {
        return this.modelId;
    }

    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

    public Set<Integer> getSortIds() {
        return this.sortIds;
    }

    public void setSortIds(Set<Integer> sortIds) {
        this.sortIds = sortIds;
    }

    public Integer getRecomendId() {
        return this.recomendId;
    }

    public void setRecomendId(Integer recomendId) {
        this.recomendId = recomendId;
    }

    public Boolean getCoverFlag() {
        return this.coverFlag;
    }

    public void setCoverFlag(Boolean coverFlag) {
        this.coverFlag = coverFlag;
    }

}
