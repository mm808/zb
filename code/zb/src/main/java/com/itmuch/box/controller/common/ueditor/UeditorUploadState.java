package com.itmuch.box.controller.common.ueditor;

public class UeditorUploadState {
    // 状态
    private String state;
    // 访问路径
    private String url;
    // 文件大小
    private String size;
    // 文件类型
    private String type;
    // 原始文件名
    private String original;

    public String getState() {
        return this.state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSize() {
        return this.size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOriginal() {
        return this.original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public static final String SUCCESS = "SUCCESS";
    public static final String MAX_SIZE = "文件大小超出限制";
    public static final String IO_ERROR = "IO错误";
    public static final String NOTFOUND_UPLOAD_DATA = "未找到上传数据";
    public static final String NOT_ALLOW_FILE_TYPE = "不允许的文件类型";
}
