package com.itmuch.box.dao.attach;

import org.springframework.data.jpa.repository.JpaRepository;

import com.itmuch.box.domain.attach.entity.Attach;

public interface AttachRepository extends JpaRepository<Attach, Integer> {

    Attach findByMemberIdAndUuid(Integer memberId, String uuid);
}
