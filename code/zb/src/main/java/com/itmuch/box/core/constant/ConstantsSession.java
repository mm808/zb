package com.itmuch.box.core.constant;

public class ConstantsSession {
    /**
     * 用户id.
     */
    public static final String MEMBER_FACE = "memberFace";

    /**
     * 验证码在session中的key.
     */
    public static final String KAPTCHA_SESSION_KEY = "KAPTCHA_SESSION_KEY";

}
