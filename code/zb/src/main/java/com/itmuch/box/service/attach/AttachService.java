package com.itmuch.box.service.attach;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.itmuch.box.core.base.BaseService;
import com.itmuch.box.dao.attach.AttachRepository;
import com.itmuch.box.domain.attach.entity.Attach;

@Service
@Transactional
public class AttachService extends BaseService<AttachRepository, Attach> {
    public Attach findByMemberIdAndUuid(Integer memberId, String uuid) {
        return this.dao.findByMemberIdAndUuid(memberId, uuid);
    }
}
