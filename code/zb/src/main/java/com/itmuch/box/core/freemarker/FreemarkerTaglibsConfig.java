package com.itmuch.box.core.freemarker;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 作用: 让freemarker从classpath下加载security.tld
 * 参考: https://github.com/spring-projects/spring-boot/issues/907
 * 参考: https://github.com/wayshall/spring-boot-pit
 * 参考: https://github.com/isopov/fan
 */
@Configuration
public class FreemarkerTaglibsConfig {
    @Bean
    @ConditionalOnMissingBean(ClassPathTldsLoader.class)
    public ClassPathTldsLoader classPathTldsLoader() {
        return new ClassPathTldsLoader();
    }

}