package com.itmuch.box.service.elastic;

import org.springframework.stereotype.Service;

import com.itmuch.box.core.base.BaseElasticService;
import com.itmuch.box.dao.elastic.ArticleElasticRepository;
import com.itmuch.box.domain.elastic.entity.ArticleElastic;

@Service
public class ArticleElasticService extends BaseElasticService<ArticleElasticRepository, ArticleElastic> {

}
