package com.itmuch.box.util;

import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Sets;

public class StringUtil {

    /**
     * 随机生成count位个整数, 例如6位,主要用于做验证码
     * @param count 字符串长度
     * @return 证书组成的字符串
     */
    public static String radomString(Integer count) {
        String string = "";
        for (int i = 0; i < count; i++) {
            Integer radom = (int) (Math.random() * 10);
            string += radom.toString();
        }
        return string;
    }

    public static Set<String> splitAndTrim(String string, String regex) {
        if (StringUtils.isEmpty(string)) {
            return Sets.newHashSet();
        }
        String trim = string.trim();
        String[] split = trim.split(regex);
        Set<String> set = Sets.newHashSet();
        if ((split != null) && (split.length > 0)) {
            for (String item : split) {
                set.add(item.trim());
            }
        }
        return set;
    }

    /**
     * 将部分字符变为****
     * @param string 入参 字符串
     * @param type 类型 例如手机号/邮箱
     * @return 转换后的字符串
     */
    public static String hideSomeString(String string, Type type) {
        // 邮箱
        if (Type.EMAIL.equals(type)) {
            String s = string.split("@")[1];
            return string.substring(0, 3) + "****@" + s;
        }
        // 手机号
        else if (Type.MOBILE.equals(type)) {
            return string.substring(0, 3) + "****" + string.substring(string.length() - 4);
        }
        // 其他情况
        else {
            return null;
        }
    }

    public enum Type {
        MOBILE, EMAIL;
    }
}
