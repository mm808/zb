package com.itmuch.box.core.base;

import org.springframework.beans.factory.annotation.Value;

public abstract class BaseController {
    @Value("${adminPath}")
    protected String adminPath;
    @Value("${commonPath}")
    protected String commonPath;

    @Value("${uploadPath}")
    protected String uploadPath;
    @Value("${attachLocation}")
    protected String attachLocation;

    @Value("${projectCode}")
    protected String projectCode;

    /**
     * 用于防止xss攻击.
     * 参考代码: http://www.zhihu.com/question/32486587
     * 
     * 本项目使用的spring security的 x-xss-protection 的header的方式防止xss. 
     * 大部分主流浏览器支持x-xss-protection的header. 故而注释
     * 该代码保留, 主要用于适应某些地方需要escape
     * 
     * 注意: initbinder防止不了ajax请求.
     * 原因详见: http://stackoverflow.com/questions/25403676/initbinder-with-requestbody-escaping-xss-in-spring-3-2-4
     * 其中也有防止ajax请求xss攻击的DEMO.
     * xss防止参考文档: http://ju.outofmemory.cn/entry/63726
     * @param binder binder
     */
    /*@Deprecated
    @InitBinder
    protected void initBinder(final WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringEscapeEditor(true, true, true, true));
    }*/
}
