package com.itmuch.box.controller.front.common;

import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.itmuch.box.core.base.BaseController;
import com.itmuch.box.domain.category.entity.Category;
import com.itmuch.box.domain.comment.entity.Comment;
import com.itmuch.box.domain.content.entity.Content;
import com.itmuch.box.domain.content.vo.ContentVo;
import com.itmuch.box.domain.member.entity.Member;
import com.itmuch.box.domain.sort.entity.Sort;
import com.itmuch.box.service.category.CategoryService;
import com.itmuch.box.service.comment.CommentService;
import com.itmuch.box.service.config.ConfigService;
import com.itmuch.box.service.content.ContentArticleService;
import com.itmuch.box.service.content.ContentService;
import com.itmuch.box.service.member.MemberService;
import com.itmuch.box.service.sort.SortService;

@Controller
public class DynamicController extends BaseController {
    @Resource
    private CategoryService categoryService;
    @Resource
    private SortService sortService;
    @Autowired
    private ContentService contentService;
    @Resource
    private ContentArticleService contentArticleService;
    @Resource
    private MemberService memberService;
    @Resource
    private ConfigService configService;
    @Autowired
    private CommentService commentService;

    /**
     * 栏目页
     * @param categoryId 栏目id
     * @param page 页码
     * @param map 模型map
     * @param req 请求
     * @return 栏目页
     */
    @RequestMapping("/category/{categoryId}/{page}")
    public ModelAndView category(@PathVariable("categoryId") Integer categoryId, @PathVariable("page") Integer page, ModelMap map,
            HttpServletRequest req) {
        Category category = this.categoryService.findOne(categoryId);
        if (category != null) {
            Set<Sort> sorts = this.sortService.selectTopByCategoryId(categoryId);

            map.put("page", page);
            map.put("url", req.getContextPath() + "/category/" + categoryId);

            return new ModelAndView("front/category/list-article").addObject("category", category).addObject("sorts", sorts);
        }

        return new ModelAndView();
    }

    /**
     * 二级分类页
     * @param sortId 二级分类id
     * @param page 页码
     * @param map 模型map
     * @param req 请求
     * @return 二级分类页
     */
    @RequestMapping("/sort/{sortId}/{page}")
    public ModelAndView sort(@PathVariable("sortId") Integer sortId, @PathVariable("page") Integer page, ModelMap map, HttpServletRequest req) {
        // 当前二级分类
        Sort sort = this.sortService.findOne(sortId);
        if (sort != null) {
            Integer categoryId = sort.getCategoryId();
            // 通过二级分类查询所在栏目
            Category category = this.categoryService.findOne(categoryId);
            if (category != null) {
                // 通过栏目id, 查询栏目下的顶级二级分类
                Set<Sort> sorts = this.sortService.selectTopByCategoryId(categoryId);

                // 当前二级分类下的子分类
                Set<Sort> subSorts = null;
                // 对于顶级二级分类
                if (sort.getParentId() == 0) {
                    subSorts = this.sortService.selectChildrenById(sortId);
                }
                // 说明已经是二级子分类了
                else {
                    subSorts = this.sortService.selectChildrenById(sort.getParentId());
                }

                map.put("page", page);
                map.put("url", req.getContextPath() + "/sort/" + categoryId);

                return new ModelAndView("front/category/sort/list-article").addObject("category", category).addObject("sorts", sorts)
                        .addObject("sort", sort).addObject("subSorts", subSorts);
            }
        }
        return new ModelAndView();
    }

    @RequestMapping("/content/{id}/{pageNo}")
    public ModelAndView content(@PathVariable Integer id, @PathVariable Integer pageNo) {

        ContentVo contentVo = this.contentArticleService.findArticleById2(id);
        Page<Comment> commentPage = this.commentService.findByContentId(id, new PageRequest(pageNo - 1, 3));

        Integer memberId = contentVo.getContent().getMemberId();
        Member member = this.memberService.findOne(memberId);

        Page<Content> pageInfo = this.contentService.findByMemberIdAndModelId(memberId, 1, new PageRequest(0, 5));

        // 没查询到结果
        return new ModelAndView("front/content/article/article").addObject("article", contentVo).addObject("commentPage", commentPage)
                .addObject("member", member).addObject("pageInfo", pageInfo);
    }
}
