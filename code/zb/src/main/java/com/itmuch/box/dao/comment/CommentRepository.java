package com.itmuch.box.dao.comment;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.itmuch.box.domain.comment.entity.Comment;

public interface CommentRepository extends JpaRepository<Comment, Integer> {

    Page<Comment> findByContentId(Integer id, Pageable pageRequest);
}
