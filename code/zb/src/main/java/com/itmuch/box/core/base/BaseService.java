package com.itmuch.box.core.base;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import com.itmuch.box.core.security.SubjectUtil;
import com.itmuch.box.util.TimeUtil;

public class BaseService<T extends JpaRepository<D, Integer>, D extends BaseEntity> {
    @Autowired
    protected T dao;

    public D insert(D d) {
        d.setCreateTime(TimeUtil.now());

        Integer id = SubjectUtil.getSubjectId();
        d.setCreateId(id);

        return this.dao.save(d);
    }

    public D update(D d) {
        d.setUpdateTime(TimeUtil.now());
        Integer id = SubjectUtil.getSubjectId();
        d.setUpdateId(id);

        return this.dao.save(d);
    }

    public List<D> findAll() {
        return this.dao.findAll();
    }

    public D findOne(Integer id) {
        return this.dao.findOne(id);
    }

    // 尽量使用 insert 及 update
    @Deprecated
    public D save(D d) {
        return this.dao.save(d);
    }

    public void delete(Integer id) {
        this.dao.delete(id);
    }

}
