package com.itmuch.box.dao.sort;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.itmuch.box.domain.sort.entity.Sort;

public interface SortRepository extends JpaRepository<Sort, Integer> {

    List<Sort> findByCategoryId(Integer categoryId);

    Set<Sort> findByCategoryIdAndParentId(Integer categoryId, Integer parentId);

    Set<Sort> findByParentId(Integer id);
}
