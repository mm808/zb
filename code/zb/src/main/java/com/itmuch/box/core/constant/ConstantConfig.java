package com.itmuch.box.core.constant;

// f_config 里面的key
public class ConstantConfig {
    public static final String FRONT_DOMAIN = "front.domain";
    public static final String ATTACH_DOMAIN = "attach.domain";
    public static final String MAIL_HOST = "mail.host";
    public static final String MAIL_PORT = "mail.port";
    public static final String MAIL_USERNAME = "mail.username";
    public static final String MAIL_PASSWORD = "mail.password";
    public static final String MAIL_SMTP_AUTH = "mail.smtp.auth";
    public static final String MAIL_REG_SUBJECT = "mail.reg.subject";
    public static final String MAIL_REG_TEXT = "mail.reg.text";
}
