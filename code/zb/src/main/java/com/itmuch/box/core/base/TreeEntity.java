package com.itmuch.box.core.base;

import java.util.List;

public class TreeEntity<T> extends BaseEntity {
    private static final long serialVersionUID = 1L;
    //    protected Integer id;
    protected Integer parentId;
    protected List<T> children;
    protected String name;

    public Integer getParentId() {
        return this.parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public List<T> getChildren() {
        return this.children;
    }

    public void setChildren(List<T> children) {
        this.children = children;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
