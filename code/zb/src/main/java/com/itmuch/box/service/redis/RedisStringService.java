package com.itmuch.box.service.redis;

import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

/**
 * 验证码 redis操作类
 * 参考文档: http://stamen.iteye.com/blog/1907984
 * @author zhouli
 */
@Service
public class RedisStringService {
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 插入/修改值
     * @param key key
     * @param value value
     * @param timeout 超时时间(时间单元)
     * @param timeunit 时间单元
     */
    public void set(String key, String value, Long timeout, TimeUnit timeunit) {
        this.stringRedisTemplate.opsForValue().set(key, value, timeout, timeunit);

    }

    /**
     * 通过key 查询redis
     * @param key key
     * @return value
     */
    public String getByKey(String key) {
        return this.stringRedisTemplate.opsForValue().get(key);
    }

    public void deleteByKey(String key) {
        this.stringRedisTemplate.delete(key);
    }
}
