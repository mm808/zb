package com.itmuch.box.core;

import java.util.ArrayList;
import java.util.List;

public class PageNative<T> {

    private List<T> content = new ArrayList<T>();
    private long number;
    private int pageSize;
    private long total;
    private boolean first;
    private boolean last;
    private long totalPages;

    public PageNative(List<T> content, long total, long pageNo, int pageSize) {
        this.content = content;
        this.total = total;
        this.pageSize = pageSize;

        this.number = pageNo - 1;

        if (this.number == 0) {
            this.first = true;
        }

        long mod = total % pageSize;
        if (mod == 0) {
            this.totalPages = total / pageSize;
        } else {
            this.totalPages = (total / pageSize) + 1;
        }

        if (pageNo == this.totalPages) {
            this.last = true;
        }
    }

    public List<T> getContent() {
        return this.content;
    }

    public void setContent(List<T> content) {
        this.content = content;
    }

    public long getNumber() {
        return this.number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public int getPageSize() {
        return this.pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public long getTotal() {
        return this.total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public boolean isFirst() {
        return this.first;
    }

    public void setFirst(boolean first) {
        this.first = first;
    }

    public boolean isLast() {
        return this.last;
    }

    public void setLast(boolean last) {
        this.last = last;
    }

    public long getTotalPages() {
        return this.totalPages;
    }

    public void setTotalPages(long totalPages) {
        this.totalPages = totalPages;
    }
}
