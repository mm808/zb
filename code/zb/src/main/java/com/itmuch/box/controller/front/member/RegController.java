package com.itmuch.box.controller.front.member;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.itmuch.box.core.base.BaseController;
import com.itmuch.box.core.constant.ConstantRedis;
import com.itmuch.box.domain.member.entity.Member;
import com.itmuch.box.domain.member.vo.MemberRegVo;
import com.itmuch.box.service.member.MemberService;
import com.itmuch.box.service.redis.RedisStringService;
import com.itmuch.box.util.mail.MailServerUtil;

@Controller
public class RegController extends BaseController {

    @Autowired
    private MemberService memberService;
    @Autowired
    private RedisStringService redisStringService;

    @RequestMapping(value = "/reg", method = RequestMethod.GET)
    public ModelAndView reg() {
        return new ModelAndView("front/member/reg/reg");
    }

    @RequestMapping(value = "/reg", method = RequestMethod.POST)
    public String reg(@Valid @ModelAttribute("regVo") MemberRegVo regVo, BindingResult result, HttpServletRequest req, RedirectAttributes ra)
            throws InterruptedException, ExecutionException {
        if (!result.hasErrors()) {
            Member memberInDB = this.memberService.findByKeys(regVo.getUsername(), regVo.getEmail());
            // 该用户名可以注册
            if (memberInDB == null) {
                if ("111111".equals(regVo.getMobileCode())) {
                    Future<Boolean> future = this.memberService.reg(regVo, req);
                    Boolean regRes = future.get();
                    if (regRes) {
                        String mailServer = MailServerUtil.queryEmailServerByEmail(regVo.getEmail());
                        ra.addFlashAttribute("mailServer", mailServer);
                        return String.format("redirect:/%s", "reg-success");
                    } else {
                        result.rejectValue("email", null, "邮箱发送失败, 请稍后再试.");
                    }
                } else {
                    result.rejectValue("mobileCode", null, "手机验证码不正确");
                }
            } else {
                result.rejectValue("username", null, "用户名或邮箱重复");
            }
        }
        return "front/member/reg/reg";
    }

    @RequestMapping(value = "/reg-valid/{vcode}", method = RequestMethod.GET)
    public String reg(@PathVariable String vcode) {
        String idString = this.redisStringService.getByKey(ConstantRedis.REG_EMAIL_PREFIX + vcode);
        if (NumberUtils.isNumber(idString)) {
            Integer id = NumberUtils.toInt(idString);

            this.memberService.bindEmail(vcode, id);
            return String.format("redirect:/%s", "reg-valid-success");
        }
        return String.format("redirect:/%s", "reg-valid-fail");
    }

    @RequestMapping(value = "/reg-success", method = RequestMethod.GET)
    public String regSuccess() {
        return "front/member/reg/reg-success";
    }

    @RequestMapping(value = "/reg-valid-success", method = RequestMethod.GET)
    public String regValidSuccess() {
        return "front/member/reg/reg-valid-success";
    }

    @RequestMapping(value = "/reg-valid-fail", method = RequestMethod.GET)
    public String regValidFail() {
        return "front/member/reg/reg-valid-fail";
    }

}
