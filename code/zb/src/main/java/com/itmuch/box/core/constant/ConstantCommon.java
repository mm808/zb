package com.itmuch.box.core.constant;

public class ConstantCommon {
    public final static int BCrypt_Password_STRENGTH = 4;

    public static final String QQ_OPEN_ID_KEY = "qq_open_id";
}
