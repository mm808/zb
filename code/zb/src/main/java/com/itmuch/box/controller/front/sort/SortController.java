package com.itmuch.box.controller.front.sort;

import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itmuch.box.core.constant.ConstantCode;
import com.itmuch.box.core.converter.AjaxResult;
import com.itmuch.box.domain.sort.entity.Sort;
import com.itmuch.box.service.sort.SortService;
import com.itmuch.box.util.ErrorMsgUtil;

@Controller
@RequestMapping("/sort")
public class SortController {
    @Resource
    private SortService sortService;

    @RequestMapping(value = "/get-top-by-cat-id", method = RequestMethod.GET)
    @ResponseBody
    public AjaxResult selectTopByCategoryId(Integer categoryId) {
        Set<Sort> list = this.sortService.selectTopByCategoryId(categoryId);
        if (!CollectionUtils.isEmpty(list)) {
            return new AjaxResult(list);
        } else {
            return ErrorMsgUtil.error("该栏目下没有二级分类", "该栏目下没有二级分类", ConstantCode.DATA_NOT_FOUND);
        }
    }

    @RequestMapping(value = "/get-son-by-id", method = RequestMethod.GET)
    @ResponseBody
    public AjaxResult selectChildrenById(Integer id) {
        Set<Sort> list = this.sortService.selectChildrenById(id);
        if (!CollectionUtils.isEmpty(list)) {
            return new AjaxResult(list);
        } else {
            return ErrorMsgUtil.error("该分类下没有子分类", "该分类下没有子分类", ConstantCode.DATA_NOT_FOUND);
        }
    }
}
