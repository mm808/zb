package com.itmuch.box.dao.category;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.itmuch.box.domain.category.entity.Category;

public interface CategoryRepository extends JpaRepository<Category, Integer> {

    Long countByParentId(int id);

    Set<Category> findByModelId(Integer modelId);
}
