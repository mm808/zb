package com.itmuch.box.dao.content;

import org.springframework.data.jpa.repository.JpaRepository;

import com.itmuch.box.domain.content.entity.ContentArticle;

public interface ContentArticleRepository extends JpaRepository<ContentArticle, Integer> {
}
