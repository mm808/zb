package com.itmuch.box.core.constant;

public class ConstantDirective {
    public static final String CONTENT_LIST_PAGE = "contentPage";

    public static final String AUTHOR_INFO = "authorInfo";
}
