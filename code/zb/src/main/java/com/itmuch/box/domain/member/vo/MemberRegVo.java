package com.itmuch.box.domain.member.vo;

import java.io.Serializable;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

public class MemberRegVo implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotEmpty(message = "用户名不能为空.")
    @Length(min = 5, max = 30, message = "用户名长度在5-30之间.")
    private String username;

    @NotEmpty(message = "邮箱不能为空.")
    @Length(min = 5, max = 40, message = "邮箱长度在5-40之间.")
    @Email(message = "邮箱格式非法.")
    private String email;

    @NotEmpty(message = "密码不能为空.")
    @Length(min = 5, max = 30, message = "密码长度在5-30之间.")
    private String password;

    @NotEmpty(message = "确认密码不能为空.")
    @Length(min = 5, max = 30, message = "确认密码长度在5-30之间.")
    private String password2;

    @NotEmpty(message = "手机号不能为空.")
    @Pattern(regexp = "^1(3[0-9]|5[0-35-9]|8[0235-9])\\d{8}$", message = "不是合法的手机号.")
    private String mobile;

    @NotEmpty(message = "手机验证码不能为空.")
    private String mobileCode;

    @NotNull(message = "请勾选同意协议.")
    @AssertTrue(message = "请勾选同意协议.")
    private Boolean agree;

    private String mailServer;

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword2() {
        return this.password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMobileCode() {
        return this.mobileCode;
    }

    public void setMobileCode(String mobileCode) {
        this.mobileCode = mobileCode;
    }

    public Boolean getAgree() {
        return this.agree;
    }

    public void setAgree(Boolean agree) {
        this.agree = agree;
    }

    public String getMailServer() {
        return this.mailServer;
    }

    public void setMailServer(String mailServer) {
        this.mailServer = mailServer;
    }
}
