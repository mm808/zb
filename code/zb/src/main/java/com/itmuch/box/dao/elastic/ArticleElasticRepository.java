package com.itmuch.box.dao.elastic;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.itmuch.box.domain.elastic.entity.ArticleElastic;

public interface ArticleElasticRepository extends ElasticsearchRepository<ArticleElastic, Integer> {

}
