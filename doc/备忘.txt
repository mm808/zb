## 疑难问题
1. elastic + 关系型数据库 多数据源时:
使用elastic时, @Document 和 @Entity不能注解在同一个实体类上, 否则:
如果DAO继承自: ElasticsearchRepository, 将会启动失败, 报属性index缺失; 如果继承自ElasticsearchCrudRepository, 则虽然能启动, 但是不会持久化到elastic中, 而是关系型数据库中. 
stackoverflow中也有人提出相同问题, 但是无解. 原因暂时还没搞清楚.
http://stackoverflow.com/questions/32079476/spring-data-jpa-transient-field-in-elastic-search


## 一对一懒加载失效的问题:
http://blog.csdn.net/wangpeng047/article/details/19624795
在主标中加上文中的注解即可.